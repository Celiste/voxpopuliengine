#ifndef VPEE_SCREENMAINMENU_HPP
#define VPEE_SCREENMAINMENU_HPP

/*! \file
	\author Brian Cobie aka. Kiruse
	\brief Declaration of the experimental main menu screen.
	\copyright MIT
 */

#include <vpe/Screen.hpp>

namespace vpee
{
	class ScreenMainMenu : public vpe::Screen
	{
	public:
		ScreenMainMenu();
		ScreenMainMenu(const ScreenMainMenu&) = default;
		ScreenMainMenu(ScreenMainMenu&&) = default;
		virtual ScreenMainMenu& operator=(const ScreenMainMenu&) = default;
		virtual ScreenMainMenu& operator=(ScreenMainMenu&&) = default;
		virtual ~ScreenMainMenu();

	protected:
		virtual void initialize() override;
	};
}

#endif
