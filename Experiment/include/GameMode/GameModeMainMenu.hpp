#ifndef VPEE_GAMEMODEMAINMENU_HPP
#define VPEE_GAMEMODEMAINMENU_HPP

#include <vpe/GameMode.hpp>

namespace vpee
{
	class GameModeMainMenu : public vpe::GameMode
	{
	public:
		GameModeMainMenu();
		virtual GameModeMainMenu& operator=(const GameModeMainMenu&) = default;
		virtual GameModeMainMenu& operator=(GameModeMainMenu&&) = default;

	protected:
		virtual void initialize() override;
		virtual void abandon() override;
	};
}

#endif
