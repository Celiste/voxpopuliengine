#!/bin/sh
find \
	"$1/bin/$2/$3" \
	"$1/lib/sfml-2.4.2/$3/lib" \
	"$1/lib/sfml-2.4.2/src/extlibs/bin/$2" \
	"$1/lib/glew-2.1.0/$3/bin" \
	-name *.dll -exec cp "{}" "bin/$2/$3" \;

read -n 1 -s -p "Post-Build completed - please press any key to continue"
