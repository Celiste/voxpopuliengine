/*! \file
	\author Brian Cobile aka. Kiruse
	\brief Implementation of the main menu screen.
	\copyright MIT
 */

#include "Screen/ScreenMainMenu.hpp"

namespace vpee
{
	ScreenMainMenu::ScreenMainMenu()
	{

	}

	ScreenMainMenu::~ScreenMainMenu()
	{

	}

	void ScreenMainMenu::initialize()
	{

	}
}
