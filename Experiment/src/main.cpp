#include <vpe/Engine.hpp>
#include <vpe/Exception.hpp>
#include <utility>
#include "GameMode/GameModeMainMenu.hpp"

int main(int argc, char** argv)
{
	using namespace vpe;
	vpe::Graphics::GLContext ctx(sf::VideoMode(800, 600, 32u), "Test", sf::Style::None, sf::ContextSettings{});

	Engine::startup(std::shared_ptr<GameMode>{new vpee::GameModeMainMenu});
	Engine::instance().setRenderTarget(&ctx);

	Engine::instance().run();

	Engine::instance().onWindowClose += Delegate<void(sf::Event&, sf::Window&)>::fromLambda([](sf::Event& evt, sf::Window& wnd)
	{
		Engine::instance().exit();
	});
}
