#include "GameMode/GameModeMainMenu.hpp"
#include "Screen/ScreenMainMenu.hpp"

namespace vpee
{
	GameModeMainMenu::GameModeMainMenu()
	{
		screen.reset(new ScreenMainMenu());
	}


	void GameModeMainMenu::initialize()
	{
		screen->initialize();
	}

	void GameModeMainMenu::abandon()
	{
		screen.reset();
	}
}
