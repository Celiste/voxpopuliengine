#include "LinearAlgebra.h"
#include <iostream>
#include <Core/Math/Math.hpp>
#include <Core/Math/LinearAlgebra.hpp>

using namespace std;
using namespace vpe::Math;

namespace vpe
{
    namespace UnitTest
    {
        void simple_test_linearalgebra()
        {
            bool success = true;

            { // 2D Vectors
                FVector2 a(2, 3), b(3, 4), c = a + b;

                if (!nearly(c.x, 5) || !nearly(c.y, 7))
                {
                    cout << "Vector2 addition failed" << endl;
                    success = false;
                }

                c = a - b;

                if (!nearly(c.x, -1) || !nearly(c.y, -1))
                {
                    cout << "Vector2 subtraction failed" << endl;
                    success = false;
                }

                float dotres = dot(a, b);
                if (!nearly(dotres, 18))
                {
                    cout << "Vector2 dot product failed" << endl;
                    success = false;
                }

                float crossres = cross(a, b);
                if (!nearly(crossres, -1))
                {
                    cout << "Vector2 cross product failed" << endl;
                    success = false;
                }

                if (!nearly(dot(a, orthogonal(a)), 0))
                {
                    cout << "Vector2 orthogonal failed" << endl;
                    success = false;
                }
            } // 2D Vectors

            { // 3D Vectors
                FVector3 a(2, 3, 4), b(5, 6, 7);
                FVector2 c(8, 9);

                FVector3 d = a + c;
                if (!nearly(d.x, 10) || !nearly(d.y, 12) || !nearly(d.z, 4))
                {
                    cout << "Vector3 + Vector2 failed" << endl;
                    success = false;
                }

                d = b - c;
                if (!nearly(d.x, -3) || !nearly(d.y, -3) || !nearly(d.z, 7))
                {
                    cout << "Vector3 - Vector2 failed" << endl;
                    success = false;
                }

                float dotres = dot(a, b);
                if (!nearly(dotres, 56))
                {
                    cout << "Vector3 dot product failed" << endl;
                    success = false;
                }

                d = cross(a, b);
                if (!nearly(d.x, -3) || !nearly(d.y, 6) || !nearly(d.z, -3))
                {
                    cout << "Vector3 cross product failed" << endl;
                    success = false;
                }
            } // 3D Vectors

            { // 4D Vectors (nothing specific to check yet)
                FVector3 pos(2, 5, 10);
                FVector4 rot(1, 0, 0, c_pi_over_2);
                FQuaternion quat(rot);

                if (!nearly(quat.r, 0.7071068f) || !nearly(quat.i, 0.7071068f) || !nearly(quat.j, 0) || !nearly(quat.k, 0))
                {
                    cout << "Axis-Angle to Quaternion conversion failed" << endl;
                    success = false;
                }

                FVector3 rotated = pos * quat;
                // Apparently rotated.y == -10...
                if (!nearly(rotated.x, 2) || !nearly(rotated.y, -10) || !nearly(rotated.z, 5))
                {
                    cout << "Vector * Quaternion rotation failed" << endl;
                    success = false;
                }

                FMatrix4 mat = FMatrix4::fromTranslation(0, 0, 5);
                mat *= FMatrix4::fromAxisAngle(1, 0, 0, c_pi_over_2);
                mat *= FMatrix4::fromScale(1, 2, 1);

                FVector3 transformed = mat * pos;
                if (!nearly(transformed.x, 2) || !nearly(transformed.y, -10) || !nearly(transformed.z, 15))
                {
                    cout << "Transform Matrix * Vector failed" << endl;
                    success = false;
                }
            } // 4D Vectors

            if (success)
            {
                cout << "Linear Algebra unit test succeeded" << endl;
            }
            else
            {
                cout << "Linear Algebra unit test failed" << endl;
            }
        }
    }
}
