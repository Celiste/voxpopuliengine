#include <iostream>
#include "Delegate.h"
#include <Core/Delegate.hpp>


inline int echo(int val) { return val; }

struct Foo
{
	struct FooEvent : public vpe::Event {
	public:
		int value = 0;

	public:
		FooEvent() = default;
		FooEvent(int value) : Event(), value(value) {}
		FooEvent(const FooEvent&) = default;
		FooEvent(FooEvent&&) = default;
		FooEvent& operator=(const FooEvent&) = default;
		FooEvent& operator=(FooEvent&&) = default;
		~FooEvent() = default;

	public:
		static FooEvent& create(int val) { return *new FooEvent(val); }
	};

	int value;
	Foo() : value(0) {}
	Foo(int val) : value(val) {}

	int getValue() const { return value; }
	int getValue() const volatile { return value + 1; }

	void setValue(int val) { value = val; }
	void setValue(int val) volatile { value = val + 1; }

	int pass(int val)
	{
		value = val;
		return val;
	}

	int pass(int val) const
	{
		return val;
	}

	int pass(int val) volatile
	{
		value = val + 1;
		return value;
	}

	int pass(int val) const volatile
	{
		return val + 1;
	}

	static int echo(int val)
	{
		return val;
	}

	void trigger1()
	{
		event1.invoke(27);
	}

	void trigger2()
	{
		const FooEvent& evt = FooEvent::create(29);
		event2.invoke(evt, 27);
	}

	vpe::EventEmitter<Foo, vpe::Event, int> event1;
	vpe::EventEmitter<Foo, FooEvent, int> event2;
};


namespace vpe {
	namespace UnitTest
	{
		void simple_test_delegate()
		{
			using namespace std;
			bool MulticastAddOkay = false;

			// Test function delegate
			if (Delegate<int(int)>::FunctionDelegate<echo>()(13) != 13) cout << "FunctionDelegate failed" << endl;

			// Test method delegates
			{
				Foo foo1(14);
				volatile Foo foo2(15);

				Delegate<int()>::ConstMethodDelegate<Foo, &Foo::getValue> deleg1(foo1);
				if (deleg1() != 14) cout << "ConstMethodDelegate failed" << endl;

				Delegate<int()>::CVMethodDelegate<Foo, &Foo::getValue> deleg2(foo1), deleg3(foo2);
				if (deleg2() != 15 || deleg3() != 16) cout << "CVMethodDelegate failed" << endl;

				Delegate<void(int)>::MethodDelegate<Foo, &Foo::setValue> deleg4(foo1);
				deleg4(16);
				if (foo1.value != 16) cout << "MethodDelegate failed" << endl;

				Delegate<void(int)>::VolatileMethodDelegate<Foo, &Foo::setValue> deleg5(foo2);
				deleg5(17);
				if (foo2.value != 18) cout << "VolatileMethodDelegate failed" << endl;
			}

			// Test lambda delegates
			{
				int myvalue = 26;

				auto ldeleg1 = Delegate<int(int)>::fromLambda([](int echo)
				{
					return echo;
				});

				auto ldeleg2 = Delegate<int()>::fromLambda([=]()
				{
					return myvalue;
				});

				auto ldeleg3 = Delegate<int()>::fromLambda([&]()
				{
					return myvalue;
				});

				auto ldeleg4 = Delegate<int(int)>::fromLambda([&](int echo)
				{
					++myvalue;
					return echo;
				});

				if (ldeleg1(25) != 25) cout << "LambdaDelegate (no capture) failed" << endl;
				if (ldeleg2() != 26) cout << "LambdaDelegate (copy capture, 1) failed" << endl;
				if (ldeleg3() != 26) cout << "LambdaDelegate (ref capture a, 1) failed" << endl;
				if (ldeleg4(28) != 28 || myvalue != 27) cout << "LambdaDelegate (ref capture b) failed" << endl;
				if (ldeleg2() != 26) cout << "LambdaDelegate (copy capture, 2) failed" << endl;
				if (ldeleg3() != 27) cout << "LambdaDelegate (ref capture a, 2) failed" << endl;
			}

			// Test add to multicast delegate & one-time handlers
			{
				int lambdaValue = 0;

				Foo foo1(24);
				volatile Foo foo2(25);

				MulticastDelegate<void(int)> multi;
				multi += Delegate<void(int)>::MethodDelegate<Foo, &Foo::setValue>(foo1);
				multi += Delegate<void(int)>::VolatileMethodDelegate<Foo, &Foo::setValue>(foo2);
				multi.once(Delegate<void(int)>::fromLambda([&](int val) { lambdaValue = val; }));

				MulticastAddOkay = multi.size() == 3;

				if (!MulticastAddOkay)
				{
					cout << "MulticastDelegate add failed - additional tests will be skipped" << endl;
				}

				else
				{
					multi(26);
					if (foo1.value != 26 || foo2.value != 27 || lambdaValue != 26) cout << "MulticastDelegate call failed" << endl;

					if (multi.size() != 2) cout << "MulticastDelegate one-time handler auto-removal failed" << endl;
				}
			}

			// More multicast delegate tests
			if (MulticastAddOkay)
			{
				Foo foo1(24);
				volatile Foo foo2(25);

				MulticastDelegate<int(int)> multi;
				multi += Delegate<int(int)>::FunctionDelegate<echo>();
				multi += Delegate<int(int)>::FunctionDelegate<Foo::echo>();
				multi += Delegate<int(int)>::MethodDelegate<Foo, &Foo::pass>(foo1);
				multi += Delegate<int(int)>::ConstMethodDelegate<Foo, &Foo::pass>(foo1);
				multi += Delegate<int(int)>::VolatileMethodDelegate<Foo, &Foo::pass>(foo1);
				multi += Delegate<int(int)>::VolatileMethodDelegate<Foo, &Foo::pass>(foo2);
				multi += Delegate<int(int)>::CVMethodDelegate<Foo, &Foo::pass>(foo1);
				multi += Delegate<int(int)>::CVMethodDelegate<Foo, &Foo::pass>(foo2);
				multi += Delegate<int(int)>::CVMethodDelegate<Foo, &Foo::pass>(foo2);
				multi += Delegate<int(int)>::CVMethodDelegate<Foo, &Foo::pass>(foo2);
				multi += Delegate<int(int)>::CVMethodDelegate<Foo, &Foo::pass>(foo2);

				if (multi.size() != 11) cout << "MulticastDelegate expected delegate count of 11" << endl;
				else {
					int sum = 0;

					multi.invoke(32, [&](int val)
					{
						sum += val;
					});

					if (sum != 359) cout << "MulticastDelegate (lambda handler) expected sum of 359, got " << sum << endl;

					// Any type of delegate will do for the following test given that they all implement invoke() and should thus work.
					// So we use a lambda delegate for the sake of simplicity.
					int counter = 0;
					bool handlerInvoked = false;

					auto handler = Delegate<void(int)>::fromLambda([&](int val)
					{
						handlerInvoked = true;

						if ((counter < 4 && val != 42) || (counter >= 4 && val != 43)) cout << "MulticastDelegate (delegate handler) failed - unexpected value" << endl;
						++counter;
					});

					multi.invoke(42, handler);

					if (!handlerInvoked) cout << "MulticastDelegate (delegate handler) failed - handler not called" << endl;
				}
			}

			// Test event emitters
			if (MulticastAddOkay)
			{
				Foo foo(24);

				foo.event1 += Delegate<void(Event&, int)>::fromLambda([&](Event& evt, int value)
				{
					if (value != 27) cout << "EventEmitter (default event structure) failed" << endl;
				});

				foo.event2 += Delegate<void(Foo::FooEvent&, int)>::fromLambda([&](Foo::FooEvent& evt, int val)
				{
					if (val != 27 || evt.value != 29) cout << "EventEmitter (custom event structure) failed" << endl;
				});

				foo.trigger1();
				foo.trigger2();
			}

			cout << "Delegate unit test complete - if no messages are above then the test went successful" << endl;
		}
	}
}
