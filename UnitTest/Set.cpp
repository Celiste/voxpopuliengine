#include <Core/HashMap.hpp>
#include <Core/Integrals.hpp>
#include <string>
#include <iostream>
#include <utility>
#include "Set.h"

namespace vpe {
	namespace UnitTest
	{
		void simple_test_set()
		{
			using namespace std;

            // Test simple set operations
            {
                vpe::HashSet<int32_t> set1;
                vpe::HashSet<int32_t> set2;
                vpe::HashSet<int32_t> set3;

                set1 += 2;
                set1 += 4;
                set2 += 4;
                set2 += 16;

                if (set1.length() != 2 || set2.length() != 2 || !set1.has(2) || !set1.has(4) || !set2.has(4) || !set2.has(16)) cout << "HashSet add failed" << endl;

                set3 = set2;
                set3.meanWith(set1);

                if (set3.length() != 1 || !set3.has(4)) cout << "HashSet meanWith failed" << endl;

                set3 = set1;
                set3.unionWith(set2);

                if (set3.length() != 3 || !set3.has(2) || !set3.has(4) || !set3.has(16)) cout << "HashSet unionWith failed" << endl;

                set3 = set1;
                set3.differenceTo(set2);

                if (set3.length() != 1 || !set3.has(2)) cout << "HashSet differenceTo failed" << endl;
            }

            // Test set resizing
            {
                vpe::HashSet<int32_t> set;

                for (size_t i = 0; i < 12; ++i)
                {
                    set += (int32_t)i;
                }

                if (set.size() != 20 || set.length() != 12) cout << "HashSet resizing failed" << endl;

                set.resize(8);

                // Sets are resized to multiples of their increments, in this case 10
                if (set.size() != 10 || set.length() != 10) cout << "HashSet downsizing failed" << endl;
            }

            // Test hash collision
            {
                vpe::HashSet<int32_t> set;

                set += {0, 10, 20, 30, 40, 50, 11};

                if (set.size() != 30 || !set.has(0) || !set.has(10) || !set.has(11) || !set.has(20) || !set.has(30) || !set.has(40) || !set.has(50))
                    cout << "HashSet hash conflict (resizing) failed" << endl;
            }

            // Test hash map
            {
                HashMap<uint8_t, std::string> map;
                map.add(8, "foo");
                map.add(9, "bar");
                map.add(8, "bar");

                if (map.size() != 10 || map.length() != 2 || !map.has(8) || !map.has(9) || map.get(8) != "bar" || map.get(9) != "bar")
                    cout << "HashMap failed" << endl;
            }

			cout << "HashSet Unit Test complete - if there are no messages above, the unit test was successful." << endl;
		}
	}
}
