#include "FixedPointDecimal.h"
#include <Core/FixedPointDecimal.hpp>
#include <iostream>

namespace vpe {
	namespace UnitTest
	{
		void simple_test_fixedpointdecimal()
		{
			using namespace std;

			{
				// Visual Studio's IntelliSense can't understand that the underlying code actually compiles fine.
				Fixed fixed(4);

				if (fixed.raw != 4 << 8) cout << "Fixed constructor (int) failed" << endl;

				double val = fixed;
				if (val < 3.9995 || val > 4.0005) cout << "Fixed conversion (double, 1) failed" << endl;

				fixed = 4.5f;
				val = fixed;
				if (val < 4.49995 || val > 4.50005) cout << "Fixed conversion (double, 2) failed" << endl;
			}

			cout << "FixedPointDecimal unit test concluded. If no messages are above this, the test was successful." << endl;
		}
	}
}
