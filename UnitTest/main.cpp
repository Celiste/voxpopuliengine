#include <iostream>
#include <conio.h>
#include "Container.h"
#include "DataControl.h"
#include "Delegate.h"
#include "FixedPointDecimal.h"
#include "LinearAlgebra.h"
#include "Set.h"

int main()
{
    //vpe::UnitTest::simple_test_datacontrol();
    //vpe::UnitTest::simple_test_delegate();
    //vpe::UnitTest::simple_test_set();
    //vpe::UnitTest::simple_test_fixedpointdecimal();
    //vpe::UnitTest::simple_test_containers();
    vpe::UnitTest::simple_test_linearalgebra();

	std::cout << std::endl << "Press any key to continue ..." << std::endl;
	_getch();
	return 0;
}
