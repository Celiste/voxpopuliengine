#include "Container.h"
#include <iostream>
#include <Core/Array.hpp>
#include <Core/List.hpp>

namespace vpe
{
    namespace UnitTest
    {
        void simple_test_containers()
        {
            using namespace std;

            { // List
                bool proceed = true;

                List<int32_t> lst;
                lst.append(42);
                lst.prepend(24);
                lst.append(420);

                if (lst.length() != 3)
                {
                    cout << "List insertion failed" << endl;
                    proceed = false;
                }

                if (proceed)
                {
                    int32_t compare[] = {24, 42, 420};
                    size_t index = 0;
                    for (auto& val : lst)
                    {
                        if (val != compare[index++])
                        {
                            cout << "List insertion / iteration failed" << endl;
                            proceed = false;
                            break;
                        }
                    }

                    if (proceed)
                    {
                        List<int32_t>* copy = new List<int32_t>(lst);
                        index = 0;
                        for (auto& val : *copy)
                        {
                            if (val != compare[index++])
                            {
                                cout << "List copy failed" << endl;
                                proceed = false;
                                break;
                            }
                        }

                        if (proceed)
                        {
                            List<int32_t> move(std::move(*copy));
                            if (move.length() != 3)
                            {
                                cout << "List move failed (1)" << endl;
                                proceed = false;
                            }

                            delete copy;

                            if (proceed)
                            {
                                index = 0;
                                for (auto& val : move)
                                {
                                    if (val != compare[index++])
                                    {
                                        cout << "List move failed (2)" << endl;
                                        proceed = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (proceed)
                {
                    if (lst[0] != 24)
                    {
                        cout << "List initial get failed - the list's cursor should be on the first element after inserting a value" << endl;
                        proceed = false;
                    }
                }

                if (proceed)
                {
                    lst.append(24);
                    lst.prepend(24);
                    lst.removeAll(24);
                    if (lst.length() != 2)
                    {
                        cout << "List occurrence removal failed" << endl;
                        proceed = false;
                    }
                }

                if (proceed)
                {
                    lst.prepend(420);
                    lst.append(42);
                    lst.append(420);
                    for (auto it = lst.begin(), end = lst.end(); (it = IteratorUtil::find(it, end, 420)) != end;)
                        it = lst.remove(it);

                    if (lst.length() != 2)
                    {
                        cout << "List find next pattern failed" << endl;
                        proceed = false;
                    }
                }

                if (!proceed)
                {
                    cout << "List UnitTest failed" << endl;
                }
                else
                {
                    cout << "List UnitTest successful" << endl;
                }
            }

            { // Array
                bool proceed = true;

                Array<int32_t> arr({3, 4, 12, 24, 42, 420});
                if (arr.length() != 6)
                {
                    cout << "Array initialize failed" << endl;
                    proceed = false;
                }

                if (proceed)
                {
                    arr.append(1);
                    arr.append({2, 3, 4});
                    arr.prepend(5);
                    arr.prepend({6, 7, 8});
                    arr.insert(10, 9);
                    arr.insert(10, {10, 11, 12});
                    if (arr.length() != 18)
                    {
                        cout << "Array modify failed (1)" << endl;
                        proceed = false;
                    }

                    if (proceed)
                    {
                        int32_t values[] = {6, 7, 8, 5, 3, 4, 12, 24, 42, 420, 10, 11, 12, 9, 1, 2, 3, 4};
                        size_t index = 0;
                        for (auto& val : arr)
                        {
                            if (val != values[index++])
                            {
                                cout << "Array modify failed (2)" << endl;
                                proceed = false;
                                break;
                            }
                        }
                    }

                    if (proceed)
                    {
                        arr.remove(4, 8);
                        if (arr.length() != 10)
                        {
                            cout << "Array modify failed (3)" << endl;
                            proceed = false;
                        }
                    }

                    if (proceed)
                    {
                        int32_t values[] = {6, 7, 8, 5, 12, 9, 1, 2, 3, 4};
                        size_t index = 0;
                        for (auto& val : arr)
                        {
                            if (val != values[index++])
                            {
                                cout << "Array modify failed (4)" << endl;
                                proceed = false;
                                break;
                            }
                        }
                    }
                }

                if (!proceed)
                {
                    cout << "Array UnitTest failed" << endl;
                }
                else
                {
                    cout << "Array UnitTest successful" << endl;
                }
            }

        }
    }
}
