#include "DataControl.h"
#include <iostream>
#include <Core/DataControl.hpp>

struct Parent
{
    bool foo;
    int bar;

    Parent() = default;
    Parent(const Parent&) = default;
    Parent(Parent&&) = default;
    Parent& operator=(const Parent&) = default;
    Parent& operator=(Parent&&) = default;
    virtual ~Parent() = default;
};

struct Child : public Parent {};

struct LifetimeTest
{
    int valid;

    LifetimeTest() : valid(0x1A71FE) {} // I ALIFE
    ~LifetimeTest() { valid = 0; }
};

namespace vpe
{
    namespace UnitTest
    {
        void simple_test_datacontrol()
        {
            using namespace std;

            bool proceed = true;

            {
                DataController<int32_t> root(new int32_t(42));

                if (*root != 42)
                {
                    cout << "DataControl creation failed" << endl;
                    proceed = false;
                }
            }

            if (proceed)
            {
                LifetimeTest* obj1 = new LifetimeTest(), *obj2 = new LifetimeTest();

                {
                    DataController<LifetimeTest> root(obj1);
                    root = obj2;
                    if (obj1->valid == 0x1A71FE)
                    {
                        cout << "DataController data deletion on assignment failed" << endl;
                        proceed = false;
                    }
                }

                if (obj2->valid == 0x1A71FE)
                {
                    cout << "DataController data deletion at end of controller lifetime failed" << endl;
                    proceed = false;
                }
            }

            if (proceed) {
                DataHandle<int32_t> handle1;
                DataHandle<int32_t> handle2;
                DataHandle<int32_t> handle3;

                {
                    DataController<int32_t> root1(new int32_t(24)), root2(new int32_t(42));
                    handle1 = handle2 = handle3 = root1;

                    if (*handle1 != 24 || *handle2 != 24 || *handle3 != 24)
                    {
                        cout << "DataHandle creation failed" << endl;
                        proceed = false;
                    }

                    if (proceed)
                    {
                        handle3 = root2;

                        if (*handle3 != 42)
                        {
                            cout << "DataHandle reassignment failed" << endl;
                            proceed = false;
                        }
                    }
                }

                if (proceed)
                {
                    if (handle1 || handle2 || handle3)
                    {
                        cout << "DataHandle update after controller release failed" << endl;
                        proceed = false;
                    }
                }
            }

            if (proceed)
            {
                DataController<Child> child(new Child());
                child->foo = true;
                child->bar = 420;
                DataHandle<Parent> parent1(child), parent2 = parent1;
                if (!parent1 || !parent2 || !parent1->foo || !parent2->foo || parent1->bar != 420 || parent2->bar != 420)
                {
                    cout << "DataHandle upcast failed" << endl;
                    proceed = false;
                }
            }

            if (proceed)
            {
                DataController<Parent> parent(new Child());
                parent->foo = false;
                parent->bar = 240;
                DataHandle<Child> child1(parent), child2 = child1;
                if (!child1 || !child2 || child1->foo || child2->foo || child1->bar != 240 || child2->bar != 240)
                {
                    cout << "DataHandle downcast failed" << endl;
                    proceed = false;
                }
            }

            if (proceed)
            {
                Child *child = new Child();
                child->foo = true;
                child->bar = 430;
                DataController<Child> controller(child);
                DataHandle<Child> handle(child);

                if (!handle || !handle->foo || handle->bar != 430)
                {
                    cout << "DataHandle creation from restored DataController failed" << endl;
                    proceed = false;
                }
            }

            cout << "DataControl UnitTest " << (proceed ? "successful" : "failed") << endl;
        }
    }
}
