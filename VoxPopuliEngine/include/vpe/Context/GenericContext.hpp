#ifndef VPE_GENERICCONTEXT_HPP
#define VPE_GENERICCONTEXT_HPP

#include "vpe.h"
#include <string>

namespace vpe {
	namespace Context
	{
		class VPE_API GenericContext
		{
		protected:
			const std::string name;

		public:
			GenericContext(const std::string& name) : name(name) {}
			GenericContext(const GenericContext&) = delete;
			GenericContext(GenericContext&&) = default;
			GenericContext& operator=(const GenericContext&) = delete;
			virtual GenericContext& operator=(GenericContext&&) = default;
			virtual ~GenericContext() = default;

		public:
			/*! \brief Gets the name of the context. */
			virtual std::string getName() const { return name; }
		};
	}
}

#endif
