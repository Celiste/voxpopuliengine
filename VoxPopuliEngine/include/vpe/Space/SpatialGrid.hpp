#ifndef VPE_SPACEGRID_HPP
#define VPE_SPACEGRID_HPP

/*! \file
	\brief Provides the SpatialGrid which indexes entities by their spatial coordinates.
	\author Celiste aka. Kiruse
 */

#include "vpe.h"
#include "Core/Integrals.hpp"
#include "Core/SemiSmartPointer.hpp"

namespace vpe
{
	class Entity;

	/*! \brief Indexes entities by their spatial coordinates for quicker proximity lookup. */
	class VPE_API SpatialGrid
	{
	private:
		size_t width;
		size_t height;
		size_t depth;

		/* Buffer of size width * height * depth */
		//WeakPointer<Entity>* buffer;

	public:
		SpatialGrid(size_t count = 100) : SpatialGrid(count, count, count) {}
		SpatialGrid(size_t width, size_t height, size_t depth);
		SpatialGrid(const SpatialGrid&);
		SpatialGrid(SpatialGrid&&);
		SpatialGrid& operator=(const SpatialGrid& copy)
		{
			this->~SpatialGrid();
			new (this) SpatialGrid(copy);
			return *this;
		}
		SpatialGrid& operator=(SpatialGrid&& move)
		{
			this->~SpatialGrid();
			new (this) SpatialGrid(std::move(move));
			return *this;
		}
		~SpatialGrid();

	public:
		/*! \brief Gets the width of this grid in number of elements. */
		size_t getWidth() const
		{
			return width;
		}

		/*! \brief Gets the height of this grid in number of elements. */
		size_t getHeight() const
		{
			return height;
		}

		/*! \brief Gets the depth of this grid in number of elements. */
		size_t getDepth() const
		{
			return depth;
		}

		/*! \brief Convenience method to get the width, height and depth of this grid in one call. */
		void getDimensions(size_t& width, size_t& height, size_t& depth) const
		{
			width = this->width;
			height = this->height;
			depth = this->depth;
		}

	public:

	};
}

#endif
