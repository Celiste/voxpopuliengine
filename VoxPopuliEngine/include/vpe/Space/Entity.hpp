#ifndef VPE_ENTITY_HPP
#define VPE_ENTITY_HPP

/*! \file
	\brief Provides the base scene entity class.
	\todo Develop "box-in" microlibrary
 */

#include "vpe.h"
#include <vector>
#include "Core/Delegate.hpp"
#include "Core/HashSet.hpp"
#include "Core/Integrals.hpp"
#include "Core/DataControl.hpp"
#include "Components/ComponentBase.hpp"

namespace vpe
{
	class Camera2DComponent;
	class ScriptComponent;
	class RenderDataComponent;
}

namespace vpe
{
	class World;

	/*! \brief Defines an entity placeable in 3D space. */
	class VPE_API Entity
	{
		//template<class CompType> using CompSet = HashSet<WeakPointer<CompType>>;

	protected:
		/*! \brief Stores all components of this entity.
			\details Components in this hash set may be found in the other specialized hash sets as well.
			\note Wanted this to be a std::vector originally, but STL types require contained data to be copyable, but the SemiSmartPointer is only movable.
		 */
		//HashSet<SemiSmartPointer<ComponentBase>> components;

		/*! \brief All Camera2DComponent's of this entity. Likely empty. */
		//CompSet<Camera2DComponent> cameraComponents;

		/*! \brief All ScriptComponent's of this entity. Usually empty or one element. */
		//CompSet<ScriptComponent> scriptComponents;

		/*! \brief All RenderDataComponent's of this entity providing the visual appearance of this entity. On average one per entity type. */
		//CompSet<RenderDataComponent> renderDataComponents;

		/*! \brief The world in which this entity resides.
			\details The game mode controls the lifetime of the world and the world in turn controls the lifetime of this entity.
		 */
		//WeakPointer<World> world;

	public:
		/*! \brief Constructs a new dangling entity.
			\details A dangling entity is not associated with any world/scene and will thus never be rendered. These can be
					 useful to use as prefabs.
		 */
		Entity() {}

		/*! \brief Constructs a new entity within the given world.
			\details The world can be changed later, but leaving a world and entering a new one may trigger a payload.
		 */
		//Entity(WeakPointer<World> world) : world(world) {}

		virtual ~Entity() = default;

	protected: // Tools for derived classes

		/*! \brief Adds the specified component to this entity's component list.
			\details Although the components collection is exposed to derived classes as well,
					 internal optimizations use HashSet's to more quickly gather commonly used
					 components such as RenderDataComponent's, CameraComponent's and
					 ScriptComponent's. Using this method automatically adds them to the various
					 sets.
			\note Since this method uses dynamic_cast, performance may rarely become a concern.
				  In that case, adding the component manually is recommended.
		 */
		//virtual WeakPointer<ComponentBase> addComponent(SemiSmartPointer<ComponentBase>&&);

		/*! \brief Removes a component from this entity.
			\details Respective counterpart of addComponent(SemiSmartPointer<ComponentBase>).
					 Accordingly it automatically removes a component from the various
					 optimization sets.
			\note Since this method uses dynamic_cast, performance may rarely become a concern.
				  In that case, adding the component manually is recommended.
		 */
		//virtual void removeComponent(WeakPointer<ComponentBase>);

	public: // Public interface

		/*! \brief Gets the world this entity resides in.
			\note May be invalid if the entity is dangling!
		 */
		/*WeakPointer<World> getWorld() const
		{
			return world;
		}*/

		/*! \brief Causes this entity to migrate to another world.
			\note When overriding this method, preferrably call it at the end of your override.
		 */
		//virtual void migrateTo(WeakPointer<World>);

		/*! \brief Gets all components of this entity. */
		//const HashSet<SemiSmartPointer<ComponentBase>>& getComponents() const { return components; }

		/*! \brief Gets all components representing a camera (and associated configuration). */
		//const HashSet<WeakPointer<Camera2DComponent>>& getCameraComponents() const { return cameraComponents; }

		/*! \brief Gets all components providing update logic. */
		//const HashSet<WeakPointer<ScriptComponent>>& getScriptComponents() const { return scriptComponents; }

		/*! \brief Gets all components providing render information. */
		//const HashSet<WeakPointer<RenderDataComponent>>& getRenderDataComponents() const { return renderDataComponents; }

	public:
		/*! \brief The event has left its current world for a new one.
			\details The first argument is a WeakPointer to this entity, the second resembles the source world
					 and the last the target world.
		 */
		//EventEmitter<Entity, Event, WeakPointer<Entity>, WeakPointer<World>, WeakPointer<World>> onWorldChanged;
	};
}

#endif
