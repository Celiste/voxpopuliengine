#ifndef VPE_WORLD_HPP
#define VPE_WORLD_HPP

/*! \file
	\brief Provides the declaration of the World, which resembles the root of a scene.
	\author Brian Cobile aka. Kiruse aka. Celiste
 */

#include "Core/HashSet.hpp"
#include "Core/DataControl.hpp"

namespace vpe
{
	class Entity;

	/*! \brief The root of a scene. Keeps track of the various entities in the scene to update and to render.
        \todo Add copy constructor and assignment.
	 */
	class VPE_API World
	{
	private:
		//HashSet<SemiSmartPointer<Entity>> entities;
		//HashSet<WeakPointer<Entity>> roots;
		//HashSet<WeakPointer<Entity>> updatableRoots;
		//HashSet<WeakPointer<Entity>> renderableRoots;

	public:
	    World() = default;
	    World(const World&) = delete;
	    World(World&&) = default;
	    virtual World& operator=(const World&) = default;
	    virtual World& operator=(World&&) = default;
	    virtual ~World() = default;

		/*! \brief Spawns the given entity inside the world as a child of parent.
			\details
			\parblock
			Being a child of parent, the entity will only receive updates when the parent does, and will only
			be rendered when the parent is.

			If no parent is specified (by default) or the parent is invalid, the entity is stored as a "root".
			Roots are updated if configured and rendered when in view.

			To ensure moderate control over the lifetime of an entity, this takes the semi-smart pointer
			to an entity and expects no other semi-smart pointer instances to this entity have been created.
			All other entity-related functions will thus take a weak pointer.
			\endparblock
		 */
		//WeakPointer<Entity> spawn(SemiSmartPointer<Entity> ntt, WeakPointer<Entity> parent = WeakPointer<Entity>());

		/*! \brief Despawns the given entity from the world.
			\details The entity will be removed from all lists and deleted when appropriate.
		 */
		//virtual void despawn(WeakPointer<Entity> ntt);
	};
}

#endif
