#ifndef VPE_GAMEMODE_HPP
#define VPE_GAMEMODE_HPP

/*! \file
	\author Brian Cobile aka. Kiruse aka. Celiste
	\brief Declares the GameMode base class which handles the logic of the VPE state machine.
 */

#include "vpe.h"
#include <memory>
#include "Screen.hpp"
#include <SFML/System/Time.hpp>

namespace vpe
{
	class Engine;

	/*! \brief The base class for all game modes which handle the logic of the state machine.
		\details A GameMode comes with a single predefined Screen which will be rendered.
				 This screen may provide the UI, the 3D render, or both.
	 */
	class VPE_API GameMode
	{
		friend class Engine;

	protected:
		/*! \brief Current screen to render. */
		std::shared_ptr<Screen> screen;
		std::shared_ptr<GameMode> nextGameMode;

	public:
		GameMode() = default;
		GameMode(const GameMode&) = default;
		GameMode(GameMode&&) = default;
		virtual GameMode& operator=(const GameMode&) = default;
		virtual GameMode& operator=(GameMode&&) = default;
		virtual ~GameMode() = default;

	protected:
		/*! \brief Called when the game mode is initialized by the engine before transitioned to. */
		virtual void initialize() = 0;

		/*! \brief Called when the nextGameMode is not nullptr by the engine before transitioning. */
		virtual void abandon() = 0;

	public:
		/*! \brief Gets the current screen to render for this gamemode. */
		virtual std::shared_ptr<Screen> getScreen() const { return screen; }

		/*! \brief Gets the next game mode to which to transition. The engine will handle the rest. */
		virtual std::shared_ptr<GameMode> getNextGameMode() const { return nextGameMode; }

		/*! \brief Update logic for the game mode. */
		virtual void update(const sf::Time& deltaTime);
	};
}

template class VPE_API std::shared_ptr<vpe::GameMode>;

#endif
