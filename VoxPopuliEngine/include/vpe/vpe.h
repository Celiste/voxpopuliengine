#ifndef VPE_PCH
#define VPE_PCH

#ifdef _MSC_VER

/*! \file
	\author	 Brian Cobile aka. Kiruse aka. Celiste
	\brief   "Precompiled header" for the general parts of the engine. Not really regarded as PCH by the compiler tho.
	\todo	 Look into how precompiled headers work under Visual Studio, possibly Borland C, and GNU GCC.
 */

/* Define VPE_API */
# ifdef VPE_EXPORT
#  define VPE_API __declspec(dllexport)
# else
#  define VPE_API __declspec(dllimport)
# endif

/* Under MSVC we'll need to export STD classes in use, but under GCC this will result in a warning */
# define VPE_EXPORT_FOREIGN(code) code

/* Disable certain MSVC warnings */
/* --- non dll-interface class ... used as base for dll-interface class ... ---
   IMPORTANT: This warning can only be ignored for standard classes because we require our programmers
   or users to use the same compiler as us! Alternatively we could just distribute the source code and
   have them compile the engine from source, which nearly guarantees the same version.
 */
# pragma warning(disable: 4275)

/* --- class ... needs to have dll-interface to be used by clients of class ...
   Same as with above.
 */
# pragma warning(disable: 4251)

/* --- destructor was implicitly defined as deleted ---
   Mostly occurs when inheriting from StaticClass, which is intentional to prevent instantiating the
   class to begin with.
 */
# pragma warning(disable: 4624)

#else
/* No VPE_API or shared lib exporting under UNIX because UNIX, that's why */
# define VPE_API
# define VPE_EXPORT_FOREIGN(...)
#endif

#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)

/* --- Use Preprocessor Constants to detect Endianness --- Works on most systems --- */
#if !defined(LITTLE_ENDIAN) && !defined(BIG_ENDIAN)
# if _MSC_VER
#   define LITTLE_ENDIAN
# else
#  if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#   define LITTLE_ENDIAN
#  else
#   define BIG_ENDIAN
#  endif
# endif
#endif

/*! \brief A helpful define to implement simple proxy methods in a class to get iterators of a collection.
	\details The collection must be an actual variable or member. 12 functions are generated following this
			 pattern: (c|r|cr)(begin|end)_<collection>() (const) with const variants for the begin and
			 end methods.
 */
#define IMPL_COLLECTION_ITERATOR(collection) \
	inline decltype(std::begin(collection)) begin_##collection() { return std::begin(collection); } \
	inline decltype(std::cbegin(collection)) begin_##collection() const { return std::cbegin(collection); } \
	inline decltype(std::cbegin(collection)) cbegin_##collection() const { return std::cbegin(collection); } \
	inline decltype(std::rbegin(collection)) rbegin_##collection() { return std::rbegin(collection); } \
	inline decltype(std::crbegin(collection)) rbegin_##collection() const { return std::crbegin(collection); } \
	inline decltype(std::crbegin(collection)) crbegin_##collection() const { return std::crbegin(collection); } \
	inline decltype(std::end(collection)) end_##collection() { return std::end(collection); } \
	inline decltype(std::cend(collection)) end_##collection() const { return std::cend(collection); } \
	inline decltype(std::cend(collection)) cend_##collection() const { return std::cend(collection); } \
	inline decltype(std::rend(collection)) rend_##collection() { return std::rend(collection); } \
	inline decltype(std::crend(collection)) rend_##collection() const { return std::crend(collection); } \
	inline decltype(std::crend(collection)) crend_##collection() const { return std::crend(collection); }

#endif
