#ifndef VPE_SCREEN_HPP
#define VPE_SCREEN_HPP

/*! \file
	\author Brian Cobile aka. Kiruse aka. Celiste
	\brief Renders the UI and provides various events to handle UI IO.
 */

#include "vpe.h"
#include <memory>
#include <SFML/System/Time.hpp>

namespace vpe
{
	/*! \brief Base class for UI screens.
		\todo Awaits Janis' 2D subsystem using SFML's graphics subsystem.
	 */
	class Screen
	{
	public:
		Screen() = default;
		Screen(const Screen&) = default;
		Screen(Screen&&) = default;
		virtual Screen& operator=(const Screen&) = default;
		virtual Screen& operator=(Screen&&) = default;
		virtual ~Screen() = default;

	public:
		virtual void initialize() = 0;

	public:
		/*! \brief Updates screen render logic for the new frame.
			\details This should not include IO logic like button presses. At most it should
					 detect button presses. The GameMode classes should be the only instances
					 handling any kind of logic.
		 */
		virtual void update(const sf::Time& deltaTime) {}

		/*! \brief Renders the screen for the current frame. */
		virtual void render() {}
	};
}

template class VPE_API std::shared_ptr<vpe::Screen>;

#endif
