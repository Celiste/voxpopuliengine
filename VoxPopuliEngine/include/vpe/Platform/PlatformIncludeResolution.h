#ifndef VPE_PLATFORMINCLUDERESOLUTION_H
#define VPE_PLATFORMINCLUDERESOLUTION_H

/*! \file
    \brief Defines the platform include directory constant.
	\see https://sourceforge.net/p/predef/wiki/OperatingSystems/
 */

#ifdef _MSC_VER
# define PLATFORM_INCLUDE_DIR "Windows"
#else
# error "VPE currently only supports Windows"
#endif

#ifndef USE_STL
# error "VPE currently requires STL"
#endif

#endif
