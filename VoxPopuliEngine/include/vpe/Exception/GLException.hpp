#ifndef VPE_GLEXCEPTION_HPP
#define VPE_GLEXCEPTION_HPP

#include <string>
#include <sstream>
#include "Graphics/internal/graphics.h"
#include "Exception.hpp"

/*! \brief Checks if OpenGL encountered an error and raises a generic exception. */
#define GLExceptionRaiseIfError() { GLenum glError = glGetError(); if (glError != GL_NO_ERROR) throw GLException(glError); }
/*! \brief Checks if OpenGL encountered an error and raises a customized exception message. */
#define GLExceptionRaiseMessageIfError(message) { GLenum glError = glGetError(); if (glError != GL_NO_ERROR) throw GLException(glError, message); }

/*! \brief Checks if OpenGL encountered an error, throws a generic exception, and allows code to be run in between this and GLExceptionRaiseIfErrorBlockEnd macro.
	\details The error number will be stored in a local variable named glError.
 */
#define GLExceptionRaiseIfErrorBlockStart() { GLenum glError = glGetError(); if (glError != GL_NO_ERROR) {
/*! \brief Ends the GLExceptionRaiseIfErrorBlockStart block. */
#define GLExceptionRaiseIfErrorBlockEnd() throw GLException(glError); } }

/*! \brief Checks if OpenGL encountered an error, throws a generic exception, and allows code to be run in between this and GLExceptionRaiseIfErrorBlockEnd macro.
	\details The error number will be stored in a local variable named glError. An std::ostringstream message is initiated as another local variable which will be
			 used to determine the error message. If you don't need this, prefer the GLExceptionRaiseIfErrorBlockStart macro instead. GLException automatically
			 appends the string: "(Error code: <glError>)".
 */
#define GLExceptionRaiseMessageIfErrorBlockStart() { GLenum glError = glGetError(); if (glError != GL_NO_ERROR) { std::ostringstream message;
/*! \brief Ends the GLExceptionRaiseMessageIfErrorBlockStart block. */
#define GLExceptionRaiseMessageIfErrorBlockEnd() throw GLException(glError, message.str()); }}

namespace vpe
{
	class GLException : public Exception
	{
	protected:
		GLenum errorCode;

	public:
		GLException(GLenum errorCode) : Exception("GLException", getMessage(errorCode)), errorCode(errorCode) {}
		GLException(GLenum errorCode, std::string message) : Exception("GLException", getMessage(message, errorCode)), errorCode(errorCode) {}

	private:
		static std::string getMessage(GLenum errorCode)
		{
			std::ostringstream stream;
			stream << "OpenGL encountered an error: " << errorCode;
			return stream.str();
		}

		static std::string getMessage(std::string message, GLenum errorCode)
		{
			std::ostringstream stream;
			stream << message << " (Error code " << errorCode << ")";
			return stream.str();
		}
	};
}

#endif
