#ifndef VPE_EXCEPTIONBASE_HPP
#define VPE_EXCEPTIONBASE_HPP

#include "vpe.h"
#include <exception>
#include <string>

#define IMPL_SIMPLE_EXCEPTION(x_class, p_class) \
	class x_class : public p_class { \
	public: \
		x_class() : p_class() {} \
		x_class(const std::string& message) : p_class(#x_class, message) {} \
		x_class(const std::string& message, const Exception* cause) : p_class(#x_class, message, cause) {} \
		\
	protected: \
		x_class(const std::string& name, const std::string& message) : p_class(name, message) {} \
		x_class(const std::string& name, const std::string& message, const Exception* cause) : p_class(name, message, cause) {} \
		\
	public: \
		x_class(const x_class&) = default; \
		x_class(x_class&&) = default; \
		virtual x_class& operator=(const x_class&) = default; \
		virtual x_class& operator=(x_class&&) = default; \
		~x_class() = default; \
	};

#define IMPL_SIMPLE_API_EXCEPTION(x_class, p_class) \
	class VPE_API x_class : public p_class { \
	public: \
		x_class() : p_class() {} \
		x_class(const std::string& message) : p_class(#x_class, message) {} \
		x_class(const std::string& message, const Exception* cause) : p_class(#x_class, message, cause) {} \
		\
	protected: \
		x_class(const std::string& name, const std::string& message) : p_class(name, message) {} \
		x_class(const std::string& name, const std::string& message, const Exception* cause) : p_class(name, message, cause) {} \
		\
	public: \
		x_class(const x_class&) = default; \
		x_class(x_class&&) = default; \
		virtual x_class& operator=(const x_class&) = default; \
		virtual x_class& operator=(x_class&&) = default; \
		~x_class() = default; \
	};

namespace vpe
{
	/*! \brief The base class for all exceptions in the VPEngine. */
	class VPE_API Exception : public std::exception
	{
	    typedef std::exception Base;

	protected:
		/*! \brief The name of this exception. */
		std::string _name = "Exception";

		/*! \brief The previous exception which caused this exception. */
		const Exception* _cause = nullptr;

		/*! \brief The exception's message. */
		std::string message = "";

	public:
		Exception() noexcept : Base() {}
		Exception(const std::string& message) noexcept : Base(), message(message) {}
		Exception(const std::string& message, const Exception* cause) noexcept : Base(), message(message), _cause(cause) {}

	protected:
		Exception(const std::string& name, const std::string& message) noexcept : Base(), message(message), _name(name) {}
		Exception(const std::string& name, const std::string& message, const Exception* cause) noexcept : Base(), message(message), _name(name), _cause(cause) {}

	public:
		Exception(const Exception&) noexcept = default;
		Exception(Exception&&) noexcept = default;
		virtual Exception& operator=(const Exception&) = default;
		virtual Exception& operator=(Exception&&) = default;
		virtual ~Exception() noexcept = default;

	public:
		/*! \brief Gets the name of this exception. */
		std::string name() const noexcept { return _name; }

		const char* what() const noexcept override { return message.c_str(); }

		/*! \brief Gets the previous exception that caused this exception. */
		const Exception* cause() const noexcept { return _cause; }
	};

	IMPL_SIMPLE_API_EXCEPTION(InvalidArgumentException, Exception)
	IMPL_SIMPLE_API_EXCEPTION(InvalidStateException, Exception)
	IMPL_SIMPLE_API_EXCEPTION(InitializationException, InvalidStateException)
	IMPL_SIMPLE_API_EXCEPTION(NotImplementedException, Exception)
	IMPL_SIMPLE_API_EXCEPTION(NotSupportedException, Exception)
	IMPL_SIMPLE_API_EXCEPTION(IndexOutOfRangeException, Exception)
}

#endif
