#ifndef VPE_Camera2DComponent_HPP
#define VPE_Camera2DComponent_HPP

/*! \file
	\brief Provides the camera component(s) that the SceneRenderer requires.
	\author Brian Cobile aka. Kiruse aka. Celiste
 */

#include "vpe.h"
#include "SpatialComponent.hpp"

namespace vpe
{
	/*! \brief Provides information for the SceneRenderer to determine the portion of a 2D scene to render.
		\details Should work with 3D scenes (once the engine is sophisticated enough) but works best with plain 2D scenes.
	 */
	class VPE_API Camera2DComponent : public SpatialComponent
	{
	protected:
		/*! \brief Dimensions of this camera's view area. */
		//Dimension2 dims;

	public:
		//Camera2DComponent(WeakPointer<Entity> entity) : SpatialComponent(entity) {}
		Camera2DComponent(const Camera2DComponent&) = default;
		Camera2DComponent(Camera2DComponent&&) = default;
		Camera2DComponent& operator=(const Camera2DComponent&) = default;
		Camera2DComponent& operator=(Camera2DComponent&&) = default;
		~Camera2DComponent() = default;

	public:
		/*! \brief Gets the configured width of the camera's render area. */
		//virtual inline int getCameraRenderWidth() const { return dims.x; }

		/*! \brief Gets the configured height of the camera's render area. */
		//virtual inline int getCameraRenderHeight() const { return dims.y; }

		/*! \brief Gets the configured width and height of the camera's render area. */
		//virtual inline Dimension2 getCameraRenderDimensions() const { return dims; }
	};
}

#endif
