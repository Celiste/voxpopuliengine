#ifndef VPE_SCRIPTCOMPONENT_HPP
#define VPE_SCRIPTCOMPONENT_HPP

/*! \file
	\brief Provides the base class for logic containing entity components at a specific but approximated tick interval.
	\author Brian Cobile aka. Kiruse aka. Celiste
 */

#include "vpe.h"
#include "ComponentBase.hpp"
#include "Core/Integrals.hpp"
#include <SFML/System.hpp>

namespace vpe
{
	/*! \brief A component containing a tick method to execute after an approximate tick interval. */
	class VPE_API ScriptComponent : public ComponentBase
	{
	public:
		//ScriptComponent(WeakPointer<Entity> entity) : ComponentBase(entity) {}
		ScriptComponent(const ScriptComponent&) = default;
		ScriptComponent(ScriptComponent&&) = default;
		ScriptComponent& operator=(const ScriptComponent&) = default;
		ScriptComponent& operator=(ScriptComponent&&) = default;
		~ScriptComponent() = default;

	public:
		/*! \brief Gets the sf::Time representing the approximated interval in which this script component is intended to tick/update.
			\note This is intended to be a constant as it will be used to optimize update queues. It's intended to retrieve this value
				  only once upon spawning the entity in the world.
		 */
		virtual sf::Time getTickInterval() const { return sf::Time::Zero; }

		/*! \brief Performs the script's logic once.
			\details Will be called approximately after every elapsed tick interval.
		 */
		virtual void tick(const sf::Time& frameDeltaTime) = 0;
	};
}

#endif
