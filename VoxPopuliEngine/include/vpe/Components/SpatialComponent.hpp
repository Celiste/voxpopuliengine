#ifndef VPE_SPATIALCOMPONENT_HPP
#define VPE_SPATIALCOMPONENT_HPP

/*! \file
	\brief Provides the base for components with spatial properties.
	\author Brian Cobile aka. Kiruse aka. Celiste
 */

#include "vpe.h"
#include "ScriptComponent.hpp"
#include "Core/Math.hpp"
#include <SFML/Graphics/Transform.hpp>

namespace vpe
{
	class VPE_API SpatialComponent : public ScriptComponent
	{
	private:
		/*! \brief Whether the spatial parameters have changed and need recalculation. */
		bool dirty;

		/*! \brief Relative position. */
		//Vector2 position;

		/*! \brief Relative rotation. */
		float rotation;

		/*! \brief Relative scale. */
		//Vector2 scale;

		/*! \brief Cached transform to simplify subordination. */
		//sf::Transform cachedTransform;

	protected:
		//SpatialComponent(WeakPointer<Entity> entity) : ScriptComponent(entity) {}
		SpatialComponent(const SpatialComponent&) = default;
		SpatialComponent(SpatialComponent&&) = default;
		SpatialComponent& operator=(const SpatialComponent&) = default;
		SpatialComponent& operator=(SpatialComponent&&) = default;
		~SpatialComponent() = default;

	public:
		virtual void tick(const sf::Time&) override;

		/*! \brief Marks this spatial component as dirty to enforce recalculating its cached transform. */
		virtual inline void markDirty() { dirty = true; }

		/*! \brief Checks if this spatial component is considered dirty, i.e. any of its spatial parameters have changed. */
		virtual inline bool isDirty() { return dirty; }

		/*! \brief Sets the component's new position. */
		//virtual inline void setPosition(const Vector2& pos) { dirty = true; position = pos; }

		/*! \brief Sets the component's new rotation. */
		virtual inline void setRotation(float rot) { dirty = true; rotation = rot; }

		/*! \brief Sets the component's new scale. */
		//virtual inline void setScale(const Vector2& scale) { dirty = true; this->scale = scale; }
	};
}

#endif
