#ifndef VPE_RENDERDATACOMPONENT_HPP
#define VPE_RENDERDATACOMPONENT_HPP

/*! \file
	\brief Provides the base class for components providing render data in a relatively generic format.
	\note Currently for the purpose of this project provides no data and the renderer is specialized
		  for the SpriteComponent. Later I plan for this to provide a camera mode (orthogonal or perspective)
		  and vertices together with either UV coordinates and a texture, or vertex colors. Support for
		  stencil shapes is planned for orthogonal/2D mode only.
	\author Brian Cobile aka. Kiruse aka. Celiste
 */

#include "vpe.h"
#include "ComponentBase.hpp"

namespace vpe
{
	class RenderDataComponent : public ComponentBase
	{
	protected:
		//RenderDataComponent(WeakPointer<Entity> entity) : ComponentBase(entity) {}
		RenderDataComponent(const RenderDataComponent&) = default;
		RenderDataComponent(RenderDataComponent&&) = default;
		RenderDataComponent& operator=(const RenderDataComponent&) = default;
		RenderDataComponent& operator=(RenderDataComponent&&) = default;
		~RenderDataComponent() = default;
	};
}

#endif
