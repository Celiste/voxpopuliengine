#ifndef VPE_SPRITECOMPONENT_HPP
#define VPE_SPRITECOMPONENT_HPP

/*! \file
	\brief Provides a general purpose and animation supporting render data component.
	\author Brian Cobile aka. Kiruse aka. Celiste
 */

#include "RenderDataComponent.hpp"
#include <SFML/Graphics/Sprite.hpp>

namespace vpe
{
	/*! \brief The base class for components with renderable SFML sprites. */
	class VPE_API SpriteComponent : public RenderDataComponent
	{
	protected:
		sf::Sprite sprite;

	public:
		SpriteComponent(WeakPointer<Entity> entity) : RenderDataComponent(entity) {}
		SpriteComponent(const SpriteComponent&) = default;
		SpriteComponent(SpriteComponent&&) = default;
		SpriteComponent& operator=(const SpriteComponent&) = default;
		SpriteComponent& operator=(SpriteComponent&&) = default;
		~SpriteComponent() = default;

	public:

	};
}

#endif
