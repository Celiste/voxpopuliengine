#ifndef VPE_COMPONENTBASE_HPP
#define VPE_COMPONENTBASE_HPP

/*! \file
	\brief Provides the base class for entity components.
	\details Entity components determine the composition and thus function of an entity. A
			 SpriteComponent for example is used to render a 2D sprite at the position of
			 this entity, while a ScriptComponent provides gameplay logic to be called during
			 various specific ticks.
	\author Brian Cobile aka. Kiruse aka. Celiste aka. Celiste
 */

#include "vpe.h"
#include "Core/DataControl.hpp"

namespace vpe
{
	class Entity;

	class VPE_API ComponentBase
	{
	private:
		/*! \brief The entity to which this component belongs. */
		//WeakPointer<Entity> entity;

	protected:
		//ComponentBase(WeakPointer<Entity> entity) : entity(entity) {}
		ComponentBase(const ComponentBase&) = default;
		ComponentBase(ComponentBase&&) = default;
		ComponentBase& operator=(const ComponentBase&) = default;
		ComponentBase& operator=(ComponentBase&&) = default;
    public:
		virtual ~ComponentBase() = default;

	public:
		//WeakPointer<Entity> getEntity() const { return entity; }
	};
}

#endif
