#ifndef VPE_ENGINE_HPP
#define VPE_ENGINE_HPP

/*! \file
    \author Brian Cobile aka. Kiruse aka. Celiste
	\brief  Declares the VoxPopuli default engine, instantiation and global access interface.
 */

#include "vpe.h"
#include <memory>
#include <vector>
#include "Core/Delegate.hpp"
#include "Core/Simpletons.hpp"
#include "Context/GenericContext.hpp"
#include "GameMode.hpp"
#include "Graphics/GLContext.hpp"
#include "Screen.hpp"

VPE_EXPORT_FOREIGN(template class VPE_API std::vector<sf::Window*>;)
VPE_EXPORT_FOREIGN(template class VPE_API std::shared_ptr<vpe::GameMode>;)
VPE_EXPORT_FOREIGN(template class VPE_API std::unique_ptr<vpe::GameMode>;)
VPE_EXPORT_FOREIGN(template class VPE_API std::shared_ptr<vpe::Screen>;)
VPE_EXPORT_FOREIGN(template class VPE_API std::unique_ptr<vpe::Screen>;)

namespace vpe
{
	class SceneRenderer;

	class VPE_API Engine : public CopyMoveSwitch<false, false>
	{
	private:
		static Engine* inst;

	protected:
		/*! \brief The OpenGL context to which to render the game. */
		Graphics::GLContext* renderTarget;

		/*! \brief The current window to render to. */
		std::vector<Graphics::GLContext*> ctxs;
		volatile bool exitting = false;

		/*! \brief The current game mode of the state machine providing the logic. */
		std::shared_ptr<GameMode> currentGameMode;

		/*! \brief Tracks the total game time since the run method was called. */
		sf::Time totalGameTime;

		/*! \brief The only object that's supposed to be rendering stuff. */
		SceneRenderer* renderer;

	protected:
		Engine() = default;
		~Engine() = default;

	protected:
		/*! \brief Initialization of independent subsystems and routines, for example library initialization. */
		virtual void preinitialize();

		/*! \brief Main initialization of possibly dependent subsystems, for example window creation. */
		virtual void initialize();

		/*! \brief Initialization run after all subsystems and routines have been initialized. */
		virtual void postinitialize();

		/*! \brief Load persistent assets into memory, for example menu buttons, fonts and cursor images. */
		virtual void loadPersistentAssets();

		/*! \brief Clean up resources, i.e. locked files, libraries, etc. */
		virtual void cleanup();

	public:
		/*! \brief Run the main loop of the engine. */
		virtual void run();

		/*! \brief Sets the main render target to which to render the game.
			\details This render target may be a generic, windowless SFML context, or a SFML window. If the context
					 is a window, it is automatically treated as if registered with addContext(Graphics::GLContext*).
			\param[in] ctx The context to render the game to. If nullptr, the game will not be rendered.
			\see addWindow
		 */
		virtual void setRenderTarget(Graphics::GLContext* ctx);

		/*! \brief Adds a graphics context pointer to the internal vector.
			\details
			\parblock
			If the context pointer was previously added, it will be added again. Uniqueness is not enforced.
			These contexts are not automatically rendered to. However, update and render events are emitted
			on the respective contexts which can be used to manually update and render to the context.

			If the context represents a window, SFML system events on that window are forwarded to the respective
			EventEmitter s, as long as the event is not deprecated (currently MouseWheelMoved event).

			The main render target will be partially-automatically updated and rendered to in order to reflect
			the managed 3D scene and UI. See setRenderTarget(Graphics::GLContext*).
			\endparblock
			\see setRenderTarget
				 removeWindow
			\todo Introduce multithreading interface over which the windows can be sourced out into different threads
				  without greatly influencing this partial system.
		 */
		virtual void addContext(Graphics::GLContext* wnd);

		/*! \brief Removes the first occurrence of the passed window pointer from the internal vector. */
		virtual void removeContext(Graphics::GLContext* wnd);

		/*! \brief Initiates graceful termination of this session. */
		virtual inline void exit() { exitting = true; }

		/*! \brief Gets the total game time since the engine's run function was called. */
		inline sf::Time getTotalGameTime() const { return totalGameTime; }

	protected:
		/*! \brief Delegates the SFML events through the corresponding EventEmitters. */
		virtual void delegateSFMLEvents(sf::Event&, sf::Window&) const;

	private:
		void update(const sf::Time&);
		void render();

	public:
		/*! \brief Starts up the engine using the given instance as the globally accessible instance.
		    \details Because the default engine does not come with an initial game mode, this overload
					 can only accept a custom engine instance which initializes with a custom initial
					 game mode. If no initial game mode can be detected during post initialization, the
					 engine will throw an InvalidStateException.
			\exception InvalidStateException An engine session has already been initialized.
		 */
		static void startup(Engine& inst);

		/*! \brief Starts the engine using the given initial game mode.
			\details If no instance is provided, a standard engine will be created.
		 */
		static void startup(std::shared_ptr<GameMode> initialGameMode, Engine* inst = nullptr);

		/*! \brief Just resets the pseudo-singleton instance. Separated out to allow deleting the instance before resetting it. */
		static void reset();

		/*! \brief Gets the current session's engine instance.
		    \exception InvalidStateException Engine has not been initialized. Call startup(Engine*) first.
		 */
		static Engine& instance();

	public: /* --- EVENTS --- */
		EventEmitter<Engine> onStartup;
		EventEmitter<Engine> onShutdown;

		/* --- SFML Window Events --- */
		typedef EventEmitter<Engine, sf::Event, sf::Window&> SFMLWindowEvent;
		template<class EVT> using CustomSFMLWindowEvent = EventEmitter<Engine, EVT, sf::Window&>;

		SFMLWindowEvent onWindowClose;
		CustomSFMLWindowEvent<sf::Event::SizeEvent> onResize;
		SFMLWindowEvent onFocusLost;
		SFMLWindowEvent onFocusGain;
		CustomSFMLWindowEvent<sf::Event::TextEvent> onTextEnter;
		CustomSFMLWindowEvent<sf::Event::KeyEvent> onKeyPress;
		CustomSFMLWindowEvent<sf::Event::KeyEvent> onKeyRelease;
		CustomSFMLWindowEvent<sf::Event::MouseWheelScrollEvent> onMouseWheelScroll;
		CustomSFMLWindowEvent<sf::Event::MouseButtonEvent> onMouseButtonPress;
		CustomSFMLWindowEvent<sf::Event::MouseButtonEvent> onMouseButtonRelease;
		CustomSFMLWindowEvent<sf::Event::MouseMoveEvent> onMouseMove;
		SFMLWindowEvent onMouseEnter;
		SFMLWindowEvent onMouseLeave;
		CustomSFMLWindowEvent<sf::Event::JoystickButtonEvent> onJoystickButtonPress;
		CustomSFMLWindowEvent<sf::Event::JoystickButtonEvent> onJoystickButtonRelease;
		CustomSFMLWindowEvent<sf::Event::JoystickMoveEvent> onJoystickMove;
		CustomSFMLWindowEvent<sf::Event::JoystickConnectEvent> onJoystickConnect;
		CustomSFMLWindowEvent<sf::Event::JoystickConnectEvent> onJoystickDisconnect;
		CustomSFMLWindowEvent<sf::Event::TouchEvent> onTouchBegin;
		CustomSFMLWindowEvent<sf::Event::TouchEvent> onTouchMove;
		CustomSFMLWindowEvent<sf::Event::TouchEvent> onTouchEnd;
		CustomSFMLWindowEvent<sf::Event::SensorEvent> onSensorChange;
	};
}

#endif
