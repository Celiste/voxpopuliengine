#ifndef VPE_TYPETRAITS_HPP
#define VPE_TYPETRAITS_HPP

#include <type_traits>
#include <utility>

namespace vpe
{
	template<bool COND, typename TRUE_TYPE, typename FALSE_TYPE> struct switch_type { typedef FALSE_TYPE type; };
	template<typename TRUE_TYPE, typename FALSE_TYPE> struct switch_type<true, TRUE_TYPE, FALSE_TYPE> { typedef TRUE_TYPE type; };

	template<bool C, bool V, typename T> struct toggle_cv { typedef T type; };
	template<typename T> struct toggle_cv<false, true, T> { typedef volatile T type; };
	template<typename T> struct toggle_cv<true, false, T> { typedef const T type; };
	template<typename T> struct toggle_cv<true,  true, T> { typedef const volatile T type; };

	template<bool C, bool V, class T, typename RET, typename... ARGS> struct toggle_method_cv { typedef RET(T::*type)(ARGS...); };
	template<class T, typename RET, typename... ARGS> struct toggle_method_cv<false, true, T, RET, ARGS...> { typedef RET(T::*type)(ARGS...) volatile; };
	template<class T, typename RET, typename... ARGS> struct toggle_method_cv<true, false, T, RET, ARGS...> { typedef RET(T::*type)(ARGS...) const; };
	template<class T, typename RET, typename... ARGS> struct toggle_method_cv<true,  true, T, RET, ARGS...> { typedef RET(T::*type)(ARGS...) const volatile; };

	template<typename FROM, typename TO>
	struct is_convertible
	{
	private:
		struct true_t { char v; };
		struct false_t { char v[2]; };

		static true_t helper(const TO&);
		static false_t helper(...);

	public:
		static constexpr bool value = sizeof(true_t) == sizeof(decltype(helper(std::declval<FROM>())));
	};
}

#endif
