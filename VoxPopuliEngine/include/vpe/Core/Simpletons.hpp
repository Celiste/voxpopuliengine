#ifndef VPE_SIMPLETONS_HPP
#define VPE_SIMPLETONS_HPP

#include "vpe.h"

namespace vpe
{
    template<bool Copyable, bool Movable>
    class CopyMoveSwitch
    {
    protected:
        CopyMoveSwitch() = default;

    public:
        CopyMoveSwitch(const CopyMoveSwitch&) = default;
        CopyMoveSwitch(CopyMoveSwitch&&) = default;
        CopyMoveSwitch& operator=(const CopyMoveSwitch&) = default;
        CopyMoveSwitch& operator=(CopyMoveSwitch&&) = default;

    protected:
        ~CopyMoveSwitch() = default;
    };

    template<>
    class CopyMoveSwitch<true, false>
    {
    protected:
        CopyMoveSwitch() = default;

    public:
        CopyMoveSwitch(const CopyMoveSwitch&) = default;
        CopyMoveSwitch(CopyMoveSwitch&&) = delete;
        CopyMoveSwitch& operator=(const CopyMoveSwitch&) = default;
        CopyMoveSwitch& operator=(CopyMoveSwitch&&) = delete;

    protected:
        ~CopyMoveSwitch() = default;
    };

    template<>
    class CopyMoveSwitch<false, true>
    {
    protected:
        CopyMoveSwitch() = default;

    public:
        CopyMoveSwitch(const CopyMoveSwitch&) = delete;
        CopyMoveSwitch(CopyMoveSwitch&&) = default;
        CopyMoveSwitch& operator=(const CopyMoveSwitch&) = delete;
        CopyMoveSwitch& operator=(CopyMoveSwitch&&) = default;

    protected:
        ~CopyMoveSwitch() = default;
    };

    template<>
    class CopyMoveSwitch<false, false>
    {
    protected:
        CopyMoveSwitch() = default;

    public:
        CopyMoveSwitch(const CopyMoveSwitch&) = delete;
        CopyMoveSwitch(CopyMoveSwitch&&) = delete;
        CopyMoveSwitch& operator=(const CopyMoveSwitch&) = delete;
        CopyMoveSwitch& operator=(CopyMoveSwitch&&) = delete;

    protected:
        ~CopyMoveSwitch() = default;
    };

	class VPE_API StaticClass
	{
	public:
		StaticClass() = delete;
		StaticClass(const StaticClass&) = delete;
		StaticClass(StaticClass&&) = delete;
		StaticClass& operator=(const StaticClass&) = delete;
		StaticClass& operator=(StaticClass&&) = delete;
		~StaticClass() = delete;
	};
}

#endif
