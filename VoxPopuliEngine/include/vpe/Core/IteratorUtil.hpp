#ifndef VPE_ITERATORUTIL_HPP
#define VPE_ITERATORUTIL_HPP

/*! \file
    \brief Provides various utility functions for iterators.
    \author Celiste aka. Kiruse
 */

#include "vpe.h"
#include <functional>
#include <iterator>

namespace vpe
{
    namespace IteratorUtil
    {
        /*! \brief Searches the range [<tt>from</tt>, <tt>to</tt>) for the first occurrence of \c value.
            \return The iterator pointing to the matched element or \c to if none found.
         */
        template<typename IT, typename T, typename PRED = std::equal_to<T>>
        IT find(const IT& from, const IT& to, const T& value, PRED pred = PRED())
        {
            for (auto it = from; it != to; ++it)
            {
                if (pred(*it, value)) return it;
            }
            return to;
        }
    }
}

#endif // VPE_ITERATORUTIL_HPP
