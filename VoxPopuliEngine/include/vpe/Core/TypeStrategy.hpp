#ifndef VPE_TYPESTRATEGY_HPP
#define VPE_TYPESTRATEGY_HPP

/*! \file
    \brief Provides our equivalent of the standard's allocator for the STL.
    \author Kiruse aka. Celiste
 */

#include <cstdlib>
#include <cstring>
#include "TypeTraits.hpp"
#include "Simpletons.hpp"

namespace vpe
{
    namespace internal
    {
        template<typename T, bool = std::is_standard_layout_v<T>>
        struct TypeStrategyCopy
        {
            void copy(T* target, const T* source, size_t count = 1)
            {
                std::memcpy(target, source, sizeof(T) * count);
            }
        };

        template<typename T>
        struct TypeStrategyCopy<T, false>
        {
            void copy(T* target, const T* source, size_t count = 1)
            {
                for (size_t i = 0; i < count; ++i)
                {
                    new (target + i) T(source[i]);
                }
            }
        };

        template<typename T, bool = std::is_standard_layout_v<T>>
        struct TypeStrategyMove
        {
            void move(T* target, T* source, size_t count = 1)
            {
                std::memmove(target, source, sizeof(T) * count);
            }
        };

        template<typename T>
        struct TypeStrategyMove<T, false>
        {
            void move(T* target, T* source, size_t count = 1)
            {
                for (size_t i = 0; i < count; ++i)
                {
                    new (target + i) T(std::move(source[i]));
                }
            }
        };

        /*! \brief The base class for the TypeStrategy which switches on or off copy and move operations for the strategy.
            \details First boolean flag dictates whether the strategy supports copying, second dictates moving, third further
                     distinguishes the exact method to use.
                     Most standard-layout types can be copied bytewise, however especially those managing external, dynamic
                     buffers cannot and should thus have the last flag set to false.
         */
        template<typename T, bool = std::is_copy_constructible_v<T>, bool = std::is_move_constructible_v<T>, bool = std::is_standard_layout_v<T>>
        struct TypeStrategySwitchBase {};

        template<typename T, bool StandardLayout>
        struct TypeStrategySwitchBase<T, false, true, StandardLayout> : public TypeStrategyMove<T, StandardLayout> {};

        template<typename T, bool StandardLayout>
        struct TypeStrategySwitchBase<T, true, false, StandardLayout> : public TypeStrategyCopy<T, StandardLayout> {};

        template<typename T, bool StandardLayout>
        struct TypeStrategySwitchBase<T, true, true, StandardLayout> : public TypeStrategyCopy<T, StandardLayout>, public TypeStrategyMove<T, StandardLayout> {};
    }

    /*! \brief Defines the strategy to use when working with objects of type T.
        \details Strategy includes (bulk) copying and moving (construction and assignment),
                 allocating on heap and freeing, depending on the type and various
                 configurations.
                 If TreatNonStandardLayout is true, the constexpr bool is_standard_layout will
                 be false as if the data type was not std::is_standard_layout_v<T>. This is useful
                 for types which may be standard layout but should not be copied bytewise nonetheless,
                 usually types which manage one or more heap buffers, thus copying these pointers
                 bytewise would not yield the desired effect of copying the contents of the buffer.
     */
    template<typename T, bool Copyable = std::is_copy_constructible_v<T>, bool Movable = std::is_move_constructible_v<T>, bool StandardLayout = std::is_standard_layout_v<T>>
    struct TypeStrategy : public internal::TypeStrategySwitchBase<T, Copyable, Movable, StandardLayout>
    {
        typedef internal::TypeStrategySwitchBase<T, Copyable, Movable, StandardLayout> Base;
        static constexpr bool is_standard_layout = StandardLayout;
        static constexpr bool can_copy = Copyable;
        static constexpr bool can_move = Movable;

        /*! \brief Allocates a new memory block capable of holding the specified number of elements of this type. */
        T* allocate(size_t count = 1)
        {
            return reinterpret_cast<T*>(std::malloc(count * sizeof(T)));
        }

        /*! \brief Reallocates the given memory block to the specified size.
            \details The memory block may remain the same or may be changed. Frankly this function is useless
                     for most occasions.
         */
        T* reallocate(T* oldBuffer, size_t count = 1)
        {
            return reinterpret_cast<T*>(realloc(oldBuffer, count * sizeof(T)));
        }

        /*! \brief Frees the memory block associated with the target.
            \warn Passing an address in the middle of a memory block leads to undefined behavior.
         */
        void free(T* target)
        {
            std::free(target);
        }

        /*! \brief Calls the constructor on the specified address with the passed args.
            \details If no arguments are passed (besides the address), the call is equal to
                     an invocation of the default constructor.
         */
        template<typename... ARGS>
        void construct(T* target, ARGS... args)
        {
            new (target) T(std::forward<ARGS>(args)...);
        }

        /*! \brief Destructs the passed object.
            \details For most types, simply calling the destructor is sufficient.
         */
        void destruct(T* target)
        {
            target->~T();
        }

        void swap(T* a, T* b, size_t count = 1)
        {
            char bytes[sizeof(T)];
            T* tmp = reinterpret_cast<T*>(bytes);
            Base::move(tmp, a);
            Base::move(a, b);
            Base::move(b, tmp);
        }
    };
}

#endif // VPE_TYPESTRATEGY_HPP
