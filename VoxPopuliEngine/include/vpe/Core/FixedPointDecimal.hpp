#ifndef VPE_FIXEDPOINTDECIMAL_HPP
#define VPE_FIXEDPOINTDECIMAL_HPP

/*! \file
	\brief Provides the Fixed struct representing a fixed point decimal.
	\author Kiruse aka. Celiste
 */

#include "Core/Integrals.hpp"
#include "Core/TypeTraits.hpp"
#include "Core/Math/Math.hpp"

#define GENERATE_NONVARIABLE_INTEGRAL_VARIANTS(return_type, function_name, argument_name, code) \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, char>>::type> \
	return_type function_name(char argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned char>>::type> \
	return_type function_name(unsigned char argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, short> && sizeof(short) <= sizeof(N)>::type> \
	return_type function_name(short argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned short> && sizeof(unsigned short) <= sizeof(N)>::type> \
	return_type function_name(unsigned short argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, int> && sizeof(int) <= sizeof(N)>::type> \
	return_type function_name(int argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned int> && sizeof(unsigned int) <= sizeof(N)>::type> \
	return_type function_name(unsigned int argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, long> && sizeof(long) <= sizeof(N)>::type> \
	return_type function_name(long argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned long> && sizeof(unsigned long) <= sizeof(N)>::type> \
	return_type function_name(unsigned long argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, long long> && sizeof(long long) <= sizeof(N)>::type> \
	return_type function_name(long long argument_name) code \
	template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned long long> && sizeof(unsigned long long) <= sizeof(N)>::type> \
	return_type function_name(unsigned long long argument_name) code

namespace vpe
{
	/*! \brief Represents a fixed point number (named after float).
		\details
		\parblock
		These provide less accuracy than any floating point number in favor of consistency.
		The gap between numbers is homogenous such that calculations are still reliable
		for large numbers. This counters is designed to counter the precision error that
		most engines experience for example during physics calculations when moving far from
		the world origin.

		Four general purpose typedefs are provided:
		 - Fixed (FixedPointDecimal<int32_t, 8>)
		 - LongFixed (FixedPointDecimal<int64_t, 16>)
		 - BigFixed (FixedPointDecimal<vpe::BigInt<128>, 32>)
		 - DynamicFixed (FixedPointDecimal<vpe::BigInt, 0>)
		While Fixed and LongFixed are simple implementations, BigFixed is a specialized implementation
		exposing the same methods based on our BigInt implementation with 128 bits. The DynamicFixed
		is another specialization based on a specialization of the BigInt<0> which allocates more
		memory as its buffer reaches its capacity. The decimals are stored in a separate BigInt<0>
		to achieve the same result.

		When specializing a FixedPointDecimal with a custom NUMERIC type, the following conditions
		must be met:
		 - std::is_integral<NUMERIC>::value evaluates truthfully
		 - Various supported foundational integer types (bool, char, short, int, long, long long)
		   can be cast to NUMERIC implicitly
		 - NUMERIC can be explicitly cast to other foundational integer types, lossy or losslessly.
		\endparblock
		\warning While the structure aims to be as efficient as possible, this library does not
				 have direct hardware support such that multiplication and division is split
				 into various (bit shifting) operations.
		\warning While specializations may be based on int32_t, due to the PrecisionBits count
				 only a value range of -2^23 - 1 to 2^23 (Fixed typedef) is possible. Since bits
				 are simply shifted, numbers beyond that may be unexpectedly truncated.
		\todo Find efficient algorithm to convert double's decimal value into fixed decimal
		\todo Meh. Write algebra functions. :c
	 */
	template<typename NUMERIC, unsigned int PrecisionBits>
	struct FixedPointDecimal
	{
		//static_assert(false, "Not yet implemented :c");
		static_assert(std::is_integral_v<NUMERIC>, "A fixed precision decimal must be based on an integral type");
		static_assert(PrecisionBits < sizeof(NUMERIC) * 8, "Cannot use moar precision bits than the base integral type has");
		static_assert(PrecisionBits, "Why make a fixed point decimal with no precision?");
		static_assert(!std::is_same_v<NUMERIC, bool>, "It don' make no deyum sense to try and create a fixed point decimal based on a boolean!");

		/*! \brief The raw value. In most cases you won't want to tamper with this. */
		NUMERIC raw;

		FixedPointDecimal() = default;

		GENERATE_NONVARIABLE_INTEGRAL_VARIANTS(, FixedPointDecimal, value, : FixedPointDecimal((NUMERIC)value) {})

		FixedPointDecimal(NUMERIC value)
		{
			*this = value;
		}
		FixedPointDecimal(double value)
		{
			*this = value;
		}
		FixedPointDecimal(const FixedPointDecimal&) = default;
		FixedPointDecimal(FixedPointDecimal&&) = default;
		FixedPointDecimal& operator=(const FixedPointDecimal&) = default;
		FixedPointDecimal& operator=(FixedPointDecimal&&) = default;
		~FixedPointDecimal() = default;

		FixedPointDecimal& operator=(NUMERIC value)
		{
			raw = value << PrecisionBits;
			return *this;
		}

		FixedPointDecimal& operator=(double value)
		{
			raw = ((NUMERIC)value) << PrecisionBits;

			// TODO: Haow 2 convert double's decimals most efficiently into fixed point decimal values?
			value -= ((NUMERIC)value);

			for (size_t bit = 1; bit <= PrecisionBits; ++bit)
			{
				double decimal = 1.0 / Math::power2(bit);
				if (value >= decimal)
				{
					raw |= 1 << (PrecisionBits - bit);
					value -= decimal;
				}
			}

			return *this;
		}

		operator bool() const
		{
			return raw;
		}

		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, char>>::type>
		operator char()
		{
			return (char)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned char>>::type>
		operator unsigned char()
		{
			return (unsigned char)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, short>>::type>
		operator short()
		{
			return (short)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned short>>::type>
		operator unsigned short()
		{
			return (unsigned short)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, int>>::type>
		operator int()
		{
			return (int)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned int>>::type>
		operator unsigned int()
		{
			return (unsigned int)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, long>>::type>
		operator long()
		{
			return (long)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned long>>::type>
		operator unsigned long()
		{
			return (unsigned long)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, long long>>::type>
		operator long long()
		{
			return (long long)((NUMERIC)raw);
		}
		template<typename N = NUMERIC, typename = typename std::enable_if<!std::is_same_v<N, unsigned long long>>::type>
		operator unsigned long long()
		{
			return (unsigned long long)((NUMERIC)raw);
		}

		operator NUMERIC() const
		{
			return raw >> PrecisionBits;
		}

		/*! \brief Attempt to convert this number into a floating point number.
			\warning Not sure what the defined behavior is if the requested precision is too large...
		 */
		operator float() const
		{
			// If we can convert to double, we can convert to float as well.
			return (float)((double)*this);
		}

		/*! \brief Attempt to convert this number into a double precision floating point number.
			\warning Not sure what the defined behavior is if the requested precision is too large...
		 */
		operator double() const
		{
			double result = raw >> PrecisionBits;
			for (size_t bit = 1; bit <= PrecisionBits; ++bit)
			{
				NUMERIC digit = (raw >> (PrecisionBits - bit)) & 1;
				result += (double)digit / Math::power2(bit);
			}
			return result;
		}

		// Arithmetic Operators
		FixedPointDecimal& operator+=(const FixedPointDecimal& other)
		{
			raw += other.raw;
			return *this;
		}
		FixedPointDecimal operator+(const FixedPointDecimal& other) const
		{
			return FixedPointDecimal(*this) += other;
		}

		FixedPointDecimal& operator-=(const FixedPointDecimal& other)
		{
			raw -= other.raw;
			return *this;
		}
		FixedPointDecimal operator-(const FixedPointDecimal& other) const
		{
			return FixedPointDecimal(*this) += other;
		}

		FixedPointDecimal& operator*=(const FixedPointDecimal& other)
		{
			// Find the number of "irrelevant" bits (unset bits after the last set bit)
			size_t leastBit = countDecimalBits(), otherLeastBit = other.countDecimalBits(), combinedLeastBit = leastBit + otherLeastBit;
			raw = ((raw >> PrecisionBits - leastBit) * (other.raw >> PrecisionBits - otherLeastBit)) << (PrecisionBits - combinedLeastBit);
			return *this;
		}
		FixedPointDecimal operator*(const FixedPointDecimal& other) const
		{
			return FixedPointDecimal(*this) *= other;
		}

		FixedPointDecimal& operator/=(const FixedPointDecimal& other)
		{
			size_t leastBit = countDecimalBits(), otherLeastBit = other.countDecimalBits();
			raw = (raw << otherLeastBit) / (other.raw << otherLeastBit);
			return *this;
		}
		FixedPointDecimal& operator/(const FixedPointDecimal& other) const
		{
			return FixedPointDecimal(*this) /= other;
		}

		bool operator==(const FixedPointDecimal& other) const
		{
			return raw == other.raw;
		}
		bool operator!=(const FixedPointDecimal& other) const
		{
			return raw != other.raw;
		}

		size_t countIntegralBits() const
		{
			for (size_t bit = sizeof(NUMERIC) - 1; bit > PrecisionBits; --bit)
			{
				if ((raw & (1 << bit))) return bit - PrecisionBits;
			}
			return 0;
		}
		size_t countDecimalBits() const
		{
			for (size_t bit = 0; bit < PrecisionBits; ++bit)
			{
				if (raw & (1 << bit)) return PrecisionBits - bit;
			}
			return 0;
		}
	};

	typedef FixedPointDecimal<int32_t, 8> Fixed;
	typedef FixedPointDecimal<int64_t, 16> LongFixed;
}

#undef GENERATE_NONVARIABLE_INTEGRAL_VARIANTS

#endif
