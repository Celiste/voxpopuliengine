#ifndef VPE_DATACONTROL_HPP
#define VPE_DATACONTROL_HPP

/*! \file
    \brief Provides the DataControl microlibrary, a way to automatically release resources while retaining most control.
    \warning This file uses a lot of theoretical work. The code here is rather dangerous. The templates mostly resemble
             compile-time only differences, in memory they are treated equally. This only works because the data is not
             stored internally, but through a pointer, which is guaranteed to have the same size. In other words, this
             is dangerous and relatively thoroughly tested code!
    \author Celiste aka. Kiruse
 */

#include "HashMap.hpp"
#include "List.hpp"
#include "ScopeLock.hpp"

namespace vpe
{
    template<typename T> class DataController;
    template<typename T> class DataHandle;

    namespace internal
    {
#ifndef VPE_DATACONTROL_SOURCE
        extern
#endif
        HashMap<void*, void*> data_controllers;
    }

    template<typename T>
    class DataController
    {
        template<typename> friend class DataController; // Because essentially all templated classes are designed to be different views on the same data.
        template<typename> friend class DataHandle;     // Because DataHandles can be of different base or child classes

        T* data;
        /* List of handles associated with this controller.
           These handles do not need to actually be of the corresponding type, we're simply treating them as such.
           List is inherently threadsafe on standard-compliant platforms through the internal use of std::atomic_bool.
         */
        List<DataHandle<T>*> handles;

    public:
        DataController() : data(nullptr) {}
        DataController(T* data) : data(data)
        {
            if (data) internal::data_controllers.add(data, this);
        }
        DataController(DataController&& move) : data(move.data), handles(std::move(move.handles))
        {
            if (data)
            {
                internal::data_controllers[data] = this;
                for (auto& handle : handles)
                {
                    handle->controller = this;
                }
            }
        }
        DataController& operator=(const DataController&) = delete;
        DataController& operator=(DataController&& move)
        {
            if (this != &move)
            {
                if (data)
                {
                    internal::data_controllers.remove(data);
                }
                internal::data_controllers[data] = this;
                for (auto& handle : move.handles)
                {
                    handles.append(handle);
                }
            }
            return *this;
        }
        DataController& operator=(T* data)
        {
            delete this->data;
            internal::data_controllers.remove(data);

            this->data = data;
            internal::data_controllers.add(data, this);
            return *this;
        }
        ~DataController()
        {
            if (data)
            {
                internal::data_controllers.remove(data);
                for (auto& handle : handles)
                {
                    handle->controller = nullptr;
                }
                delete data;
                data = nullptr;
            }
        }

        T& operator*() const { return *data; }
        T* operator->() const noexcept { return (T*)*this; }
        operator T*() const noexcept { return data; }

    private:
        /* EVIL. Do NOT call directly! Only call through DataHandle<T>::restore(T*)! */
        static DataController* restore(T* data) { return internal::data_controllers.has(data) ? reinterpret_cast<DataController<T>*>(internal::data_controllers[data]) : nullptr; }
    };

    template<typename T>
    class DataHandle
    {
        // Because the DataController may be of a base or child class
        template<typename> friend class DataController;
        template<typename> friend class DataHandle;

        DataController<T>* controller;
        typename List<DataHandle<T>*>::Iterator it; // The iterator pointing to this handle in the controller's collection. For quicker removal.

    public:
        DataHandle() : controller(nullptr) {}
        DataHandle(T* data) : controller(DataController<T>::restore(data))
        {
            if (controller)
            {
                do_register();
            }
        }
        DataHandle(const DataHandle& copy) : controller(copy.controller)
        {
            do_register();
        }
        DataHandle(DataHandle&& move) : controller(move.controller), it(std::move(move.it))
        {
            // No need to undo_register the other and re-do_register this - that would be a waste of resources.
            // We'll just reuse what already exists.
            *it = this;
            move.controller = nullptr;
        }
        DataHandle(DataController<T>& ctrl) : controller(&ctrl)
        {
            do_register();
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<T, U>, bool> = 0>
        DataHandle(DataController<U>& ctrl) : controller(reinterpret_cast<DataController<T>*>(&ctrl))
        {
            do_register();
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<U, T>, bool> = 0>
        DataHandle(DataController<U>& ctrl) : controller(nullptr)
        {
            T* tmp = dynamic_cast<T*>((U*)ctrl);
            if (tmp)
            {
                controller = reinterpret_cast<DataController<T>*>(&ctrl);
                do_register();
            }
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<T, U>, bool> = 0>
        DataHandle(const DataHandle<U>& handle) : controller(reinterpret_cast<DataController<T>*>(handle.controller))
        {
            do_register();
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<T, U>, bool> = 0>
        DataHandle(DataHandle<U>&& handle) : controller(reinterpret_cast<DataController<T>*>(handle.controller)), it(std::move(handle.it))
        {
            handle.controller = nullptr;
            *it = this;
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<U, T>, bool> = 0>
        DataHandle(const DataHandle<U>& handle) : controller(nullptr)
        {
            T* tmp = dynamic_cast<T*>((U*)handle);
            if (tmp)
            {
                controller = reinterpret_cast<DataController<T>*>(handle.controller);
                do_register();
            }
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<U, T>, bool> = 0>
        DataHandle(DataHandle<U>&& handle) : controller(nullptr)
        {
            T* tmp = dynamic_cast<T*>((U*)handle);
            if (tmp)
            {
                controller = reinterpret_cast<DataController<T>*>(handle.controller);
                it = std::move(handle.it);
                handle.controller = nullptr;
                *it = this;
            }
        }
        DataHandle& operator=(const DataHandle& copy)
        {
            if (this != &copy)
            {
                undo_register();
                controller = copy.controller;
                do_register();
            }
            return *this;
        }
        DataHandle& operator=(DataHandle&& move)
        {
            if (this != &move)
            {
                undo_register();
                controller = move.controller;
                it = std::move(move.it);
                move.controller = nullptr;
                *it = this; // Reuse existing storage
            }
            return *this;
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<T, U>, bool> = 0>
        DataHandle& operator=(DataController<U>& ctrl)
        {
            undo_register();
            controller = ctrl;
            do_register();
            return *this;
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<U, T>, bool> = 0>
        DataHandle& operator=(DataController<U>& ctrl)
        {
            undo_register();
            controller = nullptr;

            T* tmp = dynamic_cast<T*>((U*)ctrl);
            if (tmp)
            {
                controller = ctrl;
                do_register();
            }

            return *this;
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<T, U>, bool> = 0>
        DataHandle& operator=(const DataHandle<U>& handle)
        {
            undo_register();
            controller = handle.controller;
            do_register();
            return *this;
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<T, U>, bool> = 0>
        DataHandle& operator=(DataHandle<U>&& handle)
        {
            undo_register();
            controller = reinterpret_cast<DataController<T>*>(handle.controller);
            handle.controller = nullptr;
            it = std::move(handle.it);
            *it = this;
            return *this;
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<U, T>, bool> = 0>
        DataHandle& operator=(const DataHandle<U>& handle)
        {
            undo_register();

            T* tmp = dynamic_cast<T*>((U*)handle);
            if (tmp)
            {
                controller = reinterpret_cast<DataController<T>*>(handle.controller);
                do_register();
            }

            return *this;
        }
        template<typename U, typename std::enable_if_t<!std::is_same_v<T, U> && std::is_base_of_v<U, T>, bool> = 0>
        DataHandle& operator=(DataHandle<U>&& handle)
        {
            undo_register();

            T* tmp = dynamic_cast<T*>((U*)handle);
            if (tmp)
            {
                controller = reinterpret_cast<DataController<T>*>(handle.controller);
                handle.controller = nullptr;
                it = std::move(handle.it);
                *it = this;
            }

            return *this;
        }
        ~DataHandle()
        {
            undo_register();
        }

        T& operator*() const { return **controller; }
        T* operator->() const noexcept { return (T*)*this; }
        operator T*() const noexcept { return controller ? (T*)*controller : nullptr; }

    private:
        void do_register()
        {
            it = controller->handles.append(this);
        }

        void undo_register()
        {
            if (controller)
            {
                controller->handles.remove(it);
                controller = nullptr;
            }
        }
    };
}

#endif // VPE_DATACONTROL_HPP
