#ifndef VPE_LIST_HPP
#define VPE_LIST_HPP

/*! \file
    \brief Provides an implementation of a double linked list.
    \author Celiste aka. Kiruse
 */

#include <atomic>
#include <functional>
#include <iterator>
#include <utility>
#include "IteratorUtil.hpp"
#include "Semaphore.hpp"
#include "TypeStrategy.hpp"

namespace vpe
{
    namespace internal
    {
        template<typename T, typename ELEM, bool Copyable = std::is_copy_constructible_v<T>>
        struct ListBuffer
        {
            ELEM* first;
            ELEM* last;

            ListBuffer() : first(nullptr), last(nullptr) {}
            ListBuffer(ListBuffer&& move) : first(move.first), last(move.last)
            {
                move.first = move.last = nullptr;
            }
            ListBuffer& operator=(ListBuffer&&) = default;
            ~ListBuffer()
            {
                ELEM* curr = first;
                while (curr)
                {
                    // Cannot be simplified, because either the next element doesn't exist and we're trying to access a nullptr,
                    // or we're trying to access the next element over a deleted and invalid object. Both bad choices.
                    ELEM* next = curr->next;
                    delete curr;
                    curr = next;
                }
            }
        };

        template<typename T, typename ELEM>
        struct ListBuffer<T, ELEM, true> : public ListBuffer<T, ELEM, false>
        {
            typedef ListBuffer<T, ELEM, false> Base;

            ListBuffer() = default;
            ListBuffer(const ListBuffer& copy)
            {
                Base::first = Base::last = new ELEM(*copy.first);

                ELEM* curr = copy.first->next;
                while (curr)
                {
                    ELEM* elem = new ELEM(*curr);
                    Base::last = Base::last->next = elem;
                    curr = curr->next;
                }
            }
            ListBuffer(ListBuffer&&) = default;
            ListBuffer& operator=(const ListBuffer&) = default;
            ListBuffer& operator=(ListBuffer&&) = default;
            ~ListBuffer() = default;
        };
    }

    template<typename T, bool Copyable = std::is_copy_constructible_v<T>>
    struct ListElement
    {
        T value;
        ListElement* prev;
        ListElement* next;
        BinarySemaphore lock;

        ListElement(const T& value, ListElement* prev, ListElement* next) : value(value), prev(prev), next(next) {}
        ListElement(T&& value, ListElement* prev, ListElement* next) : value(std::move(value)), prev(prev), next(next) {}
        ListElement(const ListElement& copy) : value(copy.value), prev(copy.prev), next(copy.next) {}
        ListElement(ListElement&& move) : value(std::move(move.value)), prev(move.prev), next(move.next)
        {
            if (!move.lock.test()) lock.acquire();
            move.prev = move.next = nullptr;
            move.lock.release();
        }
        ListElement& operator=(const ListElement& copy)
        {
            value = copy.value;
            prev = copy.prev;
            next = copy.next;
            return *this;
        }
        ListElement& operator=(ListElement&& move)
        {
            value = std::move(value);
            prev = move.prev;
            next = move.next;
            if (!move.lock.test()) lock.acquire();
            move.prev = move.next = nullptr;
            move.lock.release();
            return *this;
        }
        ~ListElement() = default;
    };

    template<typename T>
    struct ListElement<T, false>
    {
        T value;
        ListElement* prev;
        ListElement* next;
        BinarySemaphore lock;

        ListElement(T&& value, ListElement* prev, ListElement* next) : value(std::move(value)), prev(prev), next(next) {}
        ListElement(ListElement&& move) : value(std::move(value)), prev(move.prev), next(move.next)
        {
            if (!move.lock.test()) lock.acquire();
            move.prev = move.next = nullptr;
            move.lock.release();
        }
        ListElement& operator=(ListElement&& move)
        {
            value = std::move(value);
            prev = move.prev;
            next = move.next;
            if (!move.lock.test()) lock.acquire();
            move.prev = move.next = nullptr;
            move.lock.release();
            return *this;
        }
        ~ListElement() = default;
    };

    template<typename T, typename ELEM = ListElement<T>, typename ELEM_STRAT = TypeStrategy<ELEM>>
    class List
    {
        typedef BinarySemaphore::ScopeLock ScopeLock;

    public:
        template<bool Const, bool Reverse>
        struct IteratorBase : public std::iterator<std::bidirectional_iterator_tag, typename toggle_cv<Const, false, T>::type, int>
        {
            friend class List<T, ELEM, ELEM_STRAT>;

        protected:
            ELEM* curr;

        public:
            IteratorBase() : curr(nullptr) {}
            IteratorBase(ELEM* curr) : curr(curr) {}
            IteratorBase(const IteratorBase&) = default;
            IteratorBase& operator=(const IteratorBase&) = default;

            /*! \brief Copying from a mutable iterator. */
            template<bool C = Const, typename std::enable_if_t<C, bool> = 0>
            IteratorBase(const IteratorBase<false, Reverse>& copy) : curr(copy.curr) {}

            /*! \brief Assignment copying from a mutable iterator. */
            template<bool C = Const, typename std::enable_if_t<C, bool> = 0>
            IteratorBase& operator=(const IteratorBase<false, Reverse>& copy)
            {
                curr = copy.curr;
                return *this;
            }

            virtual ~IteratorBase() = default;

            operator bool() const { return curr; }

            T& operator*() { return curr->value; }
            const T& operator*() const { return curr->value; }
            T* operator->() { return &curr->value; }
            const T* operator->() const { return &curr->value; }

            operator T&() { return curr->value; }
            operator const T&() const { return curr->value; }

            operator ELEM*() { return curr; }
            operator const ELEM*() const { return curr; }

            virtual IteratorBase& operator++() noexcept
            {
                if (curr)
                {
                    ScopeLock{curr->lock};
                    if (!Reverse)
                    {
                        curr = curr->next;
                    }
                    else
                    {
                        curr = curr->prev;
                    }
                }
                return *this;
            }
            IteratorBase operator++(int) const noexcept { return ++IteratorBase(*this); }

            virtual IteratorBase& operator--() noexcept
            {
                if (curr)
                {
                    ScopeLock{curr->lock};
                    if (!Reverse)
                    {
                        curr = curr->prev;
                    }
                    else
                    {
                        curr = curr->next;
                    }
                }
                return *this;
            }
            IteratorBase operator--(int) const noexcept { return --IteratorBase(*this); }

            IteratorBase& operator+=(int distance) noexcept
            {
                if (distance < 0) return *this -= -distance;
                distance = Math::abs(distance);
                for (int i = 0; i < distance; ++i) ++*this;
                return *this;
            }
            IteratorBase operator+(int distance) noexcept { return IteratorBase(*this) += distance; }
            IteratorBase& operator-=(int distance) noexcept
            {
                if (distance < 0) return *this += -distance;
                distance = Math::abs(distance);
                for (int i = 0; i < distance; ++i) --*this;
                return *this;
            }
            IteratorBase operator-(int distance) noexcept { return IteratorBase(*this) -= distance; }

            bool operator==(const IteratorBase& other) const noexcept { return curr == other.curr; }
            bool operator!=(const IteratorBase& other) const noexcept { return !(*this == other); }
        };

        template<bool Const>
        struct IteratorBase<Const, true> : public IteratorBase<Const, false>
        {
        private:
            typedef IteratorBase<Const, false> Base;

        public:
            IteratorBase() = default;
            IteratorBase(ELEM* curr) : Base(curr) {}
            IteratorBase(const IteratorBase&) = default;
            IteratorBase(IteratorBase&&) = default;
            IteratorBase& operator=(const IteratorBase&) = default;
            IteratorBase& operator=(IteratorBase&&) = default;
            ~IteratorBase() = default;

            IteratorBase& operator++() noexcept override
            {
                return Base::operator--();
            }
            IteratorBase& operator--() noexcept override
            {
                return Base::operator++();
            }
        };

        typedef IteratorBase<false, false> Iterator;
        typedef IteratorBase<false, true> RIterator;
        typedef IteratorBase<true, false> ConstIterator;
        typedef IteratorBase<true, true> RConstIterator;

    protected:
        /*! \brief Buffer object copying and/or moving the elements of this list.
            \details Guaranteed to be movable, but not necessarily copyable. This causes the defaulted
                     copy constructor to be deleted if the underlying data type cannot be copied itself.
         */
        internal::ListBuffer<T, ELEM> m_buffer;

        /*! \brief Caches the number of elements in the buffer. */
        size_t m_length;

        /*! \brief Semaphore to use to synchronize threads attempting to override first or last elements of the list. */
        BinarySemaphore borderlock;

    public:
        List() : m_length(0), borderlock() {}
        List(const List& copy) : m_buffer(copy.m_buffer), m_length(copy.m_length), borderlock() {}
        List(List&& move) : m_buffer(std::move(move.m_buffer)), m_length(move.m_length), borderlock()
        {
            if (!move.borderlock.test()) borderlock.acquire();
            move.borderlock.release();
        }
        List& operator=(const List& copy)
        {
            m_buffer = copy.m_buffer;
            m_length = copy.m_length;
            borderlock.release();
            return *this;
        }
        List& operator=(List&& move)
        {
            m_buffer = std::move(move.m_buffer);
            m_length = move.m_length;
            if (move.borderlock.test()) borderlock.release();
            else borderlock.acquire();
            move.borderlock.release();
            return *this;
        }
        ~List() = default;

        //{ List Manipulation
        /*! \brief Prepends the given value to the list through copying.
            \return The iterator pointing to the inserted element.
         */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, Iterator> prepend(const T& value)
        {
            ScopeLock{borderlock};

            ELEM* elem = new ELEM(value, nullptr, m_buffer.first);
            if (!m_buffer.first)
            {
                m_buffer.first = m_buffer.last = elem;
            }
            else
            {
                ScopeLock{m_buffer.first->lock};
                m_buffer.first = m_buffer.first->prev = elem;
            }

            return Iterator(elem);
        }

        /*! \brief Prepends the given value to the list through moving.
            \return The iterator pointing to the inserted element.
         */
        Iterator prepend(T&& value)
        {
            ScopeLock{borderlock};

            ELEM* elem = new ELEM(std::move(value), nullptr, m_buffer.first);
            if (!m_buffer.first)
            {
                m_buffer.first = m_buffer.last = elem;
            }
            else
            {
                ScopeLock{m_buffer.first->lock};
                m_buffer.first = m_buffer.first->prev = elem;
            }
            ++m_length;

            return Iterator(elem);
        }

        /*! \brief Appends the given value to the list through copying.
            \return The iterator pointing to the inserted element.
         */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, Iterator> append(const T& value)
        {
            ScopeLock{borderlock};

            ELEM* elem = new ELEM(value, m_buffer.last, nullptr);
            if (!m_buffer.last)
            {
                m_buffer.first = m_buffer.last = elem;
            }
            else
            {
                ScopeLock{m_buffer.last->lock};
                m_buffer.last = m_buffer.last->next = elem;
            }
            ++m_length;

            return Iterator(elem);
        }

        /*! \brief Appends the given value to the list through copying.
            \return The iterator pointing to the inserted element.
         */
        Iterator append(T&& value)
        {
            ScopeLock{borderlock};

            ELEM* elem = new ELEM(std::move(value), m_buffer.last, nullptr);
            if (!m_buffer.last)
            {
                m_buffer.first = m_buffer.last = elem;
            }
            else
            {
                ScopeLock{m_buffer.last->lock};
                m_buffer.last = m_buffer.last->next = elem;
            }
            ++m_length;

            return Iterator(elem);
        }

        /*! \brief Inserts the given value before the designated element through copying.
            \details Assumes the iterator points to an element in this list.
            \return The iterator representing the inserted element.
         */
        template<typename IT, typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, IT> insertBefore(const IT& target, const T& value)
        {
            ELEM* elem = new ELEM(value, target->prev, *target);
            if (target->prev) target->prev->next = elem;
            target->prev = elem;
            ++m_length;
            return target - 1;
        }

        /*! \brief Inserts the given value before the designated element through copying.
            \details Assumes the iterator points to an element in this list.
            \return The iterator representing the inserted element.
         */
        template<typename IT>
        IT insertBefore(const IT& target, T&& value)
        {
            ELEM* elem = new ELEM(std::move(value), target->prev, *target);
            if (target->prev) target->prev->next = elem;
            target->prev = elem;
            ++m_length;
            return target - 1;
        }

        /*! \brief Inserts the given value after the designated element through copying.
            \details Assumes the iterator points to an element in this list.
            \return The iterator representing the inserted element.
         */
        template<typename IT, typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, IT> insertAfter(IT& target, const T& value)
        {
            ELEM* elem = new ELEM(value, *target, target->next);
            if (target->next) target->next->prev = elem;
            target->next = elem;
            ++m_length;
            return target + 1;
        }

        /*! \brief Inserts the given value after the designated element through copying.
            \details Assumes the iterator points to an element in this list.
            \return The iterator representing the inserted element.
         */
        template<typename IT>
        IT insertAfter(const IT& target, T&& value)
        {
            ELEM* elem = new ELEM(std::move(value), *target, target->next);
            if (target->next) target->next->prev = elem;
            target->next = elem;
            ++m_length;
            return target + 1;
        }

        /*! \brief Removes the designated element.
            \details Assumes the iterator points to a member of this list. A custom iterator
                     must in particular expose a \c curr member variable to this class, provide
                     a copy constructor and be bidirectional.
            \return The previous element, or the next if no previous exists.
         */
        template<typename IT>
        auto remove(const IT& elem) -> decltype(std::enable_if_t<!std::is_same_v<IT, T>, IT>(), elem.curr, IT())
        {
            if (elem.curr == m_buffer.first)
            {
                ScopeLock{borderlock};
                m_buffer.first = elem.curr->next;
            }
            if (elem.curr == m_buffer.last)
            {
                ScopeLock{borderlock};
                m_buffer.last = elem.curr->prev;
            }
            if (elem.curr->prev) elem.curr->prev->next = elem.curr->next;
            if (elem.curr->next) elem.curr->next->prev = elem.curr->prev;

            Iterator target{elem};
            if (elem.curr->prev) --target;
            else ++target;

            delete elem.curr;
            --m_length;

            return target;
        }

        /*! \brief Removes all occurrences of \c value matched by equality. */
        void removeAll(const T& value) { removeAll<>(value); }

        /*! \brief Removes all occurrences of the value matched by the predicate.
            \details The default predicate is std::equal_to<T>.
                     This is simply a proxy which default constructs a functor from the predicate type
                     and forwards the call to removeAll(const T&, PRED).
         */
        template<typename PRED = std::equal_to<T>>
        void removeAll(const T& value) { removeAll<>(value, PRED()); }

        /*! \brief Removes all occurrences of the value matched by the predicate.
            \details The predicate must be a functor.
         */
        template<typename PRED>
        void removeAll(const T& value, PRED predicate)
        {
            ELEM* curr = m_buffer.first;
            while (curr)
            {
                ELEM* next = curr->next;
                if (predicate(value, curr->value))
                {
                    if (curr->prev) curr->prev->next = curr->next;
                    if (curr->next) curr->next->prev = curr->prev;
                    if (curr == m_buffer.first)
                    {
                        ScopeLock{borderlock};
                        m_buffer.first = curr->next;
                    }
                    if (curr == m_buffer.last )
                    {
                        ScopeLock{borderlock};
                        m_buffer.last  = curr->prev;
                    }
                    delete curr;
                    --m_length;
                }
                curr = next;
            }
        }
        //}

        /*! \brief Gets the element at the given index.
            \details Actually returns an iterator which can be simply type-cast. If
                     the index is out of bounds, the iterator will evaluate to false.
         */
        Iterator get(size_t index) { return begin() + (int)index; }

        /*! \brief Gets an immutable iterator pointing to the element at the specified index.
            \details If the index is out of bounds, the iterator will evaluate to false.
         */
        ConstIterator get(size_t index) const { return cbegin() + (int)index; }

        /*! \brief Gets the element at the given index.
            \details Actually returns an iterator which can be simply type-cast. If
                     the index is out of bounds, the iterator will evaluate to false.
         */
        Iterator operator[](size_t index) { return get(index); }

        /*! \brief Gets an immutable reference to the element at the given index.
            \details Actually returns an iterator which can be simply type-cast. If
                     the index is out of bounds, the iterator will evaluate to false.
         */
        ConstIterator operator[](size_t index) const { return get(index); }

        /*! \brief Gets the length of this list. */
        size_t length() const { return m_length; }

        /*! \brief Forces recounting of the elements in this list.
            \details The methods of this list all assume any supplied iterators point to an
                     element in this list. However, if they don't, the cached length will
                     desync. This method forcibly recounts the number of elements in this
                     list, although with proper usage this should never be necessary.
         */
        size_t count()
        {
            m_length = 0;
            for (auto& val : *this) ++m_length;
            return m_length;
        }

    public:
        //{ std::iterator interface
        Iterator begin() { return Iterator(m_buffer.first); }
        ConstIterator begin() const { return ConstIterator(m_buffer.first); }
        ConstIterator cbegin() const { return ConstIterator(m_buffer.first); }
        RIterator rbegin() { return RIterator(m_buffer.last); }
        RConstIterator rbegin() const { return RConstIterator(m_buffer.last); }
        RConstIterator crbegin() const { return RConstIterator(m_buffer.last); }

        Iterator end() { return Iterator(); }
        ConstIterator end() const { return cend(); }
        ConstIterator cend() const { return ConstIterator(); }
        RIterator rend() { return RIterator(); }
        RConstIterator rend() const { return rend(); }
        RConstIterator crend() const { return RConstIterator(); }
        //}

    };
}

#endif // VPE_LIST_HPP
