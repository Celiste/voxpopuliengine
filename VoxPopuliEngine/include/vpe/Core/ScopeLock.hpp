#ifndef VPE_SCOPELOCK_HPP
#define VPE_SCOPELOCK_HPP

#include "vpe.h"
#include <mutex>
#include "Exception.hpp"
#include "Simpletons.hpp"

VPE_EXPORT_FOREIGN(class VPE_API std::mutex;)

namespace vpe
{
	/*! \brief Simple, convenient class designed to lock a std::mutex for the duration of this instance's lifetime.
		\details The mutex does not need to be passed in immediately, but only one mutex can be assigned to the
				 scope lock ever.
				 If delegation is not necessary, prefer ScopeLock.
		\see ScopeLock
	 */
	class VPE_API DelegatedScopeLock : CopyMoveSwitch<false, false>
	{
		std::mutex* mutex;

	public:
		inline DelegatedScopeLock() noexcept : mutex(nullptr) {}
		inline DelegatedScopeLock(std::mutex& mutex) : mutex(&mutex) { mutex.lock(); }
		inline ~DelegatedScopeLock() {
			if (mutex) mutex->unlock();
		}

		inline void assume(std::mutex& mutex)
		{
			if (this->mutex) throw InvalidStateException("A mutex has already been assumed");
			this->mutex = &mutex;
			mutex.lock();
		}

		inline operator bool() const { return mutex; }
	};

	/*! \brief Simple, more convenient class designed to lock a std::mutex for the duration of this instance's lifetime.
		\details If in need of an optional (or later initialized) lock, prefer DelegatedScopeLock.
		\see DelegatedScopeLock
	 */
	class VPE_API ScopeLock : private DelegatedScopeLock
	{
	public:
		inline ScopeLock(std::mutex& mutex) : DelegatedScopeLock(mutex) {}
		~ScopeLock() = default;
	};
}

#endif
