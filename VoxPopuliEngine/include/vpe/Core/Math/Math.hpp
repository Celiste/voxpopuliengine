#ifndef VPE_MATH_MATH_HPP
#define VPE_MATH_MATH_HPP

#include <cmath>
#include "Core/Integrals.hpp"
#include "Core/TypeTraits.hpp"

namespace vpe {
	namespace Math
	{
		constexpr double c_pi = 3.14159265358979323846;
		constexpr double c_pi_over_2 = 1.57079632679489661923;
		constexpr double c_pi_over_4 = 0.785398163397448309616;
		constexpr double c_1point5_pi = 1.5 * c_pi;
		constexpr double c_2_pi = 2 * c_pi;

		/*! \brief Determines the smallest value.
            \return The smaller of the two values, or the first if both are equal.
         */
		template<typename NUM>
		constexpr inline decltype(NUM() > NUM(), NUM()) min(const NUM& first, const NUM& second) {
            return second > first ? first : second;
		}

        /*! \brief Determines the largest value.
            \return The larger of the two values, or the first if both are equal.
         */
		template<typename NUM>
		constexpr inline decltype(NUM() < NUM(), NUM()) max(const NUM& first, const NUM& second) {
		    return second < first ? first : second;
		}

		/*! \brief Rounds the given number down. */
		constexpr inline int32_t floor(double f) noexcept {
			return (int32_t)f;
		}

		/*! \brief Rounds the given number up. */
		constexpr inline int32_t ceil(double f) noexcept {
			int32_t fl = floor(f);
			if (f - fl > 0.000001) return fl + 1;
			return fl;
		}

		/*! \brief Yields the NUM distance from 0. */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> abs(NUM num) noexcept {
			return num < 0 ? -num : num;
		}

		/*! \brief Compares the two numbers and checks if they can be considered nearly equal with respect to a certain margin.
		 */
		template<typename INTEGRAL>
		constexpr inline std::enable_if_t<std::is_integral_v<INTEGRAL>, bool> nearly(const INTEGRAL& value, const INTEGRAL& to, const INTEGRAL& margin = 0) noexcept {
			return (value - to) < margin;
		}

		/*! \brief Convenience function of nearly(const DECIMAL&, const DECIMAL&, const DECIMAL&) designed for use with integral constants / magic numbers. */
		template<typename DECIMAL, typename NUM>
		constexpr inline std::enable_if_t<!std::is_integral_v<DECIMAL> && std::is_arithmetic_v<DECIMAL> && std::is_arithmetic_v<NUM>, bool> nearly(const DECIMAL& value, const NUM& to, const DECIMAL& margin = 0.00001f) noexcept {
            return abs(value - to) < margin;
		}

		/*! \brief Calculates the faculty of the given integral value. */
		template<typename INTEGRAL>
		constexpr std::enable_if_t<std::is_integral_v<INTEGRAL>, INTEGRAL> faculty(const INTEGRAL& value) noexcept {
			INTEGRAL result = 1;
			for (INTEGRAL i = 2; i < value; ++i)
			{
				result *= i;
			}
			return result;
		}

		/*! \brief Calculates 2 to the power of value.
			\note Since the result will be an integer, the value cannot be negative as this would lead to results between 0 and 1.
			\see power2(uint64_t)
		 */
		constexpr inline uint32_t power2(uint32_t value) noexcept {
			return ((uint32_t)1) << value;
		}

		/*! \brief 64 bit version of power2(uint32_t).
			\see power2(uint32_t)
		 */
		constexpr inline uint64_t power2(uint64_t value) noexcept {
			return ((uint64_t)1) << value;
		}

		/*! \brief Calculates base to the power of power.
			\note Since the result will be an integer, the exp cannot be negative as this would lead to results between 0 and 1.
			\warning Due to integer overflow, exp should probably never reach high magnitudes.
		 */
		constexpr inline int32_t power(int32_t base, uint32_t power) noexcept {
			int32_t result = 1;
			for (uint32_t i = 0; i < power; ++i)
			{
				result *= base;
			}
			return result;
		}

		/*! \brief 64 bit version of power(int32_t, uint32_t).
			\details Formula: base^exp
			\note Since the result will be an integer, the exp cannot be negative as this would lead to results between 0 and 1.
			\warning Due to integer overflow, exp should probably never reach high magnitudes.
		 */
		constexpr inline int64_t power(int64_t base, uint64_t power) noexcept {
			int64_t result = 1;
			for (uint64_t i = 0; i < power; ++i)
			{
				result *= base;
			}
			return result;
		}

		/*! \brief Merely a convenience function to wrap a call to std::sin for non-standard data types (such as fixed point decimals and big floats). */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> sin(const NUM& rad) noexcept {
            return NUM(std::sin((double)rad));
		}

		/*! \brief Merely a convenience function to wrap a call to std::asin for non-standard data types (such as fixed point decimals and big floats). */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> asin(const NUM& sinval) noexcept {
            return NUM(std::asin((double)sinval));
        }

		/*! \brief Merely a convenience function to wrap a call to std::cos for non-standard data types (such as fixed point decimals and big floats). */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> cos(const NUM& rad) noexcept {
            return NUM(std::cos((double)rad));
		}

		/*! \brief Merely a convenience function to wrap a call to std::acos for non-standard data types (such as fixed point decimals and big floats). */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> acos(const NUM& cosval) noexcept {
            return NUM(std::acos((double)cosval));
		}

		/*! \brief Merely a convenience function to wrap a call to std::tan for non-standard data types (such as fixed point decimals and big floats). */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> tan(const NUM& rad) noexcept {
            return NUM(std::tan((double)rad));
		}

		/*! \brief Merely a convenience function to wrap a call to std::atan for non-standard data types (such as fixed point decimals and big floats). */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> atan(const NUM& tanval) noexcept {
            return NUM(std::atan((double)tanval));
		}

		/*! \brief Merely a convenience function to wrap a call to std::sqrt for non-standard data types (such as fixed point decimals and big floats). */
		template<typename NUM>
		constexpr inline std::enable_if_t<std::is_arithmetic_v<NUM>, NUM> sqrt(const NUM& num) noexcept {
		    return NUM(std::sqrt((double)num));
		}

		/*! \brief Converts the given first 4 perspective parameters into near plane bounds.
            \param[in]  fovy        Determines the field of view along the y axis in radians.
            \param[in]  aspectRatio represents the width / height ratio of the near clipping plane.
            \param[in]  near        Describes the distance of the near clipping plane to the viewer / eye center. Must be positive.
            \param[in]  far         Describes the distance of the far clipping plane to the viewer / eye center. Must be positive.
            \param[out] left   Calculated left bounds of the near clipping plane.
            \param[out] right  Calculated right bounds of the near clipping plane.
            \param[out] top    Calculated top bounds of the near clipping plane.
            \param[out] bottom Calculated bottom bounds of the near clipping plane.
            \see https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glFrustum.xml
         */
        template<typename NUM>
        void convertFOV(const NUM& fovy, const NUM& aspectRatio, const NUM& near, const NUM& far, NUM& left, NUM& right, NUM& top, NUM& bottom) {
            top = tan(fovy / 2) * near;
            bottom = -top;
            right = top * aspectRatio;
            left = -right;
        }
	}
}

#endif // VPE_MATH_MATH_HPP
