#ifndef VPE_LINEARALGEBRA_HPP
#define VPE_LINEARALGEBRA_HPP

/*! \file
	\brief Provides various structs and algorithms to calculate linear algebra.
	\author Kiruse aka. Celiste
 */

#include "Exception.hpp"
#include <type_traits>
#include "Core/Integrals.hpp"
#include "Core/FixedPointDecimal.hpp"
#include "Math.hpp"
#include <SFML/System/Vector2.hpp>

namespace vpe {
	namespace Math
	{
	    //{ Vector Structs
	    template<typename NUM, size_t Dimension>
	    struct Vector {
	        static constexpr size_t dimension = Dimension;

	        /*! \brief Vector with components initialized to 0. */
	        static const Vector Zero = Vector(0);

	        /*! \brief Vector with components initialized to 1. */
	        static const Vector One  = Vector(1);

	        NUM values[Dimension];

	        Vector() = default;
	        Vector(NUM value) {
                for (size_t i = 0; i < Dimension; ++i)
                    values[i] = value;
	        }
	        Vector(const Vector&) = default;
	        Vector(Vector&&) = default;
	        Vector& operator=(const Vector&) = default;
	        Vector& operator=(Vector&&) = default;
	        ~Vector() = default;

	        NUM& operator[](size_t index) { return values[index]; }
	        const NUM& operator[](size_t index) const { return values[index]; }

	        Vector& normalize() {
	            NUM s = 0;
	            for (size_t i = 0; i < Dimension; ++i)
                {
                    s += values[i] * values[i];
                }
                s = 1 / sqrt(s);

                for (size_t i = 0; i < Dimension; ++i)
                {
                    values[i] *= s;
                }
                return *this;
	        }

	        /*! \brief Convenience function to copy-construct a new vector from this one. */
	        Vector copy() const {
                return Vector(*this);
	        }
	    };

	    template<typename NUM>
	    struct Vector<NUM, 2> {
	        static constexpr size_t dimension = 2;

	        static const Vector Zero;
	        static const Vector One;

	        NUM values[2];

	        NUM& x;
	        NUM& y;

            Vector() : x(values[0]), y(values[1]) {}
	        Vector(const NUM& value) : x(values[0]), y(values[1]) { x = y = value; }
	        Vector(const NUM& x, const NUM& y) : x(values[0]), y(values[1]) {
	            this->x = x;
	            this->y = y;
	        }
	        Vector(NUM&& x, NUM&& y) : x(values[0]), y(values[1]) {
	            this->x = std::move(x);
	            this->y = std::move(y);
	        }
	        Vector(const Vector& copy) : x(values[0]), y(values[1]) { x = copy.x; y = copy.y; }
	        Vector(Vector&& move) : x(values[0]), y(values[1]) { x = std::move(move.x); y = std::move(move.y); }
	        Vector& operator=(const Vector& copy) {
	            x = copy.x;
	            y = copy.y;
	            return *this;
	        }
	        Vector& operator=(Vector&& move) {
                x = std::move(move.x);
                y = std::move(move.y);
                return *this;
	        }
	        ~Vector() = default;

	        NUM& operator[](size_t index) { return values[index]; }
	        const NUM& operator[](size_t index) const { return values[index]; }
	    };

	    template<typename NUM> const Vector<NUM, 2> Vector<NUM, 2>::Zero = Vector<NUM, 2>(0);
	    template<typename NUM> const Vector<NUM, 2> Vector<NUM, 2>::One = Vector<NUM, 2>(1);

        template<typename NUM> using Vector2 = Vector<NUM, 2>;
        typedef Vector2<int32_t> IVector2;
        typedef Vector2<float> FVector2;
        typedef Vector2<double> DVector2;
        typedef Vector2<Fixed> FixedVector2;
        typedef Vector2<LongFixed> LongFixedVector2;

	    template<typename NUM>
	    struct Vector<NUM, 3> {
	        static constexpr size_t dimension = 3;

	        /*! \brief Vector with components initialized to 0. */
	        static const Vector Zero;

	        /*! \brief Vector with components initialized to 1. */
	        static const Vector One;

	        //{ Axis unit vectors
	        /*! \brief Vector pointing into the monitor. */
	        static const Vector Forward;

	        /*! \brief Vector pointing out of the monitor. */
	        static const Vector Backward;

	        /*! \brief Vector pointing to the top of the monitor. */
	        static const Vector Up;

	        /*! \brief Vector pointing to the bottom of the monitor. */
	        static const Vector Down;

	        /*! \brief Vector pointing to the right of the monitor. */
	        static const Vector Right;

	        /*! \brief Vector pointing to the left of the monitor. */
	        static const Vector Left    ;
	        //}

	        NUM values[3];

	        NUM& x;
	        NUM& y;
	        NUM& z;

			Vector() : x(values[0]), y(values[1]), z(values[2]) {}
			Vector(const NUM& value) : x(values[0]), y(values[1]), z(values[2]) { x = y = z = value; }
			Vector(const NUM& x, const NUM& y, const NUM& z) : x(values[0]), y(values[1]), z(values[2]) {
				this->x = x;
				this->y = y;
				this->z = z;
			}
			Vector(NUM&& x, NUM&& y, NUM&& z) : x(values[0]), y(values[1]), z(values[2]) {
				this->x = std::move(x);
				this->y = std::move(y);
				this->z = std::move(z);
			}
			Vector(const Vector<NUM, 2>& vec, const NUM& z) : x(values[0]), y(values[1]), z(values[2]) {
                this->x = vec.x;
                this->y = vec.y;
                this->z = z;
			}
			Vector(const Vector<NUM, 2>& vec, NUM&& z = 0) : x(values[0]), y(values[1]), z(values[2]) {
			    this->x = vec.x;
			    this->y = vec.y;
			    this->z = std::move(z);
			}
			Vector(Vector<NUM, 2>&& vec, NUM&& z = 0) : x(values[0]), y(values[1]), z(values[2]) {
                this->x = std::move(vec.x);
                this->y = std::move(vec.y);
			}
	        Vector(const Vector& copy) : x(values[0]), y(values[1]), z(values[2]) {
                x = copy.x;
                y = copy.y;
                z = copy.z;
	        }
	        Vector(Vector&& move) : x(values[0]), y(values[1]), z(values[2]) {
                x = std::move(move.x);
                y = std::move(move.y);
                z = std::move(move.z);
	        }
	        Vector& operator=(const Vector& copy) {
	            x = copy.x;
	            y = copy.y;
	            z = copy.z;
	            return *this;
            }
	        Vector& operator=(Vector&& move) {
	            x = std::move(move.x);
	            y = std::move(move.y);
	            z = std::move(move.z);
	            return *this;
            }
	        Vector& operator=(const Vector<NUM, 2>& vec) {
	            x = vec.x;
	            y = vec.y;
	            z = 0;
	            return *this;
            }
	        Vector& operator=(Vector<NUM, 2>&& vec) {
	            x = std::move(vec.x);
	            y = std::move(vec.y);
	            z = 0;
	            return *this;
            }
	        ~Vector() = default;

	        NUM& operator[](size_t index) { return values[index]; }
	        const NUM& operator[](size_t index) const { return values[index]; }
	    };

        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::Zero = Vector<NUM, 3>(0);
        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::One = Vector<NUM, 3>(1);
        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::Forward = Vector<NUM, 3>(0, 0, -1);
        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::Backward = Vector<NUM, 3>(0, 0, 1);
        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::Up = Vector<NUM, 3>(0, 1, 0);
        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::Down = Vector<NUM, 3>(0, -1, 0);
        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::Right = Vector<NUM, 3>(1, 0, 0);
        template<typename NUM> const Vector<NUM, 3> Vector<NUM, 3>::Left = Vector<NUM, 3>(-1, 0, 0);

        template<typename NUM> using Vector3 = Vector<NUM, 3>;
        typedef Vector3<int32_t> IVector3;
        typedef Vector3<float> FVector3;
        typedef Vector3<double> DVector3;
        typedef Vector3<Fixed> FixedVector3;
        typedef Vector3<LongFixed> LongFixedVector3;

	    template<typename NUM>
	    struct Vector<NUM, 4> {
	        static constexpr size_t dimension = 4;

	        /*! \brief Vector with all components initialized to 0. */
            static const Vector Zero;

            /*! \brief Vector with all components initialized to 1. */
            static const Vector One;

            NUM values[4];

            NUM& x;
            NUM& y;
            NUM& z;
            NUM& w;

            Vector() : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {}
			Vector(const NUM& value) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) { x = y = z = w = value; }
			Vector(const NUM& x, const NUM& y, const NUM& z, const NUM& w) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {
				this->x = x;
				this->y = y;
				this->z = z;
				this->w = w;
			}
			Vector(NUM&& x, NUM&& y, NUM&& z, NUM&& w) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {
				this->x = std::move(x);
				this->y = std::move(y);
				this->z = std::move(z);
				this->w = std::move(w);
			}
			Vector(const Vector<NUM, 2>& vec, const NUM& z, const NUM& w) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {
			    this->x = vec.x;
			    this->y = vec.y;
			    this->z = z;
			    this->w = w;
			}
			Vector(const Vector<NUM, 2>& vec, NUM&& z = 0, NUM&& w = 0) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {
			    this->x = vec.x;
			    this->y = vec.y;
			    this->z = std::move(z);
			    this->w = std::move(w);
			}
			Vector(Vector<NUM, 2>&& vec, NUM&& z = 0, NUM&& w = 0) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {
                this->x = std::move(vec.x);
                this->y = std::move(vec.y);
                this->z = std::move(z);
                this->w = std::move(w);
			}
	        Vector(const Vector& copy) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {
                x = copy.x;
                y = copy.y;
                z = copy.z;
                w = copy.w;
	        }
	        Vector(Vector&& move) : x(values[0]), y(values[1]), z(values[2]), w(values[3]) {
	            x = std::move(move.x);
	            y = std::move(move.y);
	            z = std::move(move.z);
	            w = std::move(move.w);
            }
	        Vector& operator=(const Vector& copy) {
	            x = copy.x;
	            y = copy.y;
	            z = copy.z;
	            w = copy.w;
	            return *this;
            }
	        Vector& operator=(Vector&& move) {
	            x = std::move(move.x);
	            y = std::move(move.y);
	            z = std::move(move.z);
	            w = std::move(move.w);
	            return *this;
            }
	        Vector& operator=(const Vector2<NUM>& vec) {
	            x = vec.x;
	            y = vec.y;
	            z = w = 0;
	            return *this;
            }
	        Vector& operator=(Vector<NUM, 2>&& vec) {
	            x = std::move(vec.x);
	            y = std::move(vec.y);
	            z = w = 0;
            }
	        Vector& operator=(const Vector3<NUM>& vec) {
	            x = vec.x;
	            y = vec.y;
	            z = vec.z;
	            w = 0;
	            return *this;
            }
	        Vector& operator=(Vector<NUM, 3>&& vec) {
	            x = std::move(vec.x);
	            y = std::move(vec.y);
	            z = std::move(vec.z);
	            w = 0;
	            return *this;
            }
            ~Vector() = default;

	        NUM& operator[](size_t index) { return values[index]; }
	        const NUM& operator[](size_t index) const { return values[index]; }
	    };

        template<typename NUM> const Vector<NUM, 4> Vector<NUM, 4>::Zero = Vector<NUM, 4>(0);
        template<typename NUM> const Vector<NUM, 4> Vector<NUM, 4>::One = Vector<NUM, 4>(1);

        template<typename NUM> using Vector4 = Vector<NUM, 4>;
        typedef Vector4<int32_t> IVector4;
        typedef Vector4<float> FVector4;
        typedef Vector4<double> DVector4;
        typedef Vector4<Fixed> FixedVector4;
        typedef Vector4<LongFixed> LongFixedVector4;

		template<typename NUM>
		struct Quaternion {
		    /*! \brief Quaternion with components inialized to 0. */
		    static const Quaternion Zero;

		    /*! \brief Unit Quaternion with imaginary components initialized to 0 and the real part initialized to 1. */
		    static const Quaternion RealUnit;

			NUM r;
			NUM i;
			NUM j;
			NUM k;

			Quaternion() = default;
			Quaternion(NUM r, NUM i, NUM j, NUM k) : r(r), i(i), j(j), k(k) {}
			/*! \brief Converts the vector into a quaternion.
                \details Assumes the vector is a normalized Euler axis & angle.
                \see http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
             */
			explicit Quaternion(const Vector<NUM, 4>& vec) {
			    NUM a = vec.w / 2, b = sin(a);
                r = cos(a);
                i = vec.x * b;
                j = vec.y * b;
                k = vec.z * b;
			}
			Quaternion(const Quaternion&) = default;
			Quaternion(Quaternion&&) = default;
			Quaternion& operator=(const Quaternion&) = default;
			Quaternion& operator=(Quaternion&&) = default;
			~Quaternion() = default;

			/*! \brief Converts this quaternion back into a Euler axis + angle.
                \see http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/
             */
            Vector4<NUM> toEulerAngle() const {
                if (nearly(i, 0) && nearly(j, 0) && nearly(k, 0)) return Vector4<NUM>(Vector3<NUM>::Forward, 0);

                NUM tmp = 1 / sqrt(1 - r*r);
                return Vector4<NUM>(i * tmp, j * tmp, k * tmp, 2 * acos(r));
            }

            /*! \brief Normalizes this quaternion (to length 1). */
            Quaternion& normalize() {
                NUM s = 1 / sqrt(r*r + i*i + j*j + k*k);
                r *= s;
                i *= s;
                j *= s;
                k *= s;
                return *this;
            }

            /*! \brief Conjugates / inverts this quaternion.
                \details Assumes the quaternion is normalized.
                \see normalize()
             */
            Quaternion& conjugate() {
                i = -i;
                j = -j;
                k = -k;
                return *this;
            }

            /*! \brief Convenience method to copy-construct a new quaternion from this one. */
            Quaternion copy() const {
                return Quaternion(*this);
            }
		};

		template<typename NUM> const Quaternion<NUM> Quaternion<NUM>::Zero = Quaternion(0, 0, 0, 0);
		template<typename NUM> const Quaternion<NUM> Quaternion<NUM>::RealUnit = Quaternion(1, 0, 0, 0);

		typedef Quaternion<float> FQuaternion;
		typedef Quaternion<double> DQuaternion;
		typedef Quaternion<Fixed> FixedQuaternion;
        typedef Quaternion<LongFixed> LongFixedQuaternion;
		//}

		//{ Vector & Quaternion Addition
		template<typename NUM, size_t N, size_t M>
		std::enable_if_t<N >= M, Vector<NUM, N>&> operator+=(Vector<NUM, N>& lhs, const Vector<NUM, M>& rhs) {
		    for (size_t i = 0; i < M; ++i)
            {
                lhs[i] += rhs[i];
            }
            return lhs;
		}
		template<typename NUM, size_t N, size_t M>
		std::enable_if_t<N >= M, Vector<NUM, N>> operator+(const Vector<NUM, N>& lhs, const Vector<NUM, M>& rhs) {
		    Vector<NUM, N> copy(lhs);
		    return copy += rhs;
		}

		template<typename NUM>
		Quaternion<NUM>& operator+=(Quaternion<NUM>& lhs, const Quaternion<NUM>& rhs) {
		    lhs.r += rhs.r;
		    lhs.i += rhs.i;
		    lhs.j += rhs.j;
		    lhs.k += rhs.k;
		    return lhs;
		}
		template<typename NUM>
		Quaternion<NUM> operator+(const Quaternion<NUM>& lhs, const Quaternion<NUM>& rhs) {
		    Quaternion<NUM> copy(lhs);
		    return copy += rhs;
		}
		//}

		//{ Vector & Quaternion Subtraction
        template<typename NUM, size_t N, size_t M>
		std::enable_if_t<N >= M, Vector<NUM, N>&> operator-=(Vector<NUM, N>& lhs, const Vector<NUM, M>& rhs) {
		    for (size_t i = 0; i < M; ++i)
            {
                lhs[i] -= rhs[i];
            }
            return lhs;
		}
		template<typename NUM, size_t N, size_t M>
		std::enable_if_t<N >= M, Vector<NUM, N>> operator-(const Vector<NUM, N>& lhs, const Vector<NUM, M>& rhs) {
		    Vector<NUM, N> copy(lhs);
		    return copy -= rhs;
		}

		template<typename NUM>
		Quaternion<NUM>& operator-=(Quaternion<NUM>& lhs, const Quaternion<NUM>& rhs) {
		    lhs.r -= rhs.r;
		    lhs.i -= rhs.i;
		    lhs.j -= rhs.j;
		    lhs.k -= rhs.k;
		    return lhs;
		}
		template<typename NUM>
		Quaternion<NUM> operator-(const Quaternion<NUM>& lhs, const Quaternion<NUM>& rhs) {
		    Quaternion<NUM> copy(lhs);
		    return copy -= rhs;
		}
		//}

        //{ Vector Dot & Cross Products & Normalization
        /*! \brief Calculates the dot product of any two given vectors.
            \details The lesser dimension vector is imaginarily 0-filled to the greater dimension
                     (such that (1|2) becomes (1|2|0), for example). Since 0a = 0, only the lesser
                     dimension becomes relevant.
         */
        template<typename NUM, size_t N, size_t M>
        NUM dot(const Vector<NUM, N>& lhs, const Vector<NUM, M>& rhs) {
            NUM result = 0;
            for (size_t i = 0; i < min<size_t>(N, M); ++i)
            {
                result += lhs[i] * rhs[i];
            }
            return result;
        }

        /*! \brief Calculates the magnitude of the imaginary 3D vector that would result if the z-component of both vectors was 0.
            \details See http://mathworld.wolfram.com/CrossProduct.html.
                     Since the calculation of the x and y components through the 3D cross product involve z,
                     which is set to 0 in this case, the resulting vector has x = y = 0 and
                     z = v1_x * v2_y - v1_y * v2_x = |v1 x v2|.
         */
        template<typename NUM>
        NUM cross(const Vector2<NUM>& lhs, const Vector2<NUM>& rhs) {
            return lhs.x * rhs.y - lhs.y * rhs.x;
        }

        /*! \brief Complementary counterpart of cross(const Vector2<NUM>&, const Vector2<NUM>&) to determine a possible orthogonal vector.
            \details Unlike in 3D space, the number of possible orthogonal vectors in 2D space is limited to 2. We simply return one of those.
         */
        template<typename NUM>
        Vector2<NUM> orthogonal(const Vector2<NUM>& vec) {
            return Vector2<NUM>(vec.y, -vec.x);
        }

        template<typename NUM>
        Vector3<NUM> cross(const Vector3<NUM>& lhs, const Vector3<NUM>& rhs) {
            Vector3<NUM> result;
            result[0] = lhs[1] * rhs[2] - lhs[2] * rhs[1];
            result[1] = lhs[2] * rhs[0] - lhs[0] * rhs[2];
            result[2] = lhs[0] * rhs[1] - lhs[1] * rhs[0];
            return result;
        }
        //}

        //{ Quaternion Multiplication
        template<typename NUM>
        Quaternion<NUM> operator*(const Quaternion<NUM>& lhs, const Quaternion<NUM>& rhs) {
            Quaternion<NUM> result;
            result.r = lhs.r * rhs.r - lhs.i * rhs.i - lhs.j * rhs.j - lhs.k * rhs.k;
            result.i = lhs.i * rhs.r + lhs.r * rhs.i - lhs.k * rhs.j + lhs.j * rhs.k;
            result.j = lhs.j * rhs.r + lhs.k * rhs.i + lhs.r * rhs.j - lhs.i * rhs.k;
            result.k = lhs.k * rhs.r - lhs.j * rhs.i + lhs.i * rhs.j + lhs.r * rhs.k;
            return result;
        }
        template<typename NUM>
        Quaternion<NUM>& operator*=(Quaternion<NUM>& lhs, const Quaternion<NUM>& rhs) {
            return lhs = Quaternion<NUM>(lhs) * rhs;
        }
        //}

		//{ Matrix Structs
		template<typename NUM, size_t ColCount, size_t RowCount>
		struct Matrix {
		    /*! \brief Constructs the zero matrix. */
		    static constexpr Matrix getZero() {
		        Matrix result;
		        for (size_t i = 0; i < ColCount; ++i)
                {
                    for (size_t j = 0; j < RowCount; ++j)
                    {
                        result[i][j] = 0;
                    }
                }
                return result;
		    }

		    /*! \brief Constructs the identity matrix.
                \details Requires ColCount == RowCount.
		     */
		    template<size_t N = ColCount, size_t M = RowCount, std::enable_if_t<N == M, bool> = 0>
            static constexpr Matrix getIdentity() {
                Matrix result = getZero();
                for (size_t i = 0; i < N; ++i)
                {
                    result[i][i] = 1;
                }
                return result;
            }

		    static constexpr size_t col_count = ColCount;
		    static constexpr size_t row_count = RowCount;

		    /*! \brief Zero-filled matrix */
		    static constexpr Matrix Zero = getZero();

		    /*! \brief Identity matrix which, multiplied with any other matrix with the same dimensions, yields the other matrix.
                \warning Due to the nature of matrix multiplication, identity matrices can only exist for square matrices (ColCount == RowCount).
		     */
		    template<size_t N = ColCount, size_t M = RowCount, std::enable_if_t<N == M, bool> = 0>
		    static constexpr Matrix Identity = getIdentity<N, M>();

		    /*! \brief Rows in this matrix.
                \warning Matrices are accessed through rows, but stored OpenGL style (column-major).
		     */
		    Vector<NUM, ColCount> rows[RowCount];

            Matrix() = default;
            Matrix(const Matrix&) = default;
            Matrix(Matrix&&) = default;
            Matrix& operator=(const Matrix&) = default;
            Matrix& operator=(Matrix&&) = default;
            ~Matrix() = default;

            Vector<NUM, ColCount>& operator[](size_t index) { return rows[index]; }
            const Vector<NUM, ColCount>& operator[](size_t index) const { return rows[index]; }
		};

		template<typename NUM>
		struct Matrix<NUM, 2, 2> {
            static constexpr size_t col_count = 2;
            static constexpr size_t row_count = 2;

            static constexpr Matrix Zero = Matrix(0, 0, 0, 0);
            static constexpr Matrix Identity = Matrix(1, 0, 0, 1);

            Vector2<NUM> rows[2];

            Matrix() = default;
            Matrix(const NUM& _11, const NUM& _12, const NUM& _21, const NUM& _22) {
                rows[0] = Vector2<NUM>(_11, _12);
                rows[1] = Vector2<NUM>(_21, _22);
            }
            Matrix(NUM&& _11, NUM&& _12, NUM&& _21, NUM&& _22) {
                rows[0] = Vector2<NUM>(std::move(_11), std::move(_12));
                rows[1] = Vector2<NUM>(std::move(_21), std::move(_22));
            }
            Matrix(const Vector2<NUM>& row1, const Vector2<NUM>& row2) {
                rows[0] = row1;
                rows[1] = row2;
            }
            Matrix(Vector2<NUM>&& row1, Vector2<NUM>&& row2) {
                rows[0] = std::move(row1);
                rows[1] = std::move(row2);
            }
            Matrix(const Matrix&) = default;
            Matrix(Matrix&&) = default;
            Matrix& operator=(const Matrix&) = default;
            Matrix& operator=(Matrix&&) = default;
            ~Matrix() = default;

            Vector2<NUM>& operator[](size_t index) { return rows[index]; }
            const Vector2<NUM>& operator[](size_t index) const { return rows[index]; }
		};
        template<typename NUM> using Matrix2 = Matrix<NUM, 2, 2>;
        typedef Matrix2<int32_t> IMatrix2;
        typedef Matrix2<float> FMatrix2;
        typedef Matrix2<double> DMatrix2;
        typedef Matrix2<Fixed> FixedMatrix2;
        typedef Matrix2<LongFixed> LongFixedMatrix2;

		template<typename NUM>
		struct Matrix<NUM, 3, 3> {
		    static constexpr size_t col_count = 3;
		    static constexpr size_t row_count = 3;

		    static constexpr Matrix Zero = Matrix(0, 0, 0, 0, 0, 0, 0, 0, 0);
		    static constexpr Matrix Identity = Matrix(1, 0, 0, 0, 1, 0, 0, 0, 1);

		    Vector3<NUM> rows[3];

		    Matrix() = default;
		    Matrix(const NUM& _11, const NUM& _12, const NUM& _13, const NUM& _21, const NUM& _22, const NUM& _23, const NUM& _31, const NUM& _32, const NUM& _33) {
		        rows[0] = Vector3<NUM>(_11, _12, _13);
		        rows[1] = Vector3<NUM>(_21, _22, _23);
		        rows[2] = Vector3<NUM>(_31, _32, _33);
		    }
		    Matrix(NUM&& _11, NUM&& _12, NUM&& _13, NUM&& _21, NUM&& _22, NUM&& _23, NUM&& _31, NUM&& _32, NUM&& _33) {
		        rows[0] = Vector3<NUM>(std::move(_11), std::move(_12), std::move(_13));
		        rows[1] = Vector3<NUM>(std::move(_21), std::move(_22), std::move(_23));
		        rows[2] = Vector3<NUM>(std::move(_31), std::move(_32), std::move(_33));
		    }
		    Matrix(const Vector3<NUM>& row1, const Vector3<NUM>& row2, const Vector3<NUM>& row3) {
		        rows[0] = row1;
		        rows[1] = row2;
		        rows[2] = row3;
		    }
		    Matrix(Vector3<NUM>&& row1, Vector3<NUM>&& row2, Vector3<NUM>&& row3) {
		        rows[0] = std::move(row1);
		        rows[1] = std::move(row2);
		        rows[2] = std::move(row3);
		    }
            Matrix(const Matrix&) = default;
            Matrix(Matrix&&) = default;
            Matrix& operator=(const Matrix&) = default;
            Matrix& operator=(Matrix&&) = default;
            ~Matrix() = default;

            Vector3<NUM>& operator[](size_t index) { return rows[index]; }
            const Vector3<NUM>& operator[](size_t index) const { return rows[index]; }
		};
        template<typename NUM> using Matrix3 = Matrix<NUM, 3, 3>;
        typedef Matrix3<int32_t> IMatrix3;
        typedef Matrix3<float> FMatrix3;
        typedef Matrix3<double> DMatrix3;
        typedef Matrix3<Fixed> FixedMatrix3;
        typedef Matrix3<LongFixed> LongFixedMatrix3;

		template<typename NUM>
		struct Matrix<NUM, 4, 4> {
		    static constexpr size_t col_count = 4;
		    static constexpr size_t row_count = 4;

		    static constexpr Matrix Zero = Matrix(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		    static constexpr Matrix Identity = Matrix(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

		    Vector4<NUM> rows[4];

		    Matrix() = default;
		    Matrix(const NUM& _11, const NUM& _12, const NUM& _13, const NUM& _14, const NUM& _21, const NUM& _22, const NUM& _23, const NUM& _24, const NUM& _31, const NUM& _32, const NUM& _33, const NUM& _34, const NUM& _41, const NUM& _42, const NUM& _43, const NUM& _44) {
		        rows[0] = Vector4<NUM>(_11, _12, _13, _14);
		        rows[1] = Vector4<NUM>(_21, _22, _23, _24);
		        rows[2] = Vector4<NUM>(_31, _32, _33, _34);
		        rows[3] = Vector4<NUM>(_41, _42, _43, _44);
		    }
		    Matrix(NUM&& _11, NUM&& _12, NUM&& _13, NUM&& _14, NUM&& _21, NUM&& _22, NUM&& _23, NUM&& _24, NUM&& _31, NUM&& _32, NUM&& _33, NUM&& _34, NUM&& _41, NUM&& _42, NUM&& _43, NUM&& _44) {
                rows[0] = Vector4<NUM>(std::move(_11), std::move(_12), std::move(_13), std::move(_14));
		        rows[1] = Vector4<NUM>(std::move(_21), std::move(_22), std::move(_23), std::move(_24));
		        rows[2] = Vector4<NUM>(std::move(_31), std::move(_32), std::move(_33), std::move(_34));
		        rows[3] = Vector4<NUM>(std::move(_41), std::move(_42), std::move(_43), std::move(_44));
		    }
		    Matrix(const Vector4<NUM>& row1, const Vector4<NUM>& row2, const Vector4<NUM>& row3, const Vector4<NUM>& row4) {
		        rows[0] = row1;
		        rows[1] = row2;
		        rows[2] = row3;
		        rows[3] = row4;
		    }
		    Matrix(Vector4<NUM>&& row1, Vector4<NUM>&& row2, Vector4<NUM>&& row3, Vector4<NUM>&& row4) {
                rows[0] = std::move(row1);
		        rows[1] = std::move(row2);
		        rows[2] = std::move(row3);
		        rows[3] = std::move(row4);
		    }
            Matrix(const Matrix&) = default;
            Matrix(Matrix&&) = default;
            Matrix& operator=(const Matrix&) = default;
            Matrix& operator=(Matrix&&) = default;
            ~Matrix() = default;

            /*! \brief Derives a matrix from the given quaternion.
                \details Assumes the quaternion is normalized.
             */
            static Matrix fromQuaternion(const Quaternion<NUM>& quat) {
                return fromQuaternion(quat.r, quat.i, quat.j, quat.k);
            }
            /*! \brief Derives a matrix from the individual components of a quaternion. */
            static Matrix fromQuaternion(const NUM& real, const NUM& i, const NUM& j, const NUM& k) {
                Matrix result;
                result[0][0] = 1 - 2 * (j*j + k*k);
                result[0][1] = 2 * (i * j - k * real);
                result[0][2] = 2 * (i * k + j * real);
                result[0][3] = 0;
                result[1][0] = 2 * (i + j + k * real);
                result[1][1] = 1 - 2 * (i*i + k*k);
                result[1][2] = 2 * (j * k - i * real);
                result[1][3] = 0;
                result[2][0] = 2 * (i * k - j * real);
                result[2][1] = 2 * (j * k + i * real);
                result[2][2] = 1 - 2 * (i*i + j*j);
                result[2][3] = 0;
                result[0][0] = result[0][1] = result[0][2] = 0;
                result[0][4] = 1;
                return result;
            }
            /*! \brief Creates a rotation matrix from the given axis & angle stored in a 4-dimensional vector.
                \details The angle must be in radians.
             */
            static Matrix fromAxisAngle(const Vector4<NUM>& axisangle) {
                return fromAxisAngle(axisangle.x, axisangle.y, axisangle.z, axisangle.w);
            }
            /*! \brief Creates a rotation matrix from the given axis & angle.
                \details Assumes the axis is normalized and the angle is in radians.
             */
            static Matrix fromAxisAngle(const Vector3<NUM>& axis, const NUM& angle) {
                return fromAxisAngle(axis.x, axis.y, axis.z, angle);
            }
            /*! \brief Creates a rotation matrix from the given axis & angle.
                \details Assumes the axis is normalized and the angle is in radians.
                \see http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/index.htm
             */
            static Matrix fromAxisAngle(const NUM& axisX, const NUM& axisY, const NUM& axisZ, const NUM& angle) {
                NUM c = cos(angle);
                NUM s = sin(angle);
                NUM t = 1 - c;

                Matrix result;
                result[0][0] = t * axisX * axisX + c;
                result[0][1] = t * axisX * axisY - axisZ * s;
                result[0][2] = t * axisX * axisZ + axisY * s;
                result[0][3] = 0;
                result[1][0] = t * axisX * axisY + axisZ * s;
                result[1][1] = t * axisY * axisY + c;
                result[1][2] = t * axisY * axisZ - axisX * s;
                result[1][3] = 0;
                result[2][0] = t * axisX * axisZ - axisY * s;
                result[2][1] = t * axisY * axisZ + axisX * s;
                result[2][2] = t * axisZ * axisZ + c;
                result[2][3] = 0;
                result[3][0] = result[3][1] = result[3][2] = 0;
                result[3][3] = 1;
                return result;
            }
            /*! \brief Derives a matrix from the given translation. */
            static Matrix fromTranslation(const Vector3<NUM>& translation) {
                return fromTranslation(translation.x, translation.y, translation.z);
            }
            /*! \brief Convenience method for fromTranslation(const Vector3<NUM>&) */
            static Matrix fromTranslation(const NUM& x, const NUM& y, const NUM& z) {
                return Matrix(1, 0, 0, x,
                              0, 1, 0, y,
                              0, 0, 1, z,
                              0, 0, 0, 1);
            }
            /*! \brief Derives a matrix from the given scale. */
            static Matrix fromScale(const Vector3<NUM>& scale) {
                return fromScale(scale.x, scale.y, scale.z);
            }
            /*! \brief Convenience method for fromScale(const Vector3<NUM>&) */
            static Matrix fromScale(const NUM& scaleX, const NUM& scaleY, const NUM& scaleZ) {
                return Matrix(scaleX, 0, 0, 0, 0, scaleY, 0, 0, 0, 0, scaleZ, 0, 0, 0, 0, 1);
            }

            /*! \brief Calculates an orthogonal projection matrix from the given parameters.
                \details This projection is used for HUDs and menus.
             */
            static Matrix orthogonal(const NUM& left, const NUM& right, const NUM& top, const NUM& bottom, const NUM& near, const NUM& far) {
                Matrix result = Zero;
                result[0][0] = 2 / (right - left);
                result[0][3] = - (right + left) / (right - left);
                result[1][1] = 2 / (top - bottom);
                result[1][3] = - (top + bottom) / (top - bottom);
                result[2][2] = -2 / (far - near);
                result[2][3] = - (far + near) / (far - near);
                result[3][3] = 1;
                return result;
            }
            /*! \brief Calculates a perspective projection matrix from the given near and far clipping planes as well as the field of view along the y axis and the width / height aspect ratio.
                \details Internally calls perspective(const NUM&, const NUM&, const NUM&, const NUM&, const NUM&, const NUM&)
             */
            static Matrix perspective(const NUM& fovy, const NUM& aspectRatio, const NUM& near, const NUM& far) {
                NUM left, right, top bottom;
                convertFOV(fovy, aspectRatio, near, far, left, right, top, bottom);
                return perspective(left, right, top, bottom, near, far);
            }
            /*! \brief Constructs a perspective projection matrix from the given near clipping plane (with bounds) and far clipping plane.
                \see https://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix/opengl-perspective-projection-matrix
             */
            static Matrix perspective(const NUM& left, const NUM& right, const NUM& top, const NUM& bottom, const NUM& near, const NUM& far) {
                Matrix result = Zero;
                result[0][0] = 2 * near / (right - left);
                result[0][2] = (right + left) / (right - left);
                result[1][1] = 2 * near / (top - bottom);
                result[1][2] = (top + bottom) / (top - bottom);
                result[2][2] = - (far + near) / (far - near);
                result[2][3] = - (2 * far * near) / (far - near);
                result[3][2] = -1;
                return result;
            }

            Vector4<NUM>& operator[](size_t index) { return rows[index]; }
            const Vector4<NUM>& operator[](size_t index) const { return rows[index]; }
		};
		template<typename NUM> using Matrix4 = Matrix<NUM, 4, 4>;
		typedef Matrix4<int32_t> IMatrix4;
        typedef Matrix4<float> FMatrix4;
        typedef Matrix4<double> DMatrix4;
        typedef Matrix4<Fixed> FixedMatrix4;
        typedef Matrix4<LongFixed> LongFixedMatrix4;
		//}

		//{ Matrix Addition
		template<typename NUM, size_t N, size_t M>
		Matrix<NUM, N, M>& operator+=(Matrix<NUM, N, M>& lhs, const Matrix<NUM, N, M>& rhs) {
		    for (size_t row = 0; row < N; ++row)
            {
                lhs[row] += rhs[row];
            }
            return lhs;
		}
		template<typename NUM, size_t N, size_t M>
		Matrix<NUM, N, M> operator+(const Matrix<NUM, N, M>& lhs, const Matrix<NUM, N, M>& rhs) {
            Matrix<NUM, N, M> copy(lhs);
            return copy += rhs;
		}
		//}

		//{ Matrix Subtraction
		template<typename NUM, size_t N, size_t M>
		Matrix<NUM, N, M>& operator-=(Matrix<NUM, N, M>& lhs, const Matrix<NUM, N, M>& rhs) {
		    for (size_t row = 0; row < N; ++row)
            {
                lhs[row] -= rhs[row];
            }
            return lhs;
		}
		template<typename NUM, size_t N, size_t M>
		Matrix<NUM, N, M> operator-(const Matrix<NUM, N, M>& lhs, const Matrix<NUM, N, M>& rhs) {
            Matrix<NUM, N, M> copy(lhs);
            return copy -= rhs;
		}
		//}

		//{ Matrix Multiplication
		template<typename NUM, size_t L, size_t M, size_t N>
		Matrix<NUM, L, N> operator*(const Matrix<NUM, L, M>& lhs, const Matrix<NUM, M, N>& rhs) {
		    Matrix<NUM, L, N> result;
		    for (size_t row = 0; row < L; ++row)
            {
                for (size_t col = 0; col < N; ++col)
                {
                    result[row][col] = 0;
                    for (size_t i = 0; i < M; ++i)
                    {
                        result[row][col] += lhs[row][i] * rhs[i][col];
                    }
                }
            }
            return result;
		}
		template<typename NUM, size_t M, size_t N>
		Matrix<NUM, M, N> operator*=(Matrix<NUM, M, N>& lhs, const Matrix<NUM, N, N>& rhs) {
            return lhs = std::move(lhs * rhs);
		}
		//}

		//{ Matrix * Vector
		/*! \brief Multiplies Matrix of dimensions M*N with a vector of dimension N.
            \return the resulting vector of dimension M.
            \note A matrix of dimension N*N is the standard case.
            \warning For the sake of mathematical strictness, there is no operator*= because the LHS must be a matrix in column-major notation.
        */
        template<typename NUM, size_t M, size_t N>
        Vector<NUM, N> operator*(const Matrix<NUM, M, N>& lhs, const Vector<NUM, N>& rhs) {
            Vector<NUM, M> result;
            for (size_t row = 0; row < M; ++row)
            {
                result[row] = 0;
                for (size_t col = 0; col < N; ++col)
                {
                    result[row] += lhs[row][col] * rhs[col];
                }
            }
            return result;
        }

        /*! \brief Convenience operator to multiply the common case of Matrix4x4 by Vector3.
            \details The corresponding w-component of the imaginary Vector4 is treated as 1. Functionally
                     equal to <code>lhs * Vector4<NUM>(rhs, 1)</code>, but faster because this algorithm
                     avoids unnecessary calculations and creating a temporary Vector4.
         */
		template<typename NUM>
		Vector3<NUM> operator*(const Matrix<NUM, 4, 4>& lhs, const Vector3<NUM>& rhs) {
            return Vector3<NUM>(lhs[0][0] * rhs.x + lhs[0][1] * rhs.y + lhs[0][2] * rhs.z + lhs[0][3],
                                lhs[1][0] * rhs.x + lhs[1][1] * rhs.y + lhs[1][2] * rhs.z + lhs[1][3],
                                lhs[2][0] * rhs.x + lhs[2][1] * rhs.y + lhs[2][2] * rhs.z + lhs[2][3]);
		}
		//}

		//{ Vector * Quaternion and vice versa
        /*! \brief For the sake of simplicity, vector * quaternion and quaternion * vector is a shorthand to the actual mathematical algorithm.
            \details The target vector is converted into a quaternion by assigning it as the vector part of the quaternion and setting the real
                     part to zero. It is then multiplied like so: qpq^-1 where p is the vector as quaternion and q is the quaternion to rotate
                     by. Afterwards the vector is extracted and returned.
                     It is assumed the quaternion to rotate by is normalized.
            \see Quaternion<NUM>::normalize()
         */
        template<typename NUM>
        Vector3<NUM> operator*(const Vector3<NUM>& lhs, const Quaternion<NUM>& rhs) {
            Quaternion<NUM> tmp(0, lhs.x, lhs.y, lhs.z);
            auto conj = rhs.copy().conjugate();
            tmp = rhs * tmp * conj;
            return Vector3<NUM>(tmp.i, tmp.j, tmp.k);
        }
        /*! \brief For the sake of simplicity, vector * quaternion and quaternion * vector is a shorthand to the actual mathematical algorithm.
            \details The target vector is converted into a quaternion by assigning it as the vector part of the quaternion and setting the real
                     part to zero. It is then multiplied like so: qpq^-1 where p is the vector as quaternion and q is the quaternion to rotate
                     by. Afterwards the vector is extracted and returned.
                     It is assumed the quaternion to rotate by is normalized.
            \see Quaternion<NUM>::normalize()
         */
        template<typename NUM>
        Vector3<NUM>& operator *=(Vector3<NUM>& lhs, const Quaternion<NUM>& rhs) {
            return lhs = lhs * rhs;
        }
        /*! \brief For the sake of simplicity, vector * quaternion and quaternion * vector is a shorthand to the actual mathematical algorithm.
            \details The target vector is converted into a quaternion by assigning it as the vector part of the quaternion and setting the real
                     part to zero. It is then multiplied like so: qpq^-1 where p is the vector as quaternion and q is the quaternion to rotate
                     by. Afterwards the vector is extracted and returned.
                     It is assumed the quaternion to rotate by is normalized.
            \see Quaternion<NUM>::normalize()
         */
        template<typename NUM>
        Vector3<NUM> operator*(const Quaternion<NUM>& lhs, const Vector3<NUM>& rhs) {
            return rhs * lhs;
        }
		//}

    }
}

#endif
