#ifndef VPE_ARRAY_HPP
#define VPE_ARRAY_HPP

/*! \file
    \brief Provides a substitute for the std::vector, vpe::Array.
 */

#include <initializer_list>
#include <iterator>
#include "TypeStrategy.hpp"
#include "Math.hpp"

namespace vpe
{
    namespace internal
    {
        /*! \brief Manages moving (and copying if applicable) of the buffer of elements. */
        template<typename T, typename STRAT, bool Copyable = std::is_copy_constructible_v<T>>
        struct ArrayBuffer
        {
            STRAT strat;
            T* buffer;
            size_t size;
            size_t length;

            ArrayBuffer(size_t size) : buffer(nullptr), size(size), length(0)
            {
                buffer = strat.allocate(size);
            }
            ArrayBuffer(ArrayBuffer&& move) : buffer(move.buffer), size(move.size), length(move.length)
            {
                move.buffer = nullptr;
                move.size = move.length = 0;
            }
            ArrayBuffer& operator=(ArrayBuffer&&) = default;
            ~ArrayBuffer()
            {
                for (size_t i = 0; i < length; ++i)
                {
                    strat.destruct(buffer + i);
                }
                strat.free(buffer);
            }
        };

        template<typename T, typename STRAT>
        struct ArrayBuffer<T, STRAT, true> : public ArrayBuffer<T, STRAT, false>
        {
            STRAT strat;
            typedef ArrayBuffer<T, STRAT, false> Base;

            ArrayBuffer(size_t size) : Base(size) {}
            ArrayBuffer(const ArrayBuffer& copy) : Base(copy.size)
            {
                Base::length = copy.length;
                Base::buffer = strat.allocate(copy.size);
                strat.copy(Base::buffer, copy.buffer, Base::length);
            }
            ArrayBuffer(ArrayBuffer&&) = default;
            ArrayBuffer& operator=(const ArrayBuffer&) = default;
            ArrayBuffer& operator=(ArrayBuffer&&) = default;
            ~ArrayBuffer() = default;
        };
    }

    /*! \brief An implementation of the ArrayList concept.
        \details The array uses a congruent buffer internally to manage its contents.
                 The buffer is dynamically increased when necessary. When adding many
                 elements, it's best to either adjust Array::bufferIncrements() or
                 to Array::reserve() beforehand to limit the number of expensive resizes.
                 In contract, the buffer is not automatically shrunk as I consider it
                 a waste of CPU time, since most uses will stay around the same size even
                 when fully emptying the array. Consider manual use of Array::shrink(),
                 Array::resize() and Array::fit() when needed.
     */
    template<typename T, typename ELEM_STRAT = TypeStrategy<T>>
    class Array
    {
    protected:
        ELEM_STRAT strat;
        internal::ArrayBuffer<T, ELEM_STRAT> m_buffer;
        size_t m_bufferIncrements;

    public:
        Array(size_t slack = 10, size_t bufferIncrements = 10) : m_buffer(slack), m_bufferIncrements(bufferIncrements) {}
        template<typename STRAT = ELEM_STRAT, typename std::enable_if_t<STRAT::can_copy, bool> = 0>
        Array(const std::initializer_list<T>& elems, size_t bufferIncrements = 10) : Array(std::begin(elems), std::end(elems), bufferIncrements) {}
        Array(std::initializer_list<T>&& elems, size_t bufferIncrements = 10) : Array(std::move(std::begin(elems)), std::move(std::end(elems)), bufferIncrements) {}
        /*! \brief Copies the values within the range designated by begin and end into this new array.
            \details A move version exists for which the iterators need to be std::move'd.
         */
        template<typename ITERATOR, typename STRAT = ELEM_STRAT, typename std::enable_if_t<STRAT::can_copy, bool> = 0>
        Array(const ITERATOR& begin, const ITERATOR& end, size_t bufferIncrements = 10) : m_buffer(std::distance(begin, end)), m_bufferIncrements(bufferIncrements)
        {
            for (ITERATOR it = begin; it != end; ++it)
            {
                append(*it);
            }
        }
        /*! \brief Moves the values within the range designated by begin and end into this new array.
            \details The iterators rvalue references to distinguish this constructor from the
                     regular constructor designed for merely copying the values into this array.
         */
        template<typename ITERATOR>
        Array(ITERATOR&& begin, ITERATOR&& end, size_t bufferIncrements = 10) : m_buffer(std::distance(begin, end)), m_bufferIncrements(bufferIncrements)
        {
            for (ITERATOR it = begin; it != end; ++it)
            {
                append(std::move(*it));
            }
        }
        Array(const Array&) = default;
        Array(Array&&) = default;
        Array& operator=(const Array&) = default;
        Array& operator=(Array&&) = default;
        template<typename STRAT = ELEM_STRAT, typename std::enable_if_t<STRAT::can_copy, bool> = 0>
        Array& operator=(const std::initializer_list<T>& elems)
        {
            clear();
            fit(elems.size());
            append(elems);
        }
        Array& operator=(std::initializer_list<T>&& elems)
        {
            clear();
            fit(elems.size());
            append(std::move(elems));
        }
        ~Array() = default;

    public:
        /*! \brief Prepends the given value to the array through copying.
            \details Prepending shifts all elements in the array back by one element. Depending
                     on the underlying type, this may or may not be an expensive operation.
                     Specifically most standard-layout types can be byte-moved.
            \see TypeStrategy::move()
         */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> prepend(const T& value)
        {
            reserve(1);
            strat.move(m_buffer.buffer + 1, m_buffer.buffer, m_buffer.length);
            strat.construct(m_buffer.buffer, value);
            ++m_buffer.length;
        }

        /*! \brief Prepends the given value to the array through moving.
            \details Prepending shifts all elements in the array back by one element. Depending
                     on the underlying type, this may or may not be an expensive operation.
                     Specifically most standard-layout types can be byte-moved.
            \see TypeStrategy::move()
         */
        void prepend(T&& value)
        {
            reserve(1);
            strat.move(m_buffer.buffer + 1, m_buffer.buffer, m_buffer.length);
            strat.construct(m_buffer.buffer, std::move(value));
            ++m_buffer.length;
        }

        /*! \brief Copies the given range of values to the beginning of this array.
            \details The order of elements in the range is kept. A move-version exists
                     for which the iterators need to be std::move'd.
         */
        template<typename ITERATOR, typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> prepend(const ITERATOR& begin, const ITERATOR& end)
        {
            insert(0, begin, end);
        }

        /*! \brief Moves the given range of values into the beginning of this array.
            \details The order of elements in the range is kept. The iterators must
                     be moved to explicitly state the intent of the operation.
        */
        template<typename ITERATOR>
        void prepend(ITERATOR&& begin, ITERATOR&& end)
        {
            insert(0, std::move(begin), std::move(end));
        }

        /*! \brief Copies the given initializer list's values into the beginning of this array. */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> prepend(const std::initializer_list<T>& elems)
        {
            prepend(std::begin(elems), std::end(elems));
        }

        /*! \brief Moves the given initializer list's values into the beginning of this array. */
        void prepend(std::initializer_list<T>&& elems)
        {
            prepend(std::move(std::begin(elems)), std::move(std::end(elems)));
        }

        /*! \brief Appends the given value to the array through copying.
            \details Unlike prepend(), this is a much simpler and faster operation.
         */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> append(const T& value)
        {
            reserve(1);
            strat.construct(m_buffer.buffer + m_buffer.length, value);
            ++m_buffer.length;
        }

        /*! \brief Appends the given value to the array through moving.
            \details Unlike prepend(), this is a much simpler and faster operation.
         */
        void append(T&& value)
        {
            reserve(1);
            strat.construct(m_buffer.buffer + m_buffer.length, std::move(value));
            ++m_buffer.length;
        }

        /*! \brief Appends the values in the given range determined by begin to end to the end of the array through copying.
            \details The order of values is kept. A move-version exists for which the
                     iterators must be std::move'd.
         */
        template<typename ITERATOR, typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> append(const ITERATOR& begin, const ITERATOR& end)
        {
            insert(m_buffer.length, begin, end);
        }

        /*! \brief Appends the values in the given range determined by begin to end to the end of the array through moving.
            \details The order of values is kept. The iterators must be moved to clearly
                     distinguish the intent of moving the values into the array.
         */
        template<typename ITERATOR>
        void append(ITERATOR&& begin, ITERATOR&& end)
        {
            insert(m_buffer.length, std::move(begin), std::move(end));
        }

        /*! \brief Appends the values of the initializer list to the end of this array by copying. */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> append(const std::initializer_list<T>& elems)
        {
            append(std::begin(elems), std::end(elems));
        }

        /*! \brief Appends the values of the initializer list to the end of this array by moving. */
        void append(std::initializer_list<T>&& elems)
        {
            append(std::move(std::begin(elems)), std::move(std::end(elems)));
        }

        /*! \brief Inserts the given value into the array at the specified index through copying.
            \details
            \parblock
            The element currently at the index and all following elements will be
            shifted back one slot. Depending on the underlying type this can be a
            rather expensive operation. Most standard-layout types can be simply
            byte-moved, which resembles the optimized and thus faster solution.

            If the specified index is out of range, the value will be simply
            appended.
            \endparblock
            \see TypeStrategy::move()
         */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> insert(size_t index, const T& value)
        {
            if (index >= m_buffer.length)
            {
                append(value);
            }
            else
            {
                reserve(1);
                strat.move(m_buffer.buffer + index + 1, m_buffer.buffer + index, m_buffer.length - index);
                strat.construct(m_buffer.buffer + index, value);
                ++m_buffer.length;
            }
        }

        /*! \brief Inserts the given value into the array at the specified index through moving.
            \parblock
            The element currently at the index and all following elements will be
            shifted back one slot. Depending on the underlying type this can be a
            rather expensive operation. Most standard-layout types can be simply
            byte-moved, which resembles the optimized and thus faster solution.

            If the specified index is out of range, the value will be simply
            appended.
            \endparblock
            \see TypeStrategy::move()
         */
        void insert(size_t index, T&& value)
        {
            if (index >= m_buffer.length)
            {
                append(std::move(value));
            }
            else
            {
                reserve(1);
                strat.move(m_buffer.buffer + index + 1, m_buffer.buffer + index, m_buffer.length - index);
                strat.construct(m_buffer.buffer + index, std::move(value));
                ++m_buffer.length;
            }
        }

        /*! \brief Inserts the given range of values into the array at the specified index through copying.
            \details The order of elements is kept. A move-version exists for which the
                     iterators need to be std::move'd.
         */
        template<typename ITERATOR, typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> insert(size_t index, const ITERATOR& begin, const ITERATOR& end)
        {
            size_t count = std::distance(begin, end);
            reserve(count);
            strat.move(m_buffer.buffer + count, m_buffer.buffer, m_buffer.length - index);

            size_t i = index;
            for (ITERATOR it = begin; it != end; ++it)
            {
                strat.construct(m_buffer.buffer + i++, *it);
            }

            m_buffer.length += count;
        }

        /*! \brief Inserts the given range of values into the array at the specified index through moving.
            \details The order of elements is kept. A move-version exists for which the
                     iterators need to be std::move'd.
         */
        template<typename ITERATOR>
        void insert(size_t index, ITERATOR&& begin, ITERATOR&& end)
        {
            size_t count = std::distance(begin, end);
            reserve(count);
            strat.move(m_buffer.buffer + index + count, m_buffer.buffer + index, m_buffer.length - index);

            size_t i = index;
            for (ITERATOR it = begin; it != end; ++it)
            {
                strat.construct(m_buffer.buffer + i++, std::move(*it));
            }

            m_buffer.length += count;
        }

        /*! \brief Insers the values of the given initializer list into this array at the specified index through copying. */
        template<typename STRAT = ELEM_STRAT>
        typename std::enable_if_t<STRAT::can_copy, void> insert(size_t index, const std::initializer_list<T>& elems)
        {
            insert(index, std::begin(elems), std::end(elems));
        }

        /*! \brief Inserts the values of the given initializer list into this array at the specified index through moving. */
        void insert(size_t index, std::initializer_list<T>&& elems)
        {
            insert(index, std::move(std::begin(elems)), std::move(std::end(elems)));
        }

        /*! \brief Removes count elements from the list starting at index.
            \details The elements after the range of removed elements are shifted up
                     to fill in the gap.
         */
        void remove(size_t index, size_t count = 1)
        {
            for (size_t i = index; i < index + count; ++i)
            {
                strat.destruct(m_buffer.buffer + i);
            }
            strat.move(m_buffer.buffer + index, m_buffer.buffer + index + count, count);
            m_buffer.length -= count;
        }

        /*! \brief Clears the contents of this array.
            \details The array will not be resized. That can be done through any
                     of the resize methods.
            \see resize(size_t)
         */
        void clear()
        {
            for (size_t i = 0; i < m_buffer.length; ++i)
            {
                strat.destruct(m_buffer + i);
            }
            m_buffer.length = 0;
        }


        /*! \brief Gets the reserved number of elements for this array.
            \see length()
         */
        size_t size() const { return m_buffer.size; }

        /*! \brief Gets the number of elements in this array. */
        size_t length() const { return m_buffer.length; }


        /*! \brief Gets the element at the specified index.
            \details Assumes the index is in bounds.
         */
        T& get(size_t index) { return m_buffer.buffer[index]; }

        /*! \brief Gets an immutable reference of the element at the specified index.
            \details Assumes the index is in bounds.
         */
        const T& get(size_t index) const { return m_buffer.buffer[index]; }

        /*! \brief Proxy to get(size_t) */
        T& operator[](size_t index) { return get(index); }

        /*! \brief Proxy to get(size_t) const */
        const T& operator[](size_t index) const { return get(index); }

        //{ Buffer management
        /*! \brief Sets the new increments at which to increase the buffer size when necessary (in element count).
            \details Larger values mean less reallocations, but also a higher memory footprint than necessary.
                     The default value is 10 (arbitrarily chosen).
            \see bufferIncrements(void)
         */
        void bufferIncrements(size_t value)
        {
            m_bufferIncrements = value;
        }

        /*! \brief Gets the current size by which to increase the buffer when necessary.
            \see bufferIncrements(size_t)
         */
        size_t bufferIncrements() const
        {
            return m_bufferIncrements;
        }

        /*! \brief Reserves enough space for the given number of elements in the buffer.
            \details Specifically useful when adding a larger amount of elements to minimize the
                     number of dynamic reallocations.
                     Does nothing if the buffer's current size already has enough space for the
                     given number of elements.
                     The buffer will be rounded to the next highest multiple of the configured
                     buffer increments.
            \see bufferIncrements(size_t)
                 resize(size_t)
                 fit(size_t)
                 shrink()
         */
        void reserve(size_t count)
        {
            if (m_buffer.size < m_buffer.length + count)
            {
                fit(m_buffer.length + count);
            }
        }

        /*! \brief Resizes the array's internal buffer to the exact size.
            \see reserve(size_t)
                 fit(size_t)
                 shrink()
         */
        void resize(size_t count)
        {
            T* oldBuffer = m_buffer.buffer;
            m_buffer.buffer = strat.allocate(count);

            strat.move(m_buffer.buffer, oldBuffer, Math::min(count, m_buffer.length));
            for (size_t i = count; i < m_buffer.length; ++i)
            {
                strat.destruct(m_buffer.buffer + i);
            }
            strat.free(oldBuffer);

            m_buffer.size = count;
            m_buffer.length = Math::min(count, m_buffer.length);
        }

        /*! \brief Resizes the array's internal buffer to fit at least the specified number of elements.
            \details The resulting size is a multiple of the configured buffer increments.
            \see bufferIncrements(size_t)
                 resize(size_t)
                 reserve(size_t)
                 shrink()
         */
        void fit(size_t count)
        {
            resize(Math::ceil((float)count / m_bufferIncrements) * m_bufferIncrements);
        }

        /*! \brief Shrinks the buffer to the exact size of the contained elements.
            \see reserve(size_t)
                 resize(size_t)
                 fit(size_t)
         */
        void shrink()
        {
            if (m_buffer.size > m_buffer.length)
            {
                resize(m_buffer.length);
            }
        }
        //}

        //{ Iterators
        T* begin() { return m_buffer.buffer; }
        const T* begin() const { return cbegin(); }
        const T* cbegin() const { return m_buffer.buffer; }
        std::reverse_iterator<T*> rbegin() { return m_buffer.buffer + m_buffer.length - 1; }
        const std::reverse_iterator<T*> rbegin() const { return crbegin(); }
        const std::reverse_iterator<T*> crbegin() const { return m_buffer.buffer + m_buffer.length - 1; }

        T* end() { return m_buffer.buffer + m_buffer.length; }
        const T* end() const { return cend(); }
        const T* cend() const { return m_buffer.buffer + m_buffer.length; }
        std::reverse_iterator<T*> rend() { return m_buffer.buffer - 1; }
        const std::reverse_iterator<T*> rend() const { return crend(); }
        const std::reverse_iterator<T*> crend() const { return m_buffer.buffer - 1; }
        //}

    };
}

//{ Iterator Traits
namespace std
{
    template<typename T>
    struct iterator_traits<vpe::Array<T>> : iterator_traits<T*>
    {
        typedef iterator_traits<T*> base;
        using base::difference_type;
        using base::value_type;
        using base::pointer;
        using base::reference;
        using base::iterator_category;
    };
}
//}

#endif // VPE_ARRAY_HPP
