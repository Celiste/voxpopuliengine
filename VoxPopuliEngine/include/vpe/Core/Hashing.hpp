#ifndef VPE_HASHING_H
#define VPE_HASHING_H

#include "Integrals.hpp"

namespace vpe
{
	typedef uint64_t hash_t;

	inline hash_t calculateHash(void) { return 0; }
	inline hash_t calculateHash(bool truth) { return truth ? 1 : 0; }
	template<typename T> inline uint64_t calculateHash(T* Ptr) { return reinterpret_cast<hash_t>(Ptr); }
	inline hash_t calculateHash(char number) { return (hash_t)number; }
	inline hash_t calculateHash(unsigned char number) { return (hash_t)number; }
	inline hash_t calculateHash(short number) { return (hash_t)number; }
	inline hash_t calculateHash(unsigned short number) { return (hash_t)number; }
	inline hash_t calculateHash(int number) { return (hash_t)number; }
	inline hash_t calculateHash(unsigned int number) { return (hash_t)number; }
	inline hash_t calculateHash(long number) { return (hash_t)number; }
	inline hash_t calculateHash(unsigned long number) { return (hash_t)number; }
	inline hash_t calculateHash(long long number) { return (hash_t)number; }
	inline hash_t calculateHash(unsigned long long number) { return number; }
	inline hash_t calculateHash(double number) { return *reinterpret_cast<hash_t*>(&number); }
	inline hash_t calculateHash(float number) { return calculateHash((double)number); }

	/*! \brief Josh Bloch's suggestion in Effective Java.
		\details Essentially the same as 17 + first * 31 + second * 31
		\warning Add an arbitrary constant before returning your hash, preferrably 17 or 5381, to further reduce potential clashes!
	 */
	inline hash_t quickCombineHash(hash_t first, hash_t second)
	{
		return ((first << 5) - first) + ((second << 5) - second);
	}

	/*! \brief Modified gawk version
		\warning Add an arbitrary constant before returning your hash, preferrably 17 or 5381, to further reduce potential clashes!
	 */
	inline hash_t complexCombineHash(hash_t first, hash_t second)
	{
		return second + (first << 6) + (first << 16) - first;
	}

	/**
	* This is not a simple XOr but Dan Bernstein's newer hash combining algorithm.
	* CAVEAT: Add an arbitrary constant before returning your hash, preferrably 17 or 5381, to further reduce potential clashes!
	*/
	inline hash_t xorCombineHash(hash_t first, hash_t second)
	{
		return ((second << 5) + first) ^ first;
	}

}

#endif /* VP_HASH_H */
