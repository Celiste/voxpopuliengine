#ifndef VPE_BinarySemaphore_HPP
#define VPE_BinarySemaphore_HPP

/*! \file
    \brief Provides various implementations of the Semaphore pattern and Semaphore-related utilities.
    \author Celiste aka. Kiruse
 */

#include "vpe.h"
#include <atomic>

//VPE_EXPORT_FOREIGN(class VPE_API std::atomic_bool;)

namespace vpe
{
    /*! \brief A modification of the BinarySemaphore pattern.
        \details Our implementation uses different terminology (test, acquire, release and wait).
                 The terms should be self-explanatory for the most part. Wait, unlike the general
                 consensus, is not the go-to method for this implementation as it implements a busy
                 wait. Instead, acquire should be used as it merely attempts acquisition and reports
                 success or failure.
     */
    class BinarySemaphore
    {
        std::atomic_bool locked;

    public:
        BinarySemaphore() : locked(false) {}
        BinarySemaphore(const BinarySemaphore&) = delete;
        BinarySemaphore(BinarySemaphore&&) = delete;
        BinarySemaphore& operator=(const BinarySemaphore&) = delete;
        BinarySemaphore& operator=(BinarySemaphore&&) = delete;
        ~BinarySemaphore() = default;

    public:
        /*! \brief Tests if the semaphore is currently already acquired. */
        bool test() { return !locked; }

        /*! \brief Attempts to acquire the semaphore.
            \return True if the semaphore was successfully acquired, otherwise false.
         */
        bool acquire()
        {
            if (locked) return false;
            locked = true;
            return true;
        }

        /*! \brief Attempts to acquire the semaphore.
            \details Implemented as a busy wait, thus designed for brief time periods.
         */
        void wait()
        {
            while (locked);
            locked = true;
        }

        /*! \brief Releases the semaphore. */
        void release()
        {
            locked = false;
        }

    public: // ScopeLock Utility
        /*! \brief RAII-based BinarySemaphore acquisition and release. */
        class ScopeLock
        {
            BinarySemaphore& semaphore;

        public:
            ScopeLock(BinarySemaphore& semaphore) : semaphore(semaphore)
            {
                semaphore.wait();
            }
            ScopeLock(const ScopeLock&) = delete;
            ScopeLock(ScopeLock&&) = delete;
            ScopeLock& operator=(const ScopeLock&) = delete;
            ScopeLock& operator=(ScopeLock&&) = delete;
            ~ScopeLock()
            {
                semaphore.release();
            }
        };
    };
}

#endif // VPE_BinarySemaphore_HPP
