#ifndef VPE_HASHSET_HPP
#define VPE_HASHSET_HPP

/*! \file
	\author  Brian Cobile aka. Kiruse aka. Celiste
	\brief   Implemenets our custom hash set algorithm.
	\details Unlike many other standard implementations this hash set avoids using an internal list or second indirection
			 by dividing the allocated memory section directly into buckets of quasi-constant size.
 */

#include <algorithm>
#include <initializer_list>
#include <iterator>
#include "Exception.hpp"
#include "Integrals.hpp"
#include "Hashing.hpp"
#include "Math.hpp"
#include "Simpletons.hpp"
#include "TypeStrategy.hpp"

namespace vpe
{
	namespace internal
	{
		template<typename T, typename ELEM_STRAT, bool Copyable = std::is_copy_constructible_v<T>>
        class HashSetBuffer
        {
        public:
            ELEM_STRAT strat;
            T* buffer;
            size_t m_size;

        public:
            HashSetBuffer(size_t p_size) : buffer(nullptr), m_size(p_size)
            {
                buffer = strat.allocate(p_size);
            }
            HashSetBuffer(const HashSetBuffer& copy) : buffer(nullptr), m_size(copy.m_size)
            {
                buffer = strat.allocate(m_size);
                strat.copy(buffer, copy.buffer, m_size);
            }
            HashSetBuffer(HashSetBuffer&& move) : buffer(move.buffer), m_size(move.m_size)
            {
                move.buffer = nullptr;
            }
            HashSetBuffer& operator=(const HashSetBuffer& copy)
            {
                if (&copy != this)
                {
                    this->~HashSetBuffer();
                    new (this) HashSetBuffer(copy);
                }
                return *this;
            }
            HashSetBuffer& operator=(HashSetBuffer&& move)
            {
                if (&move != this)
                {
                    this->~HashSetBuffer();
                    new (this) HashSetBuffer(std::move(move));
                }
                return *this;
            }
            ~HashSetBuffer()
            {
                if (buffer)
                {
                    strat.free(buffer);
                    m_size = 0;
                    buffer = nullptr;
                }
            }
        };

        template<typename T, typename ELEM_STRAT>
        class HashSetBuffer<T, ELEM_STRAT, false>
        {
        public:
            ELEM_STRAT strat;
            T* buffer;
            size_t m_size;

        public:
            HashSetBuffer(size_t p_size) : buffer(nullptr), m_size(p_size)
            {
                buffer = strat.allocate(p_size);
            }
            HashSetBuffer(HashSetBuffer&& move) : buffer(move.buffer), m_size(move.m_size)
            {
                move.buffer = nullptr;
            }
            HashSetBuffer& operator=(HashSetBuffer&& move)
            {
                if (&move != this)
                {
                    this->~HashSetBuffer();
                    new (this) HashSetBuffer(std::move(move));
                }
                return *this;
            }
            ~HashSetBuffer()
            {
                if (buffer)
                {
                    strat.free(buffer);
                    buffer = nullptr;
                    m_size = 0;
                }
            }
        };

	}


	/*! \brief An element of the HashSet class.
		\details A highly specialized type for internal use within the HashSet. It's recommended to not
				 deviate from this type, and if so, to carefully implement the move constructor and a
				 constructor like SetElement(T&&). Accordingly the wrapped type must be movable in any
				 case. A copyable type is then reduced to the movable case in the HastSet class itself.
	 */
	template<typename T, bool Copyable = std::is_copy_constructible_v<T>>
	struct SetElement
	{
	    static_assert(std::is_move_constructible_v<T>, "T must at least be movable");

	public:
		bool valid;
		hash_t hash;
		T value;

	public:
	    SetElement(const T& value) : valid(true), hash(calculateHash(value)), value(value) {}
		SetElement(T&& value) : valid(true), hash(calculateHash(value)), value(std::move(value)) {}
		SetElement(const SetElement& copy) : valid(copy.valid), hash(copy.hash), value(copy.value) {}
		SetElement(SetElement&& move) : valid(move.valid), hash(move.hash), value(std::move(move.value)) {}
		~SetElement() { valid = false; }

		inline bool operator==(const SetElement& other) const
		{
			return valid && other.valid && value == other.value;
		}

		inline bool operator!=(const SetElement& other) const { return !(*this == other); }

		inline operator T&() { return value; }

		inline operator const T&() const { return value; }
	};

	template<typename T>
	struct SetElement<T, false>
	{
    public:
        bool valid;
        hash_t hash;
        T value;

    public:
        SetElement(T&& value) : valid(true), hash(calculateHash(value)), value(std::move(value)) {}
        SetElement(SetElement&& move) : valid(move.valid), hash(move.hash), value(std::move(move.value)) {}
        ~SetElement() { valid = false; }

        inline bool operator==(const SetElement& other) const
        {
            return valid && other.valid && value == other.value;
        }

        inline operator!=(const SetElement& other) const { return !(*this == other); }

        inline operator T&() { return value; }

        inline operator const T&() const { return value; }
	};

	/*! \brief A generic implementation of a hash set.
		\details
		\parblock
		A mathematical set is an unordered collection of elements of any kind. However, since
		C++ is typesafe, this implementation limits the possible types of the elements to the
		same kind (T).

		Like many standard implementations, this one uses a bucket concept as well. However,
		unlike other implementations, this one does not use "external" buckets (e.g. linked
		list or vectors). The buckets are embedded within the first degree buffer. This buffer
		is essentially divided into equally sized sections representing our buckets. An element
		may only be situated within the corresponding bucket. If a bucket reaches its maximum
		size, first the bucket size will be increased relative to the size of the entire buffer,
		then the buffer itself. Bucket size should be around 5% (rounded up) + 2. The additional
		+2 are intended to avoid early resizes for small sets.

		This concept avoids one additional indirection and thus increases performance.

		An increment can be passed to the constructor to specify how many dummy elements shall
		be added to the set when expanding. This allows for better performance when working with
		sets of priorly unknown size. If you expect more elements in a brief period of time or
		simply aim to avoid resizing the set often specify a larger increment.

		If the number of elements about to be added is known a call to reserve() ensures
		the set can hold at least the specified amount of additional elements before resizing.

		The set should only be grown unless you know the implications of shrinking the set to
		a lower element count. Other than that resize() will always resize the set to hold
		multiples of your specified interval - i.e. if the increment is 10 the set will hold
		10, 20, 30, ... elements.

		When changing an element through its reference in this set, be sure to let the set
		reprocess the element by calling reprocess(). This will update the internal buffer and
		possibly relocate the element in the set to guarantee proper and expected behavior in
		the future.
		\endparblock
		\todo Introduce optional threadsafety!
	 */
	template<typename T, class SET_ELEM = SetElement<T>, class ELEM_STRAT = TypeStrategy<SET_ELEM>>
	class HashSet
	{
	private:
		/*! \brief Mutable (if C == false) or immutable (otherwise) iterator over the elements in the associated set and in no particular order to begin with.
			\details C means mutable, R means reversed order.
			\note Cannot/shouldn't satisfy the RandomAccessIterator concept because elements are not continuous.
		 */
		template<bool C, bool R = false>
		class IteratorBase : public std::iterator<std::bidirectional_iterator_tag, typename toggle_cv<C, false, T>::type, size_t>
		{
			typedef typename toggle_cv<C, false, HashSet>::type set_t;
			typedef typename toggle_cv<C, false, T>::type elem_t;

		protected:
			typename toggle_cv<C, false, HashSet>::type* iteratedSet;
			size_t index;

		public:
			IteratorBase(set_t* set, bool end = false) : iteratedSet(set)
			{
				if (!R)
				{
					if (!end)
					{
						index = 0;
						while (this->invalidButInBounds()) ++index;
					}
					else
					{
						index = set->buffer.m_size;
					}
				}
				else
				{
					if (!end)
					{
						index = set->buffer.m_size - 1;
						while (this->invalidButInBounds()) --index;
					}
					else
					{
						index = -1;
					}
				}
			}
			IteratorBase(const IteratorBase& other) = default;
			IteratorBase(IteratorBase&& other) = default;
			IteratorBase& operator=(const IteratorBase& other) = default;
			IteratorBase& operator=(IteratorBase&& other) = default;
			virtual ~IteratorBase() = default;

		public:
			virtual inline operator bool() const
			{
				return index >= 0 && index < iteratedSet->buffer.m_size && (bool)iteratedSet->buffer.buffer[index].valid;
			}

			virtual IteratorBase& operator++()
			{
				do {
					if (R) --index;
					else ++index;
				} while (invalidButInBounds());
				return *this;
			}

			virtual inline IteratorBase operator++(int)
			{
				IteratorBase copy(*this);
				++(*this);
				return copy;
			}

			virtual IteratorBase& operator--()
			{
				do {
					if (R) ++index;
					else --index;
				} while (invalidButInBounds());
				return *this;
			}

			virtual inline IteratorBase operator--(int)
			{
				IteratorBase copy(*this);
				--(*this);
				return copy;
			}

			virtual inline bool operator==(const IteratorBase& other)
			{
				return iteratedSet == other.iteratedSet && index == other.index;
			}

			virtual inline bool operator!=(const IteratorBase& other)
			{
				return !((*this) == other);
			}

			virtual elem_t& operator*() const
			{
				return iteratedSet->buffer.buffer[index].value;
			}

			virtual elem_t* operator->() const
			{
				return &iteratedSet->buffer.buffer[index].value;
			}

		protected:
			virtual inline bool invalidButInBounds()
			{
				return index < iteratedSet->buffer.m_size && !iteratedSet->buffer.buffer[index].valid;
			}
		};

    private:
        ELEM_STRAT strat;

	public:
		typedef IteratorBase<false, false> Iterator;
		typedef IteratorBase<false, true> RIterator;
		typedef IteratorBase<true,  false> ConstIterator;
		typedef IteratorBase<true,  true> ConstRIterator;

	protected:
        internal::HashSetBuffer<SET_ELEM, ELEM_STRAT> buffer;
		size_t _length;
		size_t bufferIncrements;
		size_t maxBucketSize;
		size_t bucketSize;

	public:
		HashSet(size_t slack = 10, size_t bufferIncrements = 10)
			: buffer(0)
			, _length(0)
			, bufferIncrements(bufferIncrements)
			, bucketSize(Math::ceil(slack * 0.05))
			, maxBucketSize(0)
		{
			resize(slack, false, false, true);
		}

		HashSet(const HashSet& other) = default;
		HashSet(HashSet&& other) = default;

		HashSet& operator=(const HashSet& other) = default;
		HashSet& operator=(HashSet&& other) = default;

		virtual ~HashSet()
		{
			if (buffer.buffer)
			{
				for (size_t iCurr = 0; iCurr < buffer.m_size; ++iCurr)
				{
					if (buffer.buffer[iCurr].valid)
					{
						strat.destruct(buffer.buffer + iCurr);
					}
				}
			}
		}

	public:
		/*! \brief Copies the given element into this set.
			\details The type must define the equality comparison operator.
			\returns nullptr if the element could not be added due to the set size being increased too many times (see VP_MAX_SET_INCREASES_PER_ADD constant).
		 */
		template<typename T1 = T>
		inline auto add(const T& elem) -> decltype(T1(elem), SET_ELEM(std::declval<T1>()))*
		{
			return add(std::move(T(elem)));
		}

		/*! \brief Moves the given element into this set.
			\details The type must define the equality comparison operator.
			\returns nullptr if the element could not be added due to the set size being increased too many times (see VP_MAX_SET_INCREASES_PER_ADD define).
		 */
		inline SET_ELEM* add(T&& elem)
		{
			// Try finding the element first
			if (auto Elem = find(elem)) return Elem;
			// Only add if not already found
			return addImpl(std::move(elem), calculateHash(elem));
		}

		/*! \brief Adds the passed initializer list to this set. */
		inline void add(const std::initializer_list<T>& elems)
		{
			add(std::begin(elems), std::end(elems));
		}

		template<typename ITER>
		void add(const ITER& begin, const ITER& end)
		{
			reserve(begin, end);

			for (ITER it = begin; it != end; ++it)
			{
				addImpl(std::move(T(*it)), calculateHash(*it), true);
			}
		}

		/*! \brief Copies the element into this set.
			\details If the element is already in the set, nothing happens.
		 */
		template<typename T1 = T>
		inline auto operator+=(const T& elem) -> decltype(T1(elem), HashSet())&
		{
			addImpl(std::move(T(elem)), calculateHash(elem));
			return *this;
		}

		/*! \brief Moves the element into this set and returns the set itself.
			\details If the element is already in the set, nothing happens. However, the element is moved regardless.
		 */
		inline HashSet& operator+=(T&& elem)
		{
			hash_t hash = calculateHash(elem);
			addImpl(std::move(elem), hash);
			return *this;
		}

		/*! \brief Adds the passed initializer list to this set. */
		inline HashSet& operator+=(const std::initializer_list<T>& elems)
		{
			add(elems);
			return *this;
		}

		/*! \brief Attempts to find, then remove the specified element from this set.
			\details If the element was found, nothing happens.
		 */
		inline bool remove(const T& findme)
		{
			SET_ELEM* elem = find(findme);
			return !!elem ? remove(*elem) : false;
		}

		/*! \brief removes the specified element from this set.
			\warning Assumes the passed element is actually a member of this set. If it is not, behavior is undefined.
		 */
		inline bool remove(SET_ELEM& removeme)
		{
			// Destruct the element. However, since we're not using conventional methods here,
			// we still operate on the destructed but valid memory region.
			strat.destruct(&removeme);
			removeme.valid = false;
			--_length;
			return true;
		}

		/*! \brief Removes the passed element from this set.
			\details If the element is not in the list, nothing happens.
		 */
		inline HashSet& operator-=(const T& elem)
		{
			remove(elem);
			return *this;
		}

		/*! \brief Find an element in the set and return its set element. */
		SET_ELEM* find(const T& findme)
		{
			hash_t hash = calculateHash(findme);

			// Element is at exact index
			size_t index = static_cast<size_t>(hash % buffer.m_size);
			if (buffer.buffer[index].valid && buffer.buffer[index].value == findme) return buffer.buffer + index;

			// Element might be in bucket
			for (size_t i = getBucketStart(index), end = i + bucketSize; i < end; ++i)
			{
				if (buffer.buffer[i].valid && buffer.buffer[i].value == findme) return buffer.buffer + i;
			}

			return nullptr;
		}

		/*! \brief Find an element in the set and return its immutable element. */
		const SET_ELEM* find(const T& findme) const
		{
			hash_t hash = calculateHash(findme);

			// Element is at exact index
			size_t index = static_cast<size_t>(hash % buffer.m_size);
			if (buffer.buffer[index].valid && buffer.buffer[index].value == findme) return buffer.buffer + index;

			// Element might be in bucket
			for (size_t i = getBucketStart(index), end = i + bucketSize; i < end; ++i)
			{
				if (buffer.buffer[i].valid && buffer.buffer[i].value == findme) return buffer.buffer + i;
			}

			return nullptr;
		}

		/*! \brief Checks if the passed element is stored in this set.
			\details The element is located by calculating its index based off its hash code and within a marginal index error called bucket size.
		 */
		inline bool has(const T& findme) const
		{
			return find(findme) != nullptr;
		}

		/*! \brief An internally used utility function which checks if the element at the specified index is valid and its value is equal to the passed in element.
			\returns true if so, otherwise false. False is also returned if the specified index is out of bounds.
		 */
		inline bool is(size_t index, const T& element) const
		{
			if (index >= buffer.m_size) throw IndexOutOfRangeException();
			return buffer.buffer[index].valid && buffer.buffer[index].value == element;
		}

		/*! \brief Checks if the element at the given index within the internal buffer is considered faulty, i.e. invalid or inside an incorrect bucket resulting from a former resize of this set. */
		inline bool isFaulty(size_t index)
		{
			return index >= buffer.m_size || !buffer.buffer[index].valid || getBucketStart(buffer.buffer[index].hash % buffer.m_size) != getBucketStart(index);
		}

		/*! \brief Reprocess the given element after it has been altered and thus its hash code changed.
			\details The element must be a member of this set.
			\exception InvalidArgumentException if the element was not part of the set.
		 */
		inline void reprocess(const T& element)
		{
			SET_ELEM* CurrElem = find(element);
			if (!CurrElem) throw InvalidArgumentException("Element not in set");
			reprocess(*CurrElem);
		}

		/*! \brief Reprocess the given element after it has been altered and thus its hash code changed.
			\details It will likely be moved to a different location within the internal buffer.
		 */
		inline void reprocess(SET_ELEM& element)
		{
			// Invalidate the previous element container and move its contents using our previously
			// defined add algorithm.
			T tmp(std::move(element.value));
			remove(element);
			addImpl(std::move(tmp), calculateHash(tmp));
		}

		/*! \brief Rehashes this set's elements.
			\details In the process thereof the bucket size may be adjusted and the elements moved to a
					 new location. Set resetBucketSize to true to force adjusting the bucket size as if
					 it started out at 1.
		 */
		inline bool rehash(bool resetBucketSize = false, bool allowResize = true)
		{
			return rehash((T*)nullptr, (T*)nullptr, resetBucketSize, allowResize);
		}

		/*! \brief Rehashes this set's elements.
			\details In the process thereof the bucket size may be adjusted and the elements moved to a
					 new location. Set resetBucketSize to true to force adjusting the bucket size as if
					 it started out at 1.
		 */
		template<typename ITER>
		bool rehash(const ITER& rangeBegin, const ITER& rangeEnd, bool resetBucketSize = false, bool allowResize = true)
		{
			if (resetBucketSize) bucketSize = 1;

			// Recalculate hashes of all elements once before rearranging in-place
			for (size_t i = 0; i < buffer.m_size; ++i)
			{
				if (buffer.buffer[i].valid)
				{
					buffer.buffer[i].hash = calculateHash(buffer.buffer[i].value);
				}
			}

			// Ensure the set can even be properly rehashed within a reasonable margin of resizes
			size_t predictedSetSize = buffer.m_size, predictedBucketSize = bucketSize;
			if (!pretestRehash(predictedSetSize, predictedBucketSize, allowResize, rangeBegin, rangeEnd)) return false;

			if (predictedSetSize != buffer.m_size) resize(predictedSetSize, false, false);
			bucketSize = predictedBucketSize;

			bool repeat = false;

			// Rearrange elements in-place
			for (size_t i = 0; i < buffer.m_size; ++i)
			{
				size_t currIndex = i;
				SET_ELEM* currElem = buffer.buffer + currIndex;

				do {
					repeat = false;

					if (currElem->valid && !rehash_isCorrect(currIndex))
					{
						size_t targetIndex = static_cast<size_t>(currElem->hash % buffer.m_size);
						SET_ELEM* targetElem = rehash_findFaultyInBucket(targetIndex);
						if (!targetElem) throw InvalidStateException("Rehashing failed with invalid bucket target");

						// If the element is valid, it needs to be reprocessed next
						if (targetElem->valid)
						{
							strat.swap(targetElem, currElem);
							repeat = true;
						}
						else {
							strat.move(targetElem, currElem);
							strat.destruct(currElem);
							currElem->valid = false;
						}
					}
				} while (repeat);
			}

			return true;
		}

	protected:
		SET_ELEM* addImpl(T&& element, hash_t hash, bool allowResize = true)
		{
			// Increase the set if its full already
			if (_length >= buffer.m_size)
			{
				if (allowResize) resize(buffer.m_size + bufferIncrements, false, true, false);
				else throw InvalidStateException("HashSet capacity reached with automatic resizing disabled");
			}

			// Find an element slot near the original index of the element
			SET_ELEM* elem = nullptr;

			do
			{
				size_t index = static_cast<size_t>(hash % buffer.m_size);
				elem = findFaultyInBucket(index);
				if (!elem)
				{
					// Allow resizing in general?
					if (allowResize && shouldIncreaseBucketOrSet(buffer.m_size, bucketSize, maxBucketSize))
					{
						resize(buffer.m_size + bufferIncrements, false);
					}

					// No resizing allowed - forcefully increment bucket size but assert
					else {
						if (maxBucketSize > 0 && bucketSize >= maxBucketSize) throw InvalidStateException("Bucket size exceeds limit and resizing disallowed");
						++bucketSize;
					}
				}
			} while (!elem);

			// If the element is already valid but still faulty, add it to the set
			if (elem->valid) {
				T tmp(std::move(elem->value));
				hash_t otherHash = elem->hash;
				elem->value = std::move(element);
				elem->hash = hash;
				addImpl(std::move(tmp), otherHash, allowResize);
				// No need to increment length because we're replacing a value
			}

			// Set the element's value
			else {
				// Finally initialize the previously invalid element
				strat.construct(elem, std::move(element));
				elem->valid = true;
				elem->hash = hash;

				// Increment the cached length
				++_length;
			}

			return elem;
		}

		/*! \brief Attempts to find a "faulty" element in the bucket containing the specified element.
			\details Prefers the element at the specified index, otherwise the closest to the beginning of the bucket.
		 */
		SET_ELEM* findFaultyInBucket(size_t index)
		{
			if (!buffer.buffer[index].valid) return buffer.buffer + index;

			for (size_t currIndex = getBucketStart(index), endIndex = currIndex + bucketSize; currIndex < endIndex; ++currIndex)
			{
				if (currIndex < buffer.m_size && isFaulty(currIndex)) return buffer.buffer + currIndex;
			}
			return nullptr;
		}

		static inline size_t getRelativeBucketSizeLimit(size_t size)
		{
			return std::max<size_t>(3u, size / 10u);
		}

		/*! \brief Increases the given set or bucket size depending on bucket/set-size relationship.
			\returns true if the set should be increased, otherwise false if the bucket should be increased.
		 */
		static inline bool shouldIncreaseBucketOrSet(size_t setSize, size_t bucketSize, size_t maxBucketSize)
		{
			// Check if the increased bucket size would exceed the limit
			return bucketSize >= getRelativeBucketSizeLimit(setSize) || (maxBucketSize > 0 && bucketSize >= maxBucketSize);
		}

		void resizeImpl(size_t newSize, bool shouldRecount, bool shouldRehash, bool strict)
		{
			bool actuallyRecount = false;

			if (!strict)
			{
				size_t predictedSize = newSize, predictedBucketSize = bucketSize;
				if (!pretestRehash(predictedSize, predictedBucketSize, true, (T*)0, (T*)0)) throw InvalidStateException("Cannot successfully rehash");
				newSize = predictedSize;
				bucketSize = predictedBucketSize;
			}

			// Destroy elements that will be lost
			if (newSize < buffer.m_size)
			{
				actuallyRecount = true;
				for (size_t currIndex = newSize; currIndex < buffer.m_size; ++currIndex)
				{
					if (buffer.buffer[currIndex].valid)
					{
						strat.destruct(buffer.buffer + currIndex);
					}
				}
			}

			// If trivial, simply reallocate
			if (ELEM_STRAT::is_standard_layout)
			{
				buffer.buffer = strat.reallocate(buffer.buffer, newSize);
			}

			// Otherwise move the elements into a new buffer
			else
			{
				SET_ELEM* tmp = strat.allocate(newSize);

				for (size_t i = 0, end = std::min(newSize, buffer.m_size); i < end; ++i)
				{
					if (buffer.buffer[i].valid)
					{
						strat.move(tmp + i, buffer.buffer + i);
						buffer.buffer[i].valid = false;
						tmp[i].valid = true;
					}
					else
					{
						tmp[i].valid = false;
					}
				}

				strat.free(buffer.buffer);
				buffer.buffer = tmp;
			}

			// Invalidate the new elements
			if (newSize > buffer.m_size)
			{
				for (size_t currIndex = buffer.m_size; currIndex < newSize; ++currIndex)
				{
					buffer.buffer[currIndex].valid = false;
				}
			}

			// Track the new size
			buffer.m_size = newSize;

			// Possibly count the new number of elements
			if (shouldRecount && actuallyRecount)
			{
				count();
			}

			if (shouldRehash)
			{
				rehash(false, !strict);
			}
		}

		inline size_t getBucketStart(size_t index) const
		{
			return (index / bucketSize) * bucketSize;
		}

		/*! \brief Tests if rehashing the set for the given size would fit all current elements in the set plus the additional ones specified by the range. */
		template<typename ITER>
		bool pretestRehash(size_t& size, size_t& bucketSize, bool allowResize, const ITER& moreBegin, const ITER& moreEnd) const
		{
			size_t predictedSize = size;
			size_t predictedBucketSize = bucketSize;

			bool reprocess, success = true;

			do
			{
				reprocess = false;
				size_t* bucketCounts = new size_t[Math::ceil((float)predictedSize / predictedBucketSize)];
				std::memset(bucketCounts, 0, predictedSize / predictedBucketSize * sizeof(size_t));

				for (size_t i = 0; !reprocess && i < std::min(predictedSize, buffer.m_size); ++i)
				{
					if (buffer.buffer[i].valid)
					{
						size_t bucketIndex = static_cast<size_t>((buffer.buffer[i].hash % predictedSize) / predictedBucketSize);
						if (++bucketCounts[bucketIndex] > predictedBucketSize)
						{
							if (allowResize && shouldIncreaseBucketOrSet(predictedSize, predictedBucketSize, maxBucketSize))
							{
								predictedSize += bufferIncrements;
							}
							else
							{
								++predictedBucketSize;
							}
							reprocess = true;
						}
					}
				}

				if (!reprocess)
				{
					for (ITER it = moreBegin; !reprocess && it != moreEnd; ++it)
					{
						if (has(*it)) continue;

						size_t bucketIndex = static_cast<size_t>((calculateHash(*it) % predictedSize) / predictedBucketSize);
						if (++bucketCounts[bucketIndex] > predictedBucketSize)
						{
							if (allowResize && shouldIncreaseBucketOrSet(predictedSize, predictedBucketSize, maxBucketSize))
							{
								predictedSize += bufferIncrements;
							}
							else
							{
								++predictedBucketSize;
								if (predictedBucketSize > getRelativeBucketSizeLimit(predictedSize) || (maxBucketSize > 0 && predictedBucketSize > maxBucketSize))
									throw InvalidStateException("HashSet resizing prohibited and (relative) max bucket size reached");
							}
							reprocess = true;
						}
					}
				}

				delete[] bucketCounts;
			} while (reprocess);

			size = predictedSize;
			bucketSize = predictedBucketSize;

			return success;
		}

		/*! \brief Tests if the element at the specified index is still situated within the correct bucket, e.g. after a set resize. */
		inline bool rehash_isCorrect(size_t index) const
		{
			return buffer.buffer[index].valid && getBucketStart(index) == getBucketStart(buffer.buffer[index].hash % buffer.m_size);
		}

		/*! \brief Variation of findFaultyInBucket(size_t) which finds either invalid elements or elements at an incorrect index. */
		SET_ELEM* rehash_findFaultyInBucket(size_t index) const
		{
			if (!rehash_isCorrect(index)) return buffer.buffer + index;

			for (size_t currIndex = getBucketStart(index), endIndex = currIndex + bucketSize; currIndex < endIndex; ++currIndex)
			{
				if (currIndex < buffer.m_size && !rehash_isCorrect(currIndex)) return buffer.buffer + currIndex;
			}
			return nullptr;
		}

	public:
		/*! \brief Determine the mean values this set and the other set share. */
		inline void meanWith(const HashSet& other)
		{
			// Iterate through this set's elements and treat those elements as invalid that
			// are not contained within the other set.
			for (size_t currIndex = 0; currIndex < buffer.m_size; ++currIndex)
			{
				auto& curr = buffer.buffer[currIndex];
				if (curr.valid && !other.has(curr.value))
				{
					remove(curr);
				}
			}
		}

		/*! \brief Add all values in the other set to this set. */
		inline void unionWith(const HashSet& other)
		{
			// Ensure we have at least enough space to fit the entire other set into this one
			reserve(other.cbegin(), other.cend());

			// Iterate through the other set's elements and add them individually
			for (const auto& elem : other)
			{
				add(elem);
			}
		}

		/*! \brief Determine the values this set has that the other set doesn't. */
		void differenceTo(const HashSet& other)
		{
			// Iterate through this set's elements and treat those elements as invalid that are
			// contained within the other set.
			for (size_t currIndex = 0; currIndex < buffer.m_size; ++currIndex)
			{
				auto& curr = buffer.buffer[currIndex];
				if (curr.valid && other.has(curr.value)) {
					remove(curr);
				}
			}
		}

		/*! \brief Calculates the complement of this set within the base set.
			\details For simplicity's sake assumes this set is a subset of the base set.
		 */
		void complementIn(const HashSet& base) {
			// Create a copy of the base set
			HashSet baseCopy(base);

			// remove all of the original set's elements from the copy
			for (size_t currIndex = 0; currIndex < baseCopy.buffer.m_size; ++currIndex) {
				auto& curr = baseCopy.buffer.buffer[currIndex];
				if (curr.valid && has(curr.value)) {
					baseCopy.remove(curr);
				}
			}

			// Overwrite the values of this set
			*this = std::move(baseCopy);
		}

		/*! \brief Checks if the lhs set is equal to the rhs set.
			\details Mathematically we'd check if the lhs set is a subset of the rhs set and vice
					 versa, but since we're working informatically we have always iterative sets,
					 i.e. sets explicitly naming their members. This means we only need to check
					 if one set contains the other and if both sets have the same size. Because
					 if they do, obviously then the other set also contains the one set.
		 */
		inline bool operator==(const HashSet& other) const
		{
			// Number of elements must match
			if (_length != other._length) return false;
			// All elements in this set must be in the other.
			return (*this) <= other;
		}

		/*! \brief Checks if this set contains at least one element that the other set does not. */
		inline bool operator!=(const HashSet& other) const
		{
			return !((*this) == other);
		}

		/*! \brief Checks if the lhs set is a subset of or equal to the rhs set. */
		friend bool operator<=(const HashSet& Lhs, const HashSet& Rhs)
		{
			for (const auto& Elem : Lhs)
			{
				if (!Rhs.has(Elem)) return false;
			}
			return true;
		}

		/*! \brief Checks if the lhs set is a superset of or equal to the rhs set. */
		inline friend bool operator>=(const HashSet& Lhs, const HashSet& Rhs)
		{
			return Rhs <= Lhs;
		}

		/*! \brief Checks if the lhs set is truly a subset of the rhs set. */
		inline friend bool operator<(const HashSet& Lhs, const HashSet& Rhs)
		{
			return Lhs <= Rhs && Lhs._length() != Rhs._length();
		}

		/*! \brief Checks if the lhs set is truly a superset of the rhs set. */
		inline friend bool operator>(const HashSet& Lhs, const HashSet& Rhs)
		{
			return Lhs >= Rhs && Lhs._length() != Rhs._length();
		}

		/*! \brief Shrinks this set's required memory to its exact number of elements.
			\warning This set implementation performs worse with shrunk sets. Preferrably use shrunk
					 sets only when iterating over a collection of elements.
			\warning Non-rehashed shrunk sets are designed for compact storage and iteration only!
					 The find() method and dependants (virtually all operator methods) may break due to
					 invalid bucket size.
			\details Shrinking with rehashing disabled should perform around O(n) while shrinking with
					 rehashing enabled should perform around O(n^2).
		*/
		void shrink(bool shouldRehash = true)
		{
			size_t prevSize = buffer.m_size;

			// Find the first valid element
			size_t firstValidIndex = buffer.m_size - 1;
			while (!buffer.buffer[firstValidIndex].valid) --firstValidIndex;

			// We're only interested in the elements up until and including the first valid one from the back
			buffer.m_size = firstValidIndex + 1;

			// Iterate through the elements of this set, moving them from the back forward over "invalid" elements
			for (size_t currIndex = firstValidIndex; currIndex >= 0; --currIndex)
			{
				size_t lastValidIndex = currIndex;
				while (lastValidIndex >= 0 && buffer.buffer[lastValidIndex].valid) --lastValidIndex;

				// Update the element we are currently "on"
				currIndex = lastValidIndex;

				// Only really need to move elements if the first invalid element isn't out of bounds
				if (lastValidIndex >= 0)
				{
					// Find the next valid element beyond the first invalid element
					size_t lastInvalidIndex = lastValidIndex;
					while (lastInvalidIndex >= 0 && !buffer.buffer[lastInvalidIndex].valid) --lastInvalidIndex;

					// The index at the element we got is actually valid. Thus go back one. Also ensures we stay in bounds.
					++lastInvalidIndex;

					// For convenience, since this is the actual index of the last valid value.
					++lastValidIndex;

					// Move the elements up to the last invalid element, automatically invalidating them.
					// No need to previously destruct the target elements since they have never been constructed to begin with.
					strat.move(buffer.buffer + lastInvalidIndex, buffer.buffer + lastValidIndex, buffer.m_size - lastValidIndex);

					// Decrease the tracked size by the number of elements the moved elements were moved by.
					buffer.m_size -= lastValidIndex - lastInvalidIndex;

					// Set the next element to process to the previously determined "next valid" element.
					currIndex = lastInvalidIndex - 1;
				}
			}

			// Physically shrink the used memory
			if (buffer.m_size != prevSize) {
				buffer.buffer = strat.reallocate(buffer.buffer, buffer.m_size);
			}

			// Rehash?
			if (shouldRehash) {
				rehash(true, false);
			}
		}

		/*! \brief Test if the given elements can fit into the set or adjust the set if necessary. */
		template<typename ITER>
		void reserve(const ITER& begin, const ITER& end)
		{
			size_t predictedSize = buffer.m_size, predictedBucketSize = bucketSize;
			if (!pretestRehash(predictedSize, predictedBucketSize, true, begin, end))
				throw InvalidStateException("Failed to reserve space for the passed range of elements");

			if (predictedSize != buffer.m_size) resize(predictedSize, false, true, false);
			bucketSize = predictedBucketSize;
		}

		/*! \brief Resize this set to fit at least newSize elements.
			\details If strict is set to true, the size will not be adjusted to either the next largest
					 multiple of the set's current increments or to fit the elements for rehashing.
			\warning Resizing the set is considered an expensive operation since it requires the
					 entire set to be rehashed which in turn might lead to additional resizing in order
					 to fit the elements into bucket sizes of approximately log_2(n).
			\warning Since the order of the elements is "undefined" resizing to a lower size may
					 or may not drop previously valid elements. Set shouldRecount to false if you're sure
					 the number of elements remains unchanged (e.g. internally done during shrink()).
		 */
		inline void resize(size_t newSize, bool shouldRecount = true, bool shouldRehash = true, bool strict = false)
		{
			resizeImpl(strict ? newSize : Math::ceil((double)newSize / bufferIncrements) * bufferIncrements, shouldRecount, shouldRehash, strict);
		}

		/*! \brief Gets the physical size of this set, i.e. the number of elements this set can currently theoretically hold. */
		inline size_t size() const
		{
			return buffer.m_size;
		}

		/*! \brief Actually counts the number of valid elements in this set and caches it.
			\details If you know the current cached length is correct, call length() instead.
			\see length()
		 */
		size_t count()
		{
			_length = 0;
			for (size_t currIndex = 0; currIndex < buffer.m_size; ++currIndex)
			{
				if (buffer.buffer[currIndex].valid) ++_length;
			}
			return _length;
		}

		/*! \brief Returns the cached number of elements in this set.
			\details The cached number of elements in this set might be out of sync if and only if you
					 manually invalidated an element. All other methods in this implementation should
					 automatically adjust the cached length or recount where necessary.
		*/
		inline size_t length() const
		{
			return _length;
		}

		/*! \brief Gets the current bucket size of this set. */
		inline size_t getBucketSize() const
		{
			return bucketSize;
		}

		/*! \brief Gets the current maximum bucket size of this set. */
		inline size_t getMaxBucketSize() const
		{
			return maxBucketSize;
		}

		/*! \brief Sets the maximum bucket size of this set.
			\details When decreasing the max bucket size below the current bucket size, shouldRehash can be
					 specified to prevent rehashing should you call rehash() manually later.
		 */
		inline void setMaxBucketSize(size_t newMaxBucketSize, bool shouldRehash = true)
		{
			maxBucketSize = newMaxBucketSize;
			if (shouldRehash && newMaxBucketSize < bucketSize)
			{
				rehash(true);
			}
		}

	public: // Iterators
		inline Iterator begin() { return Iterator(this); }
		inline ConstIterator begin() const { return cbegin(); }
		inline ConstIterator cbegin() const { return ConstIterator(this); }
		inline RIterator rbegin() { return RIterator(this); }
		inline ConstRIterator rbegin() const { return crbegin(); }
		inline ConstRIterator crbegin() const { return ConstRIterator(this); }

		inline Iterator end() { return Iterator(this, true); }
		inline ConstIterator end() const { return cend(); }
		inline ConstIterator cend() const { return ConstIterator(this, true); }
		inline RIterator rend() { return RIterator(this, true); }
		inline ConstRIterator rend() const { return crend(); }
		inline ConstRIterator crend() const { return ConstRIterator(this, true); }
	};

}

#endif
