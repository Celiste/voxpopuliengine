#ifndef VPE_HASHMAP_HPP
#define VPE_HASHMAP_HPP

#include "HashSet.hpp"

namespace vpe
{
	/*! \brief An element of the HashMap.
		\details In a hash map element, the value is essentially ignored. Only the key is relevant for hashing and storing.
				 Because the value is ignored for that purpose, the value type should provide a default constructor.
	 */
	template<typename KEY, typename VALUE>
	struct HashMapElement
	{
		KEY key;
		VALUE value;

		HashMapElement() = default;
		HashMapElement(const KEY& key) : key(key) {}
		HashMapElement(const KEY& key, const VALUE& value) : key(key), value(value) {}
		HashMapElement(const HashMapElement&) = default;
		HashMapElement(HashMapElement&&) = default;
		HashMapElement& operator=(const HashMapElement&) = default;
		HashMapElement& operator=(HashMapElement&&) = default;
		~HashMapElement() = default;

		bool operator==(const HashMapElement& other) const { return key == other.key; }
		bool operator!=(const HashMapElement& other) const { return !(*this == other); }

		operator VALUE() { return value; }
		operator const VALUE() const { return value; }

		friend hash_t calculateHash(const HashMapElement& elem) { return calculateHash(elem.key); }
	};

	/*! \brief A simple hash map based on the HashSet class.
		\details It stores a key/value pair using the key of the pair. The value is, as such, irrelevant in regards of hashing
				 and storing. The key of a particular value cannot be found by default; a specialized implementation is required.
				 Both the key and the value must be copyable in this naive implementation.
		\todo Introduce optional threadsafety!
	 */
	template<typename KEY, typename VALUE, typename MAP_ELEM = HashMapElement<KEY, VALUE>, typename SET_ELEM = SetElement<MAP_ELEM>, typename ELEM_STRAT = TypeStrategy<SET_ELEM>>
	class HashMap
	{
	private:
		HashSet<MAP_ELEM, SET_ELEM, ELEM_STRAT> internalSet;

	public:
		HashMap() = default;
		HashMap(const HashMap&) = default;
		HashMap(HashMap&&) = default;
		HashMap& operator=(const HashMap&) = default;
		HashMap& operator=(HashMap&&) = default;
		virtual ~HashMap() = default;

	public:
		virtual MAP_ELEM& add(const KEY& key, const VALUE& value)
		{
			MAP_ELEM elem = constructElem(key, value);
			auto* entry = internalSet.find(elem);
			if (entry)
			{
				entry->value.value = value;
				return entry->value;
			}
			return internalSet.add(std::move(constructElem(key, value)))->value;
		}

		virtual MAP_ELEM& add(KEY&& key, VALUE&& value)
		{
		    auto* entry = internalSet.find(constructElem(key));
			if (entry)
			{
				entry->value.value = std::move(value); // entry is a SetElement whose value is a HashMapElement whose value in turn is a VALUE
				return entry->value;
			}
			return internalSet.add(constructElem(std::move(key), std::move(value)))->value;
		}

		virtual MAP_ELEM& add(const KEY& key)
		{
            return add(std::move(KEY(key)));
		}

		virtual MAP_ELEM& add(KEY&& key)
		{
            MAP_ELEM elem = constructElem(std::move(key), VALUE());
            auto* entry = internalSet.find(elem);
            if (entry)
            {
                entry->value.value = VALUE();
                return entry->value;
            }
            return internalSet.add(std::move(elem))->value;
		}

		virtual void remove(const KEY& key)
		{
			if (has(key))
			{
				internalSet.remove(find(key));
			}
		}

		virtual bool has(const KEY& key) const
		{
			return internalSet.find(constructElem(key));
		}

		virtual VALUE& get(const KEY& key)
		{
			return find(key).value;
		}

		virtual const VALUE& get(const KEY& key) const
		{
			return find(key).value;
		}

		virtual MAP_ELEM& find(const KEY& key)
		{
			return internalSet.find(constructElem(key))->value;
		}

		virtual const MAP_ELEM& find(const KEY& key) const
		{
			return internalSet.find(constructElem(key))->value;
		}

		virtual size_t size() const
		{
			return internalSet.size();
		}

		virtual size_t length() const
		{
			return internalSet.length();
		}

		virtual void reprocess(const MAP_ELEM& elem)
		{
			internalSet.reprocess(elem);
		}

	public:
		virtual VALUE& operator[](const KEY& key)
		{
			return get(key);
		}

		virtual const VALUE& operator[](const KEY& key) const
		{
			return get(key);
		}

	protected:
		MAP_ELEM constructElem(const KEY& key) const { return MAP_ELEM(key); }
		MAP_ELEM constructElem(KEY&& key) const { return MAP_ELEM(std::move(key)); }
		MAP_ELEM constructElem(const KEY& key, const VALUE& value) const { return MAP_ELEM(key, value); }
		MAP_ELEM constructElem(KEY&& key, VALUE&& value) const { return MAP_ELEM(std::move(key), std::move(value)); }
	};
}

#endif
