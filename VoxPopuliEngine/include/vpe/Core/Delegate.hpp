#ifndef VPE_DELEGATE_HPP
#define VPE_DELEGATE_HPP

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <string>
#include <type_traits>
#include <typeinfo>
#include <utility>
#include "Simpletons.hpp"
#include "TypeTraits.hpp"

// I don't know who the fuck defines min and max in all lower case. It's not part of the standard, so it must be coming from SFML. It fucking breaks std::min and std::max, you dickbag!
#undef min
#undef max

namespace vpe {
	template<typename RET, typename... ARGS> class DelegateBase;
	template<typename RET, typename... ARGS> class Delegate;
	template<typename RET, typename... ARGS> class SinglecastDelegate;
	template<typename RET, typename... ARGS> class MulticastDelegate;
	template<typename INVOKER, typename EVENT, typename... ARGS> class EventEmitter;

	/*! \class DelegateBase
		\brief Base class for all delegates, singlecast and multicast alike.
	 */
	template<typename RET, typename... ARGS>
	class DelegateBase<RET(ARGS...)>
	{
		friend class SinglecastDelegate<RET(ARGS...)>;
		friend class MulticastDelegate<RET(ARGS...)>;

	private:
		DelegateBase() = default;
		DelegateBase(const DelegateBase&) = default;
		DelegateBase(DelegateBase&&) = default;
		DelegateBase& operator=(const DelegateBase&) = default;
		DelegateBase& operator=(DelegateBase&&) = default;
		virtual ~DelegateBase() = default;

	public:
		virtual RET invoke(ARGS... args) const = 0;

		inline RET operator()(ARGS... args) const { return invoke(args...); }

		virtual operator bool() const = 0;

		virtual bool operator==(const DelegateBase<RET(ARGS...)>&) const = 0;
		inline virtual bool operator!=(const DelegateBase<RET(ARGS...)>& other) const { return !(*this == other); }
	};

	/*! \brief Base class for singlecast delegates.
        \details Designed for use with our 6 basic singlecast delegate types. While it can be used as base
                 for other types of delegates as well, the MulticastDelegate<RET(ARGS...)> makes use of this
                 class' prototype pattern (see placementClone()) to fit all delegates into the a uniformly
                 sized memory region equal to the size of this base class. In other words, implementing a
                 custom derivation with more data breaks compatibility with the MulticastDelegate class, and
                 thus would require a custom implementation of the MulticastDelegate class as well.
     */
	template<typename RET, typename... ARGS>
	class SinglecastDelegate<RET(ARGS...)> : public DelegateBase<RET(ARGS...)>
	{
	    // Must have access to placementClone method
	    friend class MulticastDelegate<RET(ARGS...)>;

    protected:
        void* data;

    public:
        SinglecastDelegate() : data(nullptr) {}
        SinglecastDelegate(std::nullptr_t) : data(nullptr) {}
        SinglecastDelegate(void* data) : data(data) {}
        SinglecastDelegate(const SinglecastDelegate&) = default;
        SinglecastDelegate(SinglecastDelegate&&) = default;
        virtual SinglecastDelegate& operator=(const SinglecastDelegate&) = default;
        virtual SinglecastDelegate& operator=(SinglecastDelegate&&) = default;
        ~SinglecastDelegate() = default;

    protected:
		virtual void placementClone(SinglecastDelegate<RET(ARGS...)>* addr) const = 0;
    };

	/*! \brief Wrapper class for the 6 Singlecast Delegate classes.
	 */
	template<typename RET, typename... ARGS>
	class Delegate<RET(ARGS...)> : StaticClass
	{
	public:
		/*! \class FunctionDelegate
		    \brief Wraps around a single global function or static member method.
			\details Wrapping around a static method works almost the same, using `FunctionDelegate<MyClass::MyStatic>()`.
		 */
		template<RET(*FN)(ARGS...)>
		class FunctionDelegate final : public SinglecastDelegate<RET(ARGS...)>
		{
		public:
			RET invoke(ARGS... args) const override
			{
				return FN(args...);
			}

			inline operator bool() const override
			{
				return true;
			}

			inline bool operator==(const DelegateBase<RET(ARGS...)>& other) const override
			{
				return typeid(*this) == typeid(other);
			}

		protected:
			void placementClone(SinglecastDelegate<RET(ARGS...)>* addr) const override
			{
				new (addr) FunctionDelegate<FN>();
			}
		};

		/*! \class MethodDelegateBase
		    \brief Base class for the 4 differently qualified methods of the same signature.
			\details These classes must be used to avoid ambituity between two methods `my_method(...)` and
					 `my_method(...) const` which cannot be deduced from the template parameter alone.
		 */
		template<bool C, bool V, class T, typename toggle_method_cv<C, V, T, RET, ARGS...>::type M>
		class MethodDelegateBase final : public SinglecastDelegate<RET(ARGS...)>
		{
		    typedef SinglecastDelegate<RET(ARGS...)> Base;
			typedef typename toggle_cv<C, V, T>::type DataType;

		public:
			MethodDelegateBase() = default;
			MethodDelegateBase(std::nullptr_t) : Base(nullptr) {}
			MethodDelegateBase(DataType* inst) : Base((void*)inst) {}
			MethodDelegateBase(DataType& inst) : Base((void*)&inst) {}
			MethodDelegateBase(const MethodDelegateBase&) = default;
			MethodDelegateBase(MethodDelegateBase&&) = default;
			MethodDelegateBase& operator=(const MethodDelegateBase&) = default;
			MethodDelegateBase& operator=(MethodDelegateBase&&) = default;
			~MethodDelegateBase() = default;

		public:
			inline RET invoke(ARGS... args) const override
			{
				return (static_cast<DataType*>(Base::data)->*M)(args...);
			}

			inline operator bool() const override
			{
				return Base::data && M;
			}

			inline bool operator==(const DelegateBase<RET(ARGS...)>& other) const override
			{
				return typeid(*this) == typeid(other) && Base::data == ((MethodDelegateBase<C, V, T, M>&)other).data;
			}

		protected:
			inline void placementClone(SinglecastDelegate<RET(ARGS...)>* addr) const override
			{
				new (addr) MethodDelegateBase(*this);
			}

		public:
			static MethodDelegateBase<C, V, T, M> create(T& inst) { return MethodDelegateBase(&inst); }
			static MethodDelegateBase<C, V, T, M> create(T* inst) { return MethodDelegateBase(inst); }

			template<bool C1 = C, typename std::enable_if<C1, bool>::type = 0>
			static MethodDelegateBase<C, V, T, M> create(const T& inst) { return MethodDelegateBase(&inst); }
			template<bool C1 = C, typename std::enable_if<C1, bool>::type = 0>
			static MethodDelegateBase<C, V, T, M> create(const T* inst) { return MethodDelegateBase(inst); }

			template<bool V1 = V, typename std::enable_if<V1, bool>::type = 0>
			static MethodDelegateBase<C, V, T, M> create(volatile T& inst) { return MethodDelegateBase(&inst); }
			template<bool V1 = V, typename std::enable_if<V1, bool>::type = 0>
			static MethodDelegateBase<C, V, T, M> create(volatile T* inst) { return MethodDelegateBase(inst); }

			template<bool C1 = C, bool V1 = V, typename std::enable_if<C1 && V1, bool>::type = 0>
			static MethodDelegateBase<C, V, T, M> create(const volatile T& inst) { return MethodDelegateBase(&inst); }
			template<bool C1 = C, bool V1 = V, typename std::enable_if<C1 && V1, bool>::type = 0>
			static MethodDelegateBase<C, V, T, M> create(const volatile T* inst) { return MethodDelegateBase(inst); }
		};

		/*! \class MethodDelegate
		    \brief Wraps around a non-const and non-volatile qualified member method and stores the corresponding instance.
		 */
		template<class T, RET(T::*M)(ARGS...)>
		using MethodDelegate = MethodDelegateBase<false, false, T, M>;

		/*! \class MethodDelegate
		    \brief Wraps around a const but non-volatile qualified member method and stores the corresponding instance.
		 */
		template<class T, RET(T::*M)(ARGS...) const>
		using ConstMethodDelegate = MethodDelegateBase<true, false, T, M>;

		/*! \class MethodDelegate
		    \brief Wraps around a non-const but volatile qualified member method and stores the corresponding instance.
		 */
		template<class T, RET(T::*M)(ARGS...) volatile>
		using VolatileMethodDelegate = MethodDelegateBase<false, true, T, M>;

		/*! \class MethodDelegate
		    \brief Wraps around a cv (const and volatile) qualified member method and stores the corresponding instance.
		 */
		template<class T, RET(T::*M)(ARGS...) const volatile>
		using CVMethodDelegate = MethodDelegateBase<true, true, T, M>;

		/*! \class LambdaDelegate
		    \brief Wraps around a lambda with the specified signature. Use fromLambda() to create an instance.
		 */
		template<typename LAMBDA>
		class LambdaDelegate final : public SinglecastDelegate<RET(ARGS...)>
		{
		    typedef SinglecastDelegate<RET(ARGS...)> Base;

		public:
			LambdaDelegate() : Delegate() {}
			LambdaDelegate(std::nullptr_t) : Base(nullptr) {}
			LambdaDelegate(const LAMBDA* lambda) : Base((void*)lambda) {}
			LambdaDelegate(const LambdaDelegate&) = default;
			LambdaDelegate(LambdaDelegate&&) = default;
			LambdaDelegate& operator=(const LambdaDelegate&) = default;
			LambdaDelegate& operator=(LambdaDelegate&&) = default;
			~LambdaDelegate() = default;

		public:
			inline RET invoke(ARGS... args) const override
			{
				return (*(static_cast<const LAMBDA*>(Base::data)))(args...);
			}

			inline operator bool() const override
			{
				return Base::data;
			}

			inline bool operator==(const DelegateBase<RET(ARGS...)>& other) const override
			{
				return typeid(*this) == typeid(other) && Base::data != ((LambdaDelegate<LAMBDA>&)other).data;
			}

		protected:
			void placementClone(SinglecastDelegate<RET(ARGS...)>* addr) const override
			{
				new (addr) LambdaDelegate<LAMBDA>(*this);
			}
		};

	public:
		/*! \brief Creates a new LambdaDelegate from the passed lambda. */
		template<typename LAMBDA>
		static LambdaDelegate<LAMBDA> fromLambda(const LAMBDA& lambda) { return LambdaDelegate<LAMBDA>(&lambda); }
	};


	/*! \class MulticastDelegate
	    \brief A collection of delegates to invoke in sequence.
		\todo  Lacks an operator+=(const MulticastDelegate<RET(ARGS...)>&) to add all delegates (regular and one-time) to this multicast.
	 */
	template<typename RET, typename... ARGS>
	class MulticastDelegate<RET(ARGS...)> final : public DelegateBase<RET(ARGS...)>
	{
	private:
		using size_t = std::size_t;
		using deleg_t = SinglecastDelegate<RET(ARGS...)>;

		/*! \brief Buffer for regular delegates. */
		deleg_t* bufReg = nullptr;
		/*! \brief Buffer for one-time delegates.
		    \details Mutable in order to allow invoke to clear this buffer.
		 */
		mutable deleg_t* bufOnce = nullptr;

		/*! \brief Number of stored regular delegates. */
		size_t countReg = 0;
		/*! \brief Number of stored one-time delegates.
		    \details Mutable in order to allow invoke to clear this buffer.
		 */
		mutable size_t countOnce = 0;

		/*! \brief Maximum number of regular delegates. */
		size_t sizeReg = 0;
		/*! \brief Maximum number of one-time delegates.
		    \details Mutable in order to allow invoke to clear this buffer.
		 */
		mutable size_t sizeOnce = 0;

		/*! \brief Number of elements to add to max when automatically increasing buffer size. */
		size_t increment = 10;

	public:
		MulticastDelegate()
		{
			bufReg = (deleg_t*)malloc(increment * sizeof(deleg_t));
			bufOnce = (deleg_t*)malloc(increment * sizeof(deleg_t));
			sizeReg = increment;
			sizeOnce = increment;
		}
		MulticastDelegate(const MulticastDelegate& copy) : DelegateBase<RET(ARGS...)>(copy), countReg(copy.countReg), countOnce(copy.countOnce), sizeReg(copy.sizeReg), sizeOnce(copy.sizeOnce), increment(copy.increment)
		{
			bufReg = (deleg_t*)malloc(sizeReg * sizeof(deleg_t));
			bufOnce = (deleg_t*)malloc(sizeOnce * sizeof(deleg_t));

			for (size_t i = 0; i < countReg; ++i)
			{
				(copy.bufReg + i)->placementClone(bufReg + i);
			}
			for (size_t i = 0; i < countOnce; ++i)
			{
				(copy.bufOnce + i)->placementClone(bufOnce + i);
			}
		}
		MulticastDelegate(MulticastDelegate&& move) : DelegateBase<RET(ARGS...)>(move), bufReg(move.bufReg), bufOnce(move.bufOnce), countReg(move.countReg), countOnce(move.countOnce), sizeReg(move.sizeReg), sizeOnce(move.sizeOnce), increment(move.increment)
		{
			move.bufReg = nullptr;
			move.bufOnce = nullptr;
			move.countReg = 0;
			move.countOnce = 0;
			move.sizeReg = 0;
			move.sizeOnce = 0;
		}
		MulticastDelegate& operator=(const MulticastDelegate& copy)
		{
			this->~MulticastDelegate();
			new (this) MulticastDelegate(copy);
			return *this;
		}
		MulticastDelegate& operator=(MulticastDelegate&& move)
		{
			this->~MulticastDelegate();
			new (this) MulticastDelegate(move);
			return *this;
		}
		~MulticastDelegate()
		{
			resize(bufReg, countReg, sizeReg, 0);
			resize(bufOnce, countOnce, sizeOnce, 0);
		}

	protected: // Buffer manipulation
		static void resize(deleg_t*& buffer, size_t& count, size_t& size, size_t targetSize)
		{
			// Destroy excess elements
			for (size_t i = targetSize; i < count; ++i)
			{
				(buffer + i)->~SinglecastDelegate();
			}

			// Completely free the memory, not just resizing
			if (targetSize == 0)
			{
				free(buffer);
				size = 0;
				count = 0;
				buffer = nullptr;
			}

			// Resize the memory
			else
			{
				deleg_t* old = buffer;
				buffer = (deleg_t*)std::malloc(targetSize * sizeof(deleg_t));
				size = targetSize;
				count = std::min(count, targetSize);

				for (size_t i = 0; i < count; ++i)
				{
					deleg_t* curr = old + i;
					curr->placementClone(buffer + i);
					curr->~SinglecastDelegate();
				}
			}
		}

		static void add(deleg_t*& buffer, size_t& count, size_t& size, size_t increment, const deleg_t& deleg)
		{
			if (count == size)
			{
				resize(buffer, count, size, size + increment);
			}

			deleg.placementClone(buffer + count++);
		}

		static void removeFirst(deleg_t* buffer, size_t& count, const deleg_t& deleg)
		{
			for (size_t i = 0; i < count; ++i)
			{
				deleg_t* curr = buffer + i;
				if (*curr == deleg)
				{
					curr->~SinglecastDelegate();
					for (size_t j = i + 1; j < count; ++j)
					{
						deleg_t* currj = buffer + j;
						currj->placementClone(buffer + (j - 1));
						currj->~SinglecastDelegate();
					}
					--count;
					break;
				}
			}
		}

		static void removeLast(deleg_t* buffer, size_t& count, const deleg_t& deleg)
		{
			for (size_t i = count - 1; i >= 0; --i)
			{
				deleg_t* curr = buffer + i;
				if (*curr == deleg)
				{
					curr->~SinglecastDelegate();
					for (size_t j = i + 1; j < count; ++j)
					{
						deleg_t* currj = buffer + j;
						currj->placementClone(buffer + (j - 1));
						currj->~SinglecastDelegate();
					}
					--count;
					break;
				}
			}
		}

		static void removeAll(deleg_t* buffer, size_t& count, const deleg_t& deleg)
		{
			size_t removed = 0;

			for (size_t i = 0; i < count; ++i)
			{
				deleg_t* curr = buffer + i;
				if (*curr == deleg)
				{
					++removed;
					curr->~SinglecastDelegate();
				}
				if (removed != 0)
				{
					curr->placementClone(buffer + (i - removed));
					curr->~SinglecastDelegate();
				}
			}

			count -= removed;
		}

		static void clear(deleg_t*& buffer, size_t& count, size_t& size, size_t targetSize)
		{
			for (size_t i = 0; i < count; ++i)
			{
				(buffer + i)->~SinglecastDelegate();
			}

			buffer = (deleg_t*)realloc(buffer, targetSize);
			size = targetSize;
			count = 0;
		}

	public: // Addition and removal interface
		/*! \brief Adds the specified delegate to this multicast.
		    \details If a delegate wrapping the same function has previously been added, the delegate will be added again.
			\see add()
			     once()
				 operator-=()
		 */
		inline MulticastDelegate& operator+=(const deleg_t& deleg)
		{
			return add(deleg);
		}

		/*! \brief Adds the specified delegate to this multicast.
		    \details If a delegate wrapping the same function has previously been added, the delegate will be added again.
			\see operator+=()
			     once()
				 removeFirst()
		 */
		inline MulticastDelegate& add(const deleg_t& deleg)
		{
			add(bufReg, countReg, sizeReg, increment, deleg);
			return *this;
		}

		/*! \brief Removes the first occurrence of the specified delegate from this multicast.
		    \see removeFirst()
			     removeLast()
				 removeAll()
				 operator+=()
		 */
		inline MulticastDelegate& operator-=(const deleg_t& deleg)
		{
			return removeFirst(deleg);
		}

		/*! \brief Removes the first occurrence of the specified delegate from this multicast.
		    \see operator-=()
			     removeLast()
				 removeAll()
				 add()
		 */
		inline MulticastDelegate& removeFirst(const deleg_t& deleg)
		{
			removeFirst(bufReg, countReg, deleg);
			return *this;
		}

		/*! \brief Removes the last occurrence of the specified delegate from this multicast.
		    \see operator-=()
			     removeFirst()
				 removeAll()
		 */
		inline MulticastDelegate& removeLast(const deleg_t& deleg)
		{
			removeLast(bufReg, countReg, deleg);
			return *this;
		}

		/*! \brief Removes all occurrences of the specified delegate from this multicast.
		    \see operator-=()
			     removeFirst()
			     removeLast()
		 */
		inline MulticastDelegate& removeAll(const deleg_t& deleg)
		{
			removeAll(bufReg, countReg, deleg);
			return *this;
		}

		/*! \brief Adds the specified delegate as a one-time handler.
		    \details After the next invocation of this multicast, all one-time handlers are removed. If a
			         previous delegate wrapping the same function has been added prior, it will be added
					 again.
			\see unonceFirst()
			     unonceLast()
				 unonceAll()
				 clearOnetime()
		 */
		inline MulticastDelegate& once(const deleg_t& deleg)
		{
			add(bufOnce, countOnce, sizeOnce, increment, deleg);
			return *this;
		}

		/*! \brief Removes the first occurrence of the specified delegate from the one-time handlers.
		    \see once()
			     unonceLast()
				 unonceAll()
				 clearOnetime()
		 */
		inline MulticastDelegate& unonceFirst(const deleg_t& deleg)
		{
			removeFirst(bufOnce, countOnce, deleg);
			return *this;
		}

		/*! \brief Removes the last occurrence of the specified delegate from the one-time handlers.
		    \see once()
			     unonceFirst()
				 unonceAll()
				 clearOnetime()
		 */
		inline MulticastDelegate& unonceLast(const deleg_t& deleg)
		{
			removeLast(bufOnce, countOnce, deleg);
			return *this;
		}

		/*! \brief Removes all occurrences of the specified delegate from the one-time handlers.
		    \see once()
			     unonceFirst()
				 unonceLast()
				 clearOnetime()
		 */
		inline MulticastDelegate& unonceAll(const deleg_t& deleg)
		{
			removeAll(bufOnce, countOnce, deleg);
			return *this;
		}

		/*! \brief Clears all delegates from this multicast, regular and one-time handlers alike.
		    \see clearRegular()
				 clearOnetime()
		 */
		inline MulticastDelegate& clear()
		{
			clearRegular();
			clearOnetime();
			return *this;
		}

		/*! \brief Clears all regular delegates from this multicast. One-time handlers are left untouched.
		    \see clear()
			     clearOnetime()
		 */
		inline MulticastDelegate& clearRegular()
		{
			clear(bufReg, countReg, sizeReg, increment);
			return *this;
		}

		/*! \brief Clears all one-time delegates from this multicast.
		    \see clear()
			     clearRegular()
		 */
		inline MulticastDelegate& clearOnetime()
		{
			clear(bufOnce, countOnce, sizeOnce, increment);
			return *this;
		}


	protected:
		template<typename RET1 = RET, typename std::enable_if<!std::is_same<RET1, void>::value, bool>::type = 0>
		RET do_invoke(ARGS... args) const
		{
			RET result;

			for (size_t i = 0; i < countReg; ++i)
			{
				result = (bufReg + i)->invoke(args...);
			}
			for (size_t i = 0; i < countOnce; ++i)
			{
				result = (bufOnce + i)->invoke(args...);
			}

			clear(bufOnce, countOnce, sizeOnce, increment);

			return result;
		}

		template<typename RET1 = RET, typename std::enable_if<std::is_same<RET1, void>::value, bool>::type = 0>
		void do_invoke(ARGS... args) const
		{
			for (size_t i = 0; i < countReg; ++i)
			{
				(bufReg + i)->invoke(args...);
			}
			for (size_t i = 0; i < countOnce; ++i)
			{
				(bufOnce + i)->invoke(args...);
			}

			clear(bufOnce, countOnce, sizeOnce, increment);
		}

	public:
		inline RET invoke(ARGS... args) const override
		{
			return do_invoke(args...);
		}

		/*! \brief Invokes the delegates in this multicast in sequence passing their individual return values to the specified lambda.
		    \details The return value of the lambda itself is ignored.
		 */
		template<typename LAMBDA, typename RET1 = RET, typename std::enable_if<!std::is_same<RET1, void>::value, bool>::type = 0>
		RET invoke(ARGS... args, const LAMBDA& handler) const
		{
			RET result;

			for (size_t i = 0; i < countReg; ++i)
			{
				result = (bufReg + i)->invoke(args...);
				handler(result);
			}
			for (size_t i = 0; i < countOnce; ++i)
			{
				result = (bufOnce + i)->invoke(args...);
				handler(result);
			}

			return result;
		}

		/*! \brief Invokes the delegates in this multicast in sequence passing their individual return values to the specified handler delegate.
		    \details The return value of the handler delegate itself is ignored.
		 */
		template<typename HANDLER_RET = void, typename RET1 = RET>
		decltype(typename std::enable_if<!std::is_same_v<RET1, void>, RET1>::type()) invoke(ARGS... args, const DelegateBase<HANDLER_RET()>& deleg) const
		{
			RET result;

			for (size_t i = 0; i < countReg; ++i)
			{
				result = (bufReg + i)->invoke(args...);
				deleg(result);
			}
			for (size_t i = 0; i < countOnce; ++i)
			{
				result = (bufOnce + i)->invoke(args...);
				deleg(result);
			}

			return result;
		}

	public:
		inline operator bool() const override { return size(); }

		bool operator==(const DelegateBase<RET(ARGS...)>& other) const override
		{
			// Two multicast delegates only equal if they contain the exact same delegates.
			if (typeid(other) == typeid(*this))
			{
				const auto& otherMulti = (const MulticastDelegate<RET(ARGS...)>&)other;

				if (otherMulti.countReg != countReg) return false;
				if (otherMulti.countOnce != countOnce) return false;

				return *this > other;
			}

			return *this > other;
		}

		/*! \brief Tests if the other delegate is contained within or a subset of the multicast delegate.
		    \details If the other delegate is a multicast as well then all of its delegates must have an equal
			         in the left hand side.
			\see operator>()
		 */
		friend bool operator>(const MulticastDelegate<RET(ARGS...)>& multi, const DelegateBase<RET(ARGS...)>& other)
		{
			if (typeid(other) == typeid(multi))
			{
				const auto& otherMulti = (const MulticastDelegate<RET(ARGS...)>&)other;

				for (size_t i = 0; i < otherMulti.countReg; ++i)
				{
					if (!(multi > *(otherMulti.bufReg + i))) return false;
				}
				for (size_t i = 0; i < otherMulti.countOnce; ++i)
				{
					if (!(multi > *(otherMulti.bufOnce + i))) return false;
				}
				return true;
			}

			for (size_t i = 0; i < multi.countReg; ++i)
			{
				if (*(multi.bufReg + i) == other) return true;
			}
			for (size_t i = 0; i < multi.countOnce; ++i)
			{
				if (*(multi.bufOnce + i) == other) return true;
			}
			return false;
		}

		/*! \brief Tests if the other delegate is contained within or a subset of the multicast delegate.
		    \details If the other delegate is a multicast as well then all of its delegates must have an equal
			         in the right hand side.
			\see operator>()
		 */
		inline friend bool operator<(const DelegateBase<RET(ARGS...)>& deleg, const MulticastDelegate<RET(ARGS...)>& multi)
		{
			return multi > deleg;
		}

		/*! /brief Counts the number of regular and one-time delegates in this multicast. */
		size_t size() const { return countReg + countOnce; }
	};


	/*! \class Event
	    \brief The base class for event status objects passed to event handlers.
		\see   CancellableEvent
	 */
	class Event
	{
	protected:
		const std::string name;

	public:
		Event() = default;
		Event(std::string name) : name(name) {}
		Event(const Event&) = default;
		Event(Event&&) = default;
		Event& operator=(const Event&) = default;
		Event& operator=(Event&&) = default;
		virtual ~Event() = default;

	public:
		/*! \brief Gets the name of this event. */
		inline std::string getName() const { return name; }
	};

	/*! \class CancellableEvent
	    \brief The base class for all cancellable events.
		\see   Event
	 */
	class CancellableEvent : public Event
	{
	protected:
		bool cancelled = false;

	public:
		CancellableEvent() = default;
		CancellableEvent(std::string name) : Event(name) {}
		CancellableEvent(const CancellableEvent&) = default;
		CancellableEvent(CancellableEvent&&) = default;
		CancellableEvent& operator=(const CancellableEvent&) = default;
		CancellableEvent& operator=(CancellableEvent&&) = default;
		~CancellableEvent() = default;

	public:
		/*! \brief Sets whether this event should be cancelled or not. */
		inline void cancel(bool value) { cancelled = value; }
	};

	/*! \class EventEmitter
	    \brief A simple event emitter associated with an instance of the specified INVOKER class.
		\details The EVT type must be default, copy and move constructible as I am currently too lazy
				 to enforce type traits. Sorry!
	 */
	template<typename INVOKER, typename EVT = Event, typename... ARGS>
	class EventEmitter
	{
		friend INVOKER;

	protected:
		MulticastDelegate<void(EVT&, ARGS...)> handlers;

    public:
        virtual ~EventEmitter() = default;

	public:
		virtual inline EventEmitter& operator+=(const SinglecastDelegate<void(EVT&, ARGS...)>& handler)
		{
			handlers += handler;
			return *this;
		}

		virtual inline EventEmitter& operator-=(const SinglecastDelegate<void(EVT&, ARGS...)>& handler)
		{
			handlers -= handler;
			return *this;
		}

		virtual inline EventEmitter& removeAll(const SinglecastDelegate<void(EVT&, ARGS...)>& handler)
		{
			handlers.removeAll(handler);
			return *this;
		}

		virtual inline EventEmitter& once(const SinglecastDelegate<void(EVT&, ARGS...)>& handler)
		{
			handlers.once(handler);
			return *this;
		}

		virtual inline EventEmitter& unonce(const SinglecastDelegate<void(EVT&, ARGS...)>& handler)
		{
			handlers.unonceFirst(handler);
			return *this;
		}

		virtual inline EventEmitter& unonceLast(const SinglecastDelegate<void(EVT&, ARGS...)>& handler)
		{
			handlers.unonceLast(handler);
			return *this;
		}

	protected:
		/*! \brief Alias for invoke(ARGS... args) */
		inline EVT operator()(ARGS... args) const { return invoke(args...); }

		/*! \brief Alias for invoke(const EVT& evt, ARGS... args) */
		inline EVT operator()(const EVT& evt, ARGS... args) const { return invoke(evt, args...); }

		/*! \brief Alias for invoke(EVT& evt, ARGS... args) */
		inline virtual void operator()(EVT& evt, ARGS... args) const { invoke(evt, args...); }

		/*! \brief Invokes the event handlers with a default constructed instance of the event object class.
		    \details Quite obviously the event object class must be default constructable.
		 */
		virtual EVT invoke(ARGS... args) const
		{
			EVT evt;
			handlers.invoke(evt, args...);
			return evt;
		}

		/*! \brief Invokes the event handlers with a copy of the passed event object.
		    \details The event object class must be copy constructible.
		 */
		virtual EVT invoke(const EVT& evt, ARGS... args) const
		{
			EVT e(evt);
			handlers.invoke(e, args...);
			return e;
		}

		/*! \brief Invokes the event handlers with the passed event reference.
		    \details Other convenience invoke overloads are available if the event class is default
					 constructible and/or copy constructible.
		 */
		inline virtual void invoke(EVT& evt, ARGS... args) const
		{
			handlers.invoke(evt, args...);
		}
	};
}

#endif
