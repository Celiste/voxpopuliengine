#ifndef VPE_SCENERENDERER_HPP
#define VPE_SCENERENDERER_HPP

/*! \file
	\brief Declares the scene renderer base.
	\details The scene renderer is supposed to have the sole control over what is rendered on screen.
	\author Brian Cobile aka. Kiruse aka. Celiste
 */

#include "vpe.h"
#include "Components/CameraComponent.hpp"
#include "Core/Simpletons.hpp"
#include "Graphics/GLContext.hpp"

namespace vpe
{
	class World;

	/*! \brief The scene renderer base.
		\details Can be specialized for 2D or 3D scenes. Requires a Graphics::GLContext to work. If the context is
				 freed before the renderer, behavior is undefined.
				 Because a GLContext cannot be copied, a SceneRenderer cannot be copied either.
	 */
	class VPE_API SceneRenderer : CopyMoveSwitch<false, true>
	{
	protected:
		/*! \brief The context to which to render the scene's image. */
		Graphics::GLContext* ctx;

	public:
		SceneRenderer() = default;
		~SceneRenderer() = default;

	public:
		/*! \brief Sets the new graphics context to which to render. */
		inline void setGraphicsContext(Graphics::GLContext* target) { ctx = target; }

		/*! \brief Gets the current graphics context to which this scene renderer is rendering. */
		inline Graphics::GLContext* getGraphicsContext() const { return ctx; }

		/*! \brief Renders the scene for the specified camera component.
			\details The scene to render is the same scene of the camera.
			\note Naturally renders nothing if the camera is attached to a dangling entity (i.e. not part of any scene).
		 */
		//void render(const WeakPointer<Camera2DComponent>& camera);
	};
}

#endif
