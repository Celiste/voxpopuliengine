#ifndef VPE_GRAPHICS_PCH
#define VPE_GRAPHICS_PCH

/*! \file
	\author	 Brian Cobile aka. Kiruse aka. Celiste
	\brief   "Precompiled header" for the graphics subsystem. Not really regarded as PCH by the compiler tho.
	\details Microsoft is a real bitch. They haven't updated their OpenGL headers in quite a while, such that
			 most modern features are simply unavailable. We use GLEW to partially remedy this, as well as
			 detect which features the graphics adapter supports anyway. However, GLEW doesn't
			 #include <Windows.h> to avoid namespace pollution, but we don't particularly care about that. Yet.
	\todo	 Look into how precompiled headers work under Visual Studio, possibly Borland C, and GNU GCC.
 */

#ifdef _MSC_VER
# include <Windows.h>
#endif
#include <GL/glew.h>

#ifndef GL_VERSION_3_3
# error "OpenGL version 3.3 required"
#endif

#endif
