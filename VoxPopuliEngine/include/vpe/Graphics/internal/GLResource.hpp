#ifndef VPE_GLRESOURCE_HPP
#define VPE_GLRESOURCE_HPP

/*! \file
	\author  Brian Cobile aka. Kiruse aka. Celiste
	\brief	 Declares base GLResource and GLContainerResource classes.
	\details These classes are intended for engine-internal use only. They are supposed to abstract
			 away the OpenGL work such that users of the engine need not even link OpenGL.
 */

#include "vpe.h"
#include "graphics.h"
#include "Core/Simpletons.hpp"

namespace vpe {
	namespace Graphics {
		namespace internal
		{
			/*! \brief Base class for all OpenGL resources.
				\details
				\parblock
				Resources should not be copied. Copying would require the resource to be recreated in VRAM,
				and a second copy of the same buffer has little purpose. If you really need to create an
				exact copy of a resource, refer to the specific resource's implementation. Resources can,
				however, be moved.
				\endparblock
				\remark Since the class does not contain pure virtual methods, the default constructor is protected
						as to enforce the abstract class pattern.
			 */
			class GLResource
			{
			protected:
				/*! \brief The resource location as yielded from the respective OpenGL call. */
				GLuint location;

			protected:
				GLResource() = default;

			public:
				GLResource(const GLResource&) = delete;
				GLResource(GLResource&&) = default;
				GLResource& operator=(const GLResource&) = delete;
				virtual GLResource& operator=(GLResource&&) = default;
				virtual ~GLResource() = default;

			public:
				inline GLuint getLocation() const { return location; }
			};

			/*! \brief Base class for OpenGL container objects.
				\details
				\parblock
				Container Objects in OpenGL differ from regular / state objects in that they, adhering to
				their name, contain other data. These objects cannot be shared between OpenGL contexts.
				Refer to <a href="https://www.khronos.org/opengl/wiki/OpenGL_Object#Container_objects">this list</a>
				to review the current list of container objects in OpenGL.

				Unlike a regular GLResource, a GLContainerResource can only be shared between contexts by
				recreating their contents in the respective new context. Thus, upon switching into a different
				context, we do so automatically.
				\endparblock
				\remark Since the class does not contain pure virtual methods, the default constructor is protected
						as to enforce the abstract class pattern.
			 */
			class GLContainerResource : public GLResource
			{
			protected:
				GLContainerResource() = default;

			public:
				GLContainerResource(GLContainerResource&&) = default;
				virtual GLContainerResource& operator=(GLContainerResource&&) = default;
				~GLContainerResource() = default; // inherently virtual
			};
		}
	}
}

#endif
