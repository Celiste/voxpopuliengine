#ifndef VPE_GLBUFFER_HPP
#define VPE_GLBUFFER_HPP

/*! \file
	\author  Brian Cobile aka. Kiruse aka. Celiste
	\brief   Declares the various abstractions for buffer objects.
	\details Because SFML sets up OpenGL contexts for sharing, all regular OpenGL resources including buffers
			 can be shared without additional requirements. However, the graphics manager might decide that a
			 buffer, due to recent inactivity, should be freed in favor of other more pressing needs. Accordingly
			 the buffer objects always store a pointer to a copy of the data internally. The classes are designed
			 in a fire-and-forget philosophy. However, for the sake of efficiency, it is possible to pass a
			 pointer to the data directly. The GLBuffer::upload method will then directly access that data without
			 creating a copy first.
 */

#include <cstdint>
#include <cstring>
#include "GLResource.hpp"
#include "Exception.hpp"

namespace vpe {
	namespace Graphics {
		namespace internal
		{
			/*! \brief The base class for the various buffer objects of OpenGL. */
			class GLBuffer : public GLResource
			{
			protected:
				/*! \brief Enumeration of the various OpenGL buffer object targets.
					\details The symbol names correlate directly to their OpenGL counterparts following the syntax of GL_<enum_name>_BUFFER.
				 */
				enum Target : uint8_t {
					/*! \brief A regular array buffer used usually for vertex buffers. */
					Array = 0,

					/*! \brief Used together with CopyWrite and glCopyBufferSubData to avoid disturbing the state of other bindings. */
					CopyRead,

					/*! \brief Used together with CopyRead and glCopyBufferSubData to avoid disturbing the state of other bindings. */
					CopyWrite,

					/*! \brief For use with glDrawArraysIndirect and glDrawElementsIndirect.
						\details glDrawArraysIndirect is very similar to glDrawArraysInstancedBaseInstance except that it uses a server-side
								 buffer object to store the data rather than data from client memory. (Repsectively for glDrawElementsIndirect.)
					 */
					DrawIndirect,

					/*! \brief For use with glDrawElements and its variants.
						\details An element array buffer defines the order in which elements from array buffers are rendered. Consider it
								 the index buffer.
								 The pointer argument of these functions is treated as an offset into the bound buffer object.
					 */
					ElementArray,

					/*! \brief For use with glGetTexture, glGetCompressedTexture and glReadPixel.
						\details The pointer argument of these three functions is treated as an offset into the bound buffer object.
					 */
					PixelPack,

					/*! \brief For use with glCompressedTexImage1D, glCompressedTexSubImage1D, glTexImage1D, glTexSubImage1D and their respective 2D and 3D counterparts.
						\details The pointer argument of these functions is then treated as an offset into the bound buffer object.
					 */
					PixelUnpack,

					/*! \note Must be passed to glBindBufferBase. */
					ShaderStorage,

					/*! \brief Stores texture data in a VBO, I guess? I suppose it's intended for read and copy operations. */
					Texture,

					/*! \brief Must be passed to glBindBufferBase. Intended for read and copy operations. */
					TransformFeedback,

					/*! \brief Must be passed to glBindBufferBase. Intended to store multiple uniforms' data. */
					Uniform,

#ifdef GL_VERSION_4_2
					/*! \brief Must be passed to glBindBufferBase. Intended for concurrent operation counting, for example.
						\note  Only available under OpenGL 4.2+
					 */
					AtomicCounter,
#endif

#ifdef GL_VERSION_4_3
					/*! \brief For use with glDispatchComputeIndirect.
						\note  Only available under OpenGL 4.3+
					 */
					DispatchIndirect,
#endif

#ifdef GL_VERSION_4_4
					/*! \brief For use with glGetQueryObject.
						\note  Only available under OpenGL 4.4+
					 */
					Query,
#endif
				};

			public:
				/*! \brief Abstracts the three basic write modes OpenGL supports.
					\details
					\parblock
					OpenGl optimizes where buffer data is stored based on usage hints. This enum as well as
					the AccessMode enum are used to provide OpenGL with this usage hint.

					OpenGL distinguishes between three distinct usage models related to data lifetime delimiters
					as well as user write access which we refer to as writetime modes: STATIC, STREAM and DYNAMIC,
					all described below.
					\endparblock
				 */
				enum WritetimeMode : uint8_t {
					/*! \brief The user will only upload data into this buffer once and use it often.
						\details Represents any GL_STATIC_* usage hint.
					 */
					Static = 0,

					/*! \brief The user will only upload data into this buffer once and use it a few times.
						\details Represents any GL_STREAM_* usage hint.
					 */
					Stream,

					/*! \brief The user will frequently upload data into this buffer and use it often.
						\details Represents any GL_DYNAMIC_* usage hint.
					 */
					Dynamic,
				};

				/*! \brief Abstracts the three basic read modes OpenGL supports.
					\details
					\parblock
					OpenGl optimizes where buffer data is stored based on usage hints. This enum as well as
					the WritetimeMode enum are used to provide OpenGL with this usage hint.

					OpenGL distinguishes between three distinct usage models related to user and server read
					access to the data which we refer to as access modes: DRAW, READ, and COPY, all described
					below.
					\endparblock
				 */
				enum AccessMode : uint8_t {
					/*! \brief The user will never read data from this buffer. Data is used for rendering.
						\details Represents any GL_*_DRAW usage hint.
					 */
					Draw = 0,

					/*! \brief The buffer will be the target for GPU operations and the user will read them.
						\details Represents any GL_*_READ usage hint.
					 */
					Read,

					/*! \brief The buffer will be the target for GPU operations but the user will not read them. Instead
							   they are used by other GPU operations.
						\details Represents any GL_*_COPY usage hint.
					 */
					Copy,
				};

				/*! \brief A utility class binding and unbinding its associated buffer as it's created and destroyed respectively. */
				class ScopeBinder final
				{
				private:
					GLBuffer& buffer;

				public:
					ScopeBinder(GLBuffer& buffer) : buffer(buffer) { buffer.bind(); }
					ScopeBinder(const ScopeBinder&) = delete;
					ScopeBinder(ScopeBinder&&) = delete;
					ScopeBinder& operator=(const ScopeBinder&) = delete;
					ScopeBinder& operator=(ScopeBinder&&) = delete;
					~ScopeBinder() { buffer.unbind(); }
				};

			private:
				/*! \details 0 - buffer destroyed
							 1 - data written
				 */
				uint8_t flags;
				void* data = nullptr;
				std::size_t elementSize = 0;
				std::size_t elementCount = 0;

				std::string name;
				Target target;
				WritetimeMode writetimeMode;
				AccessMode accessMode;
				GLenum cachedUsageHint;

			protected:
				GLBuffer(Target target, WritetimeMode writetimeMode, AccessMode accessMode) : GLBuffer("<unnamed>", target, writetimeMode, accessMode) {}
				GLBuffer(Target target, WritetimeMode writetimeMode, AccessMode accessMode, void* data, std::size_t sizeInBytes, std::size_t count) : GLBuffer("<unnamed>", target, writetimeMode, accessMode, data, sizeInBytes, count) {}
				GLBuffer(std::string name, Target target, WritetimeMode writetimeMode, AccessMode accessMode) : name(name), target(target), writetimeMode(writetimeMode), accessMode(accessMode), cachedUsageHint(determineUsageHint(writetimeMode, accessMode))
				{
					if (target < 0 || target > Query)   throw InvalidArgumentException("Invalid target");
					if (writetimeMode < 0 || writetimeMode > 2) throw InvalidArgumentException("Invalid write mode");
					if (accessMode < 0 || accessMode > 2)   throw InvalidArgumentException("Invalid read mode");
					flags = 0;
				}
				GLBuffer(std::string name, Target target, WritetimeMode writetimeMode, AccessMode accessMode, void* data, size_t sizeInBytes, size_t count) : GLBuffer(name, target, writetimeMode, accessMode)
				{
					assume(data, sizeInBytes, count);
				}

			public:
				virtual ~GLBuffer() {
					destroy();
					flags = 1;
				}

			public:
				virtual void bind() const;
				virtual void unbind() const;

			public:
				/*! \brief Initially creates the buffer on the GPU.
					\details This method will also be called after the buffer has been temporarily
							 deleted to free up unused GPU memory.
				 */
				virtual void create();

				/*! \brief Instructs the server to free the buffer's consumed memory. */
				virtual void destroy();

				/*! \brief Uploads previous data if not already uploaded. Should be invoked after destroy() and create() in sequence. */
				virtual void upload();

			protected:
				/*! \brief Uploads new data to the server where the buffer should be.
					\details Data can only be reuploaded if WritetimeMode is WritetimeMode::Dynamic, otherwise an exception is thrown.
				 */
				virtual void upload(void* data, size_t sizeInBytes, size_t count);

				/*! \brief Assumes the specified data as its own.
					\details The data will not be cached/copied, but used directly.
				 */
				virtual void assume(void* data, size_t sizeInBytes, size_t count);

			public:
				static void unbind(Target target);

			protected:
				static GLenum determineUsageHint(WritetimeMode writetimeMode, AccessMode accessMode)
				{
					GLenum base = 0;

					switch (writetimeMode)
					{
						// Invalid mode
					default: return 0;

					case Static:
						base = GL_STATIC_DRAW;
						break;

					case Stream:
						base = GL_STREAM_DRAW;
						break;

					case Dynamic:
						base = GL_DYNAMIC_DRAW;
						break;
					}


					switch (accessMode)
					{
						// Invalid mode
					default: return 0;

					case Draw:
					case Read:
					case Copy:
						return base + accessMode;
					}
				}

			public:
				inline static bool isIndexBased(Target target)
				{
					return target == GL_UNIFORM_BUFFER || target == GL_ATOMIC_COUNTER_BUFFER || target == GL_SHADER_STORAGE_BUFFER || target == GL_TRANSFORM_FEEDBACK_BUFFER;
				}
			};

			/*! \brief Strongly-typed GL buffer object storing array/vertex data.
				\details
				\parblock
				Vertex data consists not just of vertex positions but also color, normals, UV coordinates
				and possibly more. If any of these components changes the vertex in its entirety is different.

				Because it is a regular OpenGL object, it can be shared easily between contexts (windows).
				\endparblock
			 */
			template<typename E>
			class GLArrayBuffer : public GLBuffer
			{
			public:
				/*! \brief Uploads new data to the server where the buffer should be.
					\details Data can only be reuploaded if WritetimeMode is WritetimeMode::Dynamic, otherwise an exception is thrown.
				 */
				virtual void upload(E* data, size_t count) { GLBuffer::upload((void*)data, sizeof(data), count); }

				/*! \brief Assumes the specified data as its own.
					\details The data will not be cached/copied, but used directly.
				 */
				virtual void assume(E* data, size_t count) { GLBuffer::assume((void*)data, sizeof(E), count); }
			};
		}
	}
}

#endif
