#ifndef VPE_GLCONTEXT_HPP
#define VPE_GLCONTEXT_HPP

/*! \file
	\author Brian Cobile aka. Kiruse aka. Celiste
	\brief Declares the GLContext class wrapping around an SFML OpenGL context or a SFML window.
 */

#include "vpe.h"
#include "Core/Delegate.hpp"
#include "Core/Simpletons.hpp"
#include "Exception/GLException.hpp"
#include <atomic>
#include <SFML/Window.hpp>

namespace vpe
{
	class Engine;

	namespace Graphics
	{
		/*! \brief Wraps around a window or window-less context of SFML. */
		class VPE_API GLContext final : CopyMoveSwitch<false, false>
		{
		public:
			enum InnerType {
				Undefined = 0,
				ExternalWindow,
				ExternalContext,
				InternalWindow,
				InternalContext,
			};

		private:
			std::uint16_t id;
			bool active = false;
			InnerType ctxType;

			union {
				sf::Window* extWnd;
				sf::Context* extCtx;
				sf::Window intWnd;
				sf::Context intCtx;
			};

		public:
			GLContext();
			GLContext(sf::Window& wnd);
			GLContext(sf::Context& ctx);
			GLContext(sf::WindowHandle handle, const sf::ContextSettings& settings);
			GLContext(const sf::VideoMode& videoMode, const sf::String title, std::uint32_t style, const sf::ContextSettings& settings);
			GLContext(const sf::ContextSettings& settings, std::uint32_t width, std::uint32_t height);
			~GLContext();

		public:
			/*! \brief Gets the unique ID of this context.
				\details The ID is only unique for this session.
			 */
			inline std::uint16_t getID() const { return id; }

			/*! \brief Sets the associated SFML context as the current. */
			void activate();

			/*! \brief Unsets the associated SFML context as the current.
				\details This method must be called before moving the context to another thread as it is impossible
						 to automatically unbind an OpenGL context from another thread.
			 */
			void deactivate();

			/*! \brief Tests if this context represents a SFML window. */
			bool isWindow() const;

			/*! \brief Gets the associated SFML window.
				\exception InvalidStateException The context does not use an external or internal window.
			 */
			sf::Window& getWindow();

			/*! \brief Gets the associated SFML window.
				\exception InvalidStateException The context does not use an external or internal window.
			 */
			const sf::Window& getWindow() const;

			/*! \brief Gets the associated SFML graphics context.
				\exception InvalidStateException The context does not use an external or internal context.
			 */
			sf::Context& getContext();

			/*! \brief Gets the associated SFML graphics context.
				\exception InvalidStateException The context does not use an external or internal context.
			 */
			const sf::Context& getContext() const;

		protected:
			void do_activate();
			void do_deactivate();

		public:
			/*! \brief Gets the current context of this thread. */
			static GLContext* getCurrent();

		public:
			/*! \brief The context has been activated.*/
			EventEmitter<GLContext, Event, GLContext&> onActivate;
			EventEmitter<GLContext, Event, GLContext&> onDeactivate;
			EventEmitter<Engine, Event, GLContext&, const sf::Time&> onUpdate;
			EventEmitter<Engine, Event, GLContext&> onRender;
		};
	}
}

#endif
