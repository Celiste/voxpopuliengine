#include "Graphics/internal/GLBuffer.hpp"
#include "Exception/GLException.hpp"
#include <cstdlib>
#include <cstring>
#include <sstream>

namespace vpe {
	namespace Graphics {
		namespace internal
		{
			void GLBuffer::bind() const
			{
				glBindBuffer(target, location);
				GLExceptionRaiseMessageIfError("Failed to bind buffer");
			}

			void GLBuffer::unbind() const
			{
				unbind(target);
			}

			void GLBuffer::unbind(Target target)
			{
				if (isIndexBased(target))
				{
					glBindBufferBase(target, 0, 0);
				}
				else
				{
					glBindBuffer(target, 0);
				}
				GLExceptionRaiseMessageIfError("Failed to unbind buffer");
			}

			void GLBuffer::create()
			{
				if (location != 0) throw InvalidStateException("Buffer already created");

				glGenBuffers(1, &location);
				GLExceptionRaiseMessageIfError("Failed to generate buffer");
			}

			void GLBuffer::destroy()
			{
				if (location == 0) throw InvalidStateException("Buffer inexistent");

				glDeleteBuffers(1, &location);
				location = 0;

				GLExceptionRaiseMessageIfError("Failed to delete buffer");
			}

			void GLBuffer::upload()
			{
				if (writetimeMode != WritetimeMode::Dynamic && (flags & 2) == 2) throw InvalidStateException("Lifetime mode is not Dynamic, but attempted to reupload (same) data");
				if (!data) throw InvalidStateException("No data previously uploaded");

				ScopeBinder{ *this };
				glBufferData(target, elementSize * elementCount, data, cachedUsageHint);
				GLExceptionRaiseMessageIfError("Failed to upload buffer data");

				flags |= 2;
			}

			void GLBuffer::upload(void* data, size_t sizeInBytes, size_t count)
			{
				if (writetimeMode != WritetimeMode::Dynamic && (flags & 2) == 2) throw InvalidStateException("Lifetime mode not set to Dynamic, but attempted to reupload data");

				void* tmpData = std::malloc(sizeInBytes * count);
				std::memcpy(tmpData, data, sizeInBytes * count);
				assume(tmpData, sizeInBytes, count);
			}

			void GLBuffer::assume(void* data, size_t sizeInBytes, size_t count)
			{
				if (writetimeMode != WritetimeMode::Dynamic && (flags & 2) == 2) throw InvalidStateException("Lifetime mode not set to Dynamic, but attempted to reupload data");

				this->data = data;
				elementSize = sizeInBytes;
				elementCount = count;

				upload();
			}
		}
	}
}
