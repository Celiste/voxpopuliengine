#include "Graphics/internal/graphics.h"
#include "Graphics/GLContext.hpp"

namespace vpe {
	namespace Graphics
	{
		thread_local GLContext* curr = nullptr;
		std::atomic_uint16_t nextId = 0;

		GLContext::GLContext() : id(nextId++), intCtx(), ctxType(InternalContext) {}
		GLContext::GLContext(sf::Window& wnd) : id(nextId++), extWnd(&wnd), ctxType(ExternalWindow) {}
		GLContext::GLContext(sf::Context& ctx) : id(nextId++), extCtx(&ctx), ctxType(ExternalContext) {}
		GLContext::GLContext(sf::WindowHandle handle, const sf::ContextSettings& settings) : id(nextId++), intWnd(handle, settings), ctxType(InternalWindow) {}
		GLContext::GLContext(const sf::VideoMode& videoMode, const sf::String title, std::uint32_t style, const sf::ContextSettings& settings) : id(nextId++), intWnd(videoMode, title, style, settings), ctxType(InternalWindow) {}
		GLContext::GLContext(const sf::ContextSettings& settings, std::uint32_t width, std::uint32_t height) : id(nextId++), intCtx(settings, width, height), ctxType(InternalContext) {}
		GLContext::~GLContext() { deactivate(); }

		void GLContext::activate()
		{
			if (active && curr != this) throw InvalidStateException("Context already active in another thread - deactivate before moving between threads");

			if (curr)
			{
				do_deactivate();
			}
			curr = this;
			do_activate();
			onActivate(*this);
		}

		void GLContext::deactivate()
		{
			if (!active) return;

			if (active && curr != this) throw InvalidStateException("Attempted context deactivation from different thread");

			do_deactivate();
			onDeactivate(*this);
		}

		void GLContext::do_activate()
		{
			switch (ctxType)
			{
			case InnerType::ExternalContext: extCtx->setActive(true); break;
			case InnerType::ExternalWindow:  extWnd->setActive(true); break;
			case InnerType::InternalContext: intCtx.setActive(true);  break;
			case InnerType::InternalWindow:  intWnd.setActive(true);  break;
			default:
				std::ostringstream message;
				message << "Unknown inner context type" << ctxType;
				throw InvalidStateException(message.str());
			}
		}

		void GLContext::do_deactivate()
		{
			switch (ctxType)
			{
			case InnerType::ExternalContext: extCtx->setActive(false); break;
			case InnerType::ExternalWindow:  extWnd->setActive(false); break;
			case InnerType::InternalContext: intCtx.setActive(false);  break;
			case InnerType::InternalWindow:  intWnd.setActive(false);  break;
			default:
				std::ostringstream message;
				message << "Unknown inner context type" << ctxType;
				throw InvalidStateException(message.str());
			}
		}

		bool GLContext::isWindow() const
		{
			return ctxType == InternalWindow || ctxType == ExternalWindow;
		}

		sf::Window& GLContext::getWindow()
		{
			switch (ctxType)
			{
			case InnerType::InternalWindow: return intWnd;
			case InnerType::ExternalWindow: return *extWnd;
			default: throw InvalidStateException("Context does not use a window");
			}
		}

		const sf::Window& GLContext::getWindow() const
		{
			switch (ctxType)
			{
			case InnerType::InternalWindow: return intWnd;
			case InnerType::ExternalWindow: return *extWnd;
			default: throw InvalidStateException("Context does not use a window");
			}
		}

		sf::Context& GLContext::getContext()
		{
			switch (ctxType)
			{
			case InnerType::InternalContext: return intCtx;
			case InnerType::ExternalContext: return *extCtx;
			default: throw InvalidStateException("Context does not use a window");
			}
		}

		const sf::Context& GLContext::getContext() const
		{
			switch (ctxType)
			{
			case InnerType::InternalContext: return intCtx;
			case InnerType::ExternalContext: return *extCtx;
			default: throw InvalidStateException("Context does not use a window");
			}
		}

		GLContext* GLContext::getCurrent()
		{
			return curr;
		}
	}
}
