#include "Components/SpatialComponent.hpp"

namespace vpe
{
	void SpatialComponent::tick(const sf::Time& deltaTime)
	{
		if (dirty)
		{
			dirty = false;
		}
	}
}
