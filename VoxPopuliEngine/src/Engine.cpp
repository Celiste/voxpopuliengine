#include "Engine.hpp"
#include <sstream>
#include "Graphics/internal/graphics.h"
#include "Exception.hpp"
#include "GameMode.hpp"
#include "Graphics/SceneRenderer.hpp"
#include "Core/ScopeLock.hpp"

namespace vpe
{
	Engine* Engine::inst = nullptr;

	void Engine::preinitialize()
	{
		Graphics::GLContext ctx;

		GLenum glewerr = glewInit();
		if (glewerr != GLEW_OK)
		{
			std::ostringstream message;
			message << "Failed to initialize GLEW, error code " << glewerr << ": " << glewGetErrorString(glewerr);
			throw InitializationException(message.str());
		}

		glClearColor(0, 0, 0, 1);
	}

	void Engine::initialize()
	{
		renderer = new SceneRenderer();
	}

	void Engine::postinitialize()
	{
		if (!currentGameMode) throw InvalidStateException("Initial game mode required");
	}

	void Engine::loadPersistentAssets()
	{

	}

	void Engine::run()
	{
		totalGameTime = sf::Time::Zero;
		sf::Clock frameTimer;

		while (!exitting)
		{
			sf::Time frameDeltaTime = frameTimer.restart();
			totalGameTime += frameDeltaTime;

			// Transition to the next game mode if necessary
			if (currentGameMode && currentGameMode->getNextGameMode())
			{
				std::shared_ptr<GameMode> next = currentGameMode->getNextGameMode();

				currentGameMode->abandon();
				next->initialize();
				currentGameMode = next;
			}

			sf::Event evt;
			if (renderTarget) {
				renderTarget->activate();

				if (renderTarget->isWindow())
				{
					sf::Window& wnd = renderTarget->getWindow();
					while (wnd.pollEvent(evt))
					{
						delegateSFMLEvents(evt, wnd);
					}
				}
				update(frameDeltaTime);
				renderTarget->onUpdate(*renderTarget, frameDeltaTime);

				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

				render();
				renderTarget->onRender(*renderTarget);

				if (renderTarget->isWindow())
				{
					renderTarget->getWindow().display();
				}
			}

			for (auto& ctx : this->ctxs)
			{
				if (ctx->isWindow())
				{
					auto& wnd = ctx->getWindow();
					while (wnd.pollEvent(evt))
					{
						delegateSFMLEvents(evt, wnd);
					}
				}
				ctx->onUpdate(*ctx, frameDeltaTime);
				ctx->onRender(*ctx);
			}
		}

		inst->onShutdown();
		inst->cleanup();
	}

	void Engine::cleanup()
	{

	}


	void Engine::setRenderTarget(Graphics::GLContext* ctx)
	{
		renderTarget = ctx;
	}

	void Engine::addContext(Graphics::GLContext* ctx)
	{
		ctxs.push_back(ctx);
	}

	void Engine::removeContext(Graphics::GLContext* ctx)
	{
		for (auto it = ctxs.begin(), end = ctxs.end(); it != end; ++it)
		{
			if (*it == ctx)
			{
				ctxs.erase(it);
				break;
			}
		}
	}


	void Engine::delegateSFMLEvents(sf::Event& evt, sf::Window& wnd) const
	{
		switch (evt.type)
		{
		case sf::Event::Closed: onWindowClose(evt, wnd); break;
		case sf::Event::Resized: onResize(evt.size, wnd); break;
		case sf::Event::LostFocus: onFocusLost(evt, wnd); break;
		case sf::Event::GainedFocus: onFocusGain(evt, wnd); break;
		case sf::Event::TextEntered: onTextEnter(evt.text, wnd); break;
		case sf::Event::KeyPressed: onKeyPress(evt.key, wnd); break;
		case sf::Event::KeyReleased: onKeyRelease(evt.key, wnd); break;
		case sf::Event::MouseWheelScrolled: onMouseWheelScroll(evt.mouseWheelScroll, wnd); break;
		case sf::Event::MouseButtonPressed: onMouseButtonPress(evt.mouseButton, wnd); break;
		case sf::Event::MouseButtonReleased: onMouseButtonRelease(evt.mouseButton, wnd); break;
		case sf::Event::MouseMoved: onMouseMove(evt.mouseMove, wnd); break;
		case sf::Event::MouseEntered: onMouseEnter(evt, wnd); break;
		case sf::Event::MouseLeft: onMouseLeave(evt, wnd); break;
		case sf::Event::JoystickButtonPressed: onJoystickButtonPress(evt.joystickButton, wnd); break;
		case sf::Event::JoystickButtonReleased: onJoystickButtonRelease(evt.joystickButton, wnd); break;
		case sf::Event::JoystickMoved: onJoystickMove(evt.joystickMove, wnd); break;
		case sf::Event::JoystickConnected: onJoystickConnect(evt.joystickConnect, wnd); break;
		case sf::Event::JoystickDisconnected: onJoystickDisconnect(evt.joystickConnect, wnd); break;
		case sf::Event::TouchBegan: onTouchBegin(evt.touch, wnd); break;
		case sf::Event::TouchMoved: onTouchMove(evt.touch, wnd); break;
		case sf::Event::TouchEnded: onTouchEnd(evt.touch, wnd); break;
		case sf::Event::SensorChanged: onSensorChange(evt.sensor, wnd); break;
		}
	}


	void Engine::update(const sf::Time& deltaTime)
	{
		if (currentGameMode)
		{
			currentGameMode->update(deltaTime);
		}
	}

	void Engine::render()
	{
		if (currentGameMode && currentGameMode->getScreen())
		{
			currentGameMode->getScreen()->render();
		}
	}


	void Engine::startup(Engine& inst)
	{
		if (Engine::inst) throw InvalidStateException("Already initialized");
		
		Engine::inst = &inst;

		inst.preinitialize();
		inst.initialize();
		inst.postinitialize();
		inst.onStartup();
	}

	void Engine::startup(std::shared_ptr<GameMode> initialGameMode, Engine* inst)
	{
		if (Engine::inst) throw InvalidStateException("Already initialized");
		if (!initialGameMode) throw InvalidArgumentException("Initial game mode was null");
		
		if (!inst) inst = new Engine();

		inst->currentGameMode = initialGameMode;
		startup(*inst);
	}

	void Engine::reset()
	{
		Engine::inst = nullptr;
	}

	Engine& Engine::instance()
	{
		if (!inst) throw InvalidStateException("Engine not initialized");
		return *inst;
	}
}
