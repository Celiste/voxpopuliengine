#include "Space/Entity.hpp"
#include "Components/CameraComponent.hpp"
#include "Components/ScriptComponent.hpp"
#include "Components/RenderDataComponent.hpp"

namespace vpe
{
    /*
	WeakPointer<ComponentBase> Entity::addComponent(SemiSmartPointer<ComponentBase>&& component)
	{
		WeakPointer<ComponentBase> comp(component);
		WeakPointer<Camera2DComponent> compCamera(comp);
		WeakPointer<ScriptComponent> compScript(comp);
		WeakPointer<RenderDataComponent> compRender(comp);

		components.add(std::move(component));

		if (compCamera) cameraComponents.add(compCamera);
		if (compScript) scriptComponents.add(compScript);
		if (compRender) renderDataComponents.add(compRender);

		return comp;
	}

	void Entity::removeComponent(WeakPointer<ComponentBase> component)
	{
		WeakPointer<Camera2DComponent> compCamera(component);
		WeakPointer<ScriptComponent> compScript(component);
		WeakPointer<RenderDataComponent> compRender(component);

		for (auto it = components.begin(), end = components.end(); it != end; ++it)
		{
			if (*it == component)
			{
				components.remove(*it);
			}
		}

		if (compCamera) cameraComponents.remove(compCamera);
		if (compScript) scriptComponents.remove(compScript);
		if (compRender) renderDataComponents.remove(compRender);
	}

	void Entity::migrateTo(WeakPointer<World> world)
	{
		WeakPointer<World> oldWorld = this->world;
		this->world = world;

		onWorldChanged(this, oldWorld, world);
	}
	*/
}
