#include "GameMode.hpp"

namespace vpe
{
	void GameMode::update(const sf::Time& deltaTime)
	{
		if (screen)
		{
			screen->update(deltaTime);
		}
	}
}
